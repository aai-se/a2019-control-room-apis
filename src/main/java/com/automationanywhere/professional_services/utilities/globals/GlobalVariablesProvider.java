package com.automationanywhere.professional_services.utilities.globals;

import java.util.HashMap;
import java.util.Map;

public enum GlobalVariablesProvider
{
    INSTANCE();

    private Map<String, Object> globalVariables;

    private GlobalVariablesProvider()
    {
        this.globalVariables = new HashMap<String, Object>();
    }

    public GlobalVariablesProvider getInstance()
    {
        return INSTANCE;
    }

    public void addValue(String key, Object value)
            throws Exception
    {
        if (this.globalVariables.containsKey(key))
        {
            throw new Exception("Key " + key + " already exists");
        } else
        {
            this.globalVariables.put(key, value);
        }
    }

    public Object retrieveValue(String key)
            throws Exception
    {
        if (!this.globalVariables.containsKey(key))
        {
            throw new Exception("Key " + key + " could not be found; please check the same package version is in use across all taskbots");
        } else
        {
            return this.globalVariables.get(key);
        }
    }

    public boolean checkIfVaLueExists(String key)
    {
        return this.globalVariables.containsKey(key);
    }

    public void removeValue(String key)
            throws Exception
    {
        if (!this.globalVariables.containsKey(key))
        {
            throw new Exception("Key " + key + " could not be found");
        } else
        {
            this.globalVariables.remove(key);
        }
    }
}