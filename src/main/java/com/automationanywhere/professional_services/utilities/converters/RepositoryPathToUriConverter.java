package com.automationanywhere.professional_services.utilities.converters;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RepositoryPathToUriConverter
{
    public static String convertPathToUri(String repositoryPath)
    {
        // transform string into path
        Path path = Paths.get(repositoryPath);

        // get URI from path
        URI uri = path.toUri();

        // get string from URI
        String uriString = uri.toString();

        // replace all text prior to "Automation Anywhere" with correct prefix ("repository:///")
        return uriString.replaceAll("^.+(?=Automation%20Anywhere)", "repository:///");
    }
}