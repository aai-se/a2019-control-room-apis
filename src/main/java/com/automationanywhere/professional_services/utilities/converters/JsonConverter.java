package com.automationanywhere.professional_services.utilities.converters;

import com.automationanywhere.botcommand.data.Value;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class JsonConverter
{
    public static Map<String, Value> convertJsonToMapOfValues(String json)
            throws Exception
    {
        // get input map from JSONObject
        Map<String, Object> inputMap = new JSONObject(json).toMap();

        // instantiate output map
        Map<String, Value> outputMap = new LinkedHashMap<>();

        // loop through input map to add values to output map
        for (Map.Entry<String, Object> entry : inputMap.entrySet())
        {
            // get items from input map
            String entryKey = entry.getKey();
            Object entryValue = entry.getValue();

            try
            {
                // create ValueObjectMapping for this object
                // ValueObjectMapping mapping = new ValueObjectMapping(entry.getValue());
                ValueObjectMapping mapping = new ValueObjectMapping(entryValue);
                Value mappingValue = mapping.val;

                // add value to output map
                // outputMap.put(entry.getKey(), mapping.val);
                outputMap.put(entryKey, mappingValue);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        return outputMap;
    }

    public static List<Value> convertJsonToListOfValues(String json)
            throws Exception
    {
        // get input list from JSONObject
        List<Object> inputList = new JSONArray(json).toList();

        // instantiate output list
        List<Value> outputList = new ArrayList<>();

        // loop through input list to add values to output list
        for (Object listItem : inputList)
        {
            // create ValueObjectMapping for this list item
            ValueObjectMapping mapping = new ValueObjectMapping(listItem);

            // add value to output map
            outputList.add(mapping.val);
        }

        return outputList;
    }
}