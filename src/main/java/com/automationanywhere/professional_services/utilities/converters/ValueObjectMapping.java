package com.automationanywhere.professional_services.utilities.converters;

import com.automationanywhere.botcommand.data.impl.*;
import com.automationanywhere.botcommand.data.Value;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;

public class ValueObjectMapping
{
    public final VariableType type;
    public final String name;
    public final Object obj;
    public final Value val;

    public ValueObjectMapping(String name, Value inputValue)
            throws Exception
    {
        // store name
        this.name = name;

        if (inputValue instanceof StringValue)
        {
            this.type = VariableType.STR;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof NumberValue)
        {
            this.type = VariableType.NUM;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof BooleanValue)
        {
            this.type = VariableType.BOOL;
            this.val = inputValue;
            this.obj = this.val.get();

        } else if (inputValue instanceof DateTimeValue)
        {
            this.type = VariableType.DATE;
            this.val = inputValue;
            this.obj = this.val.get().toString();

        } else if (inputValue instanceof DictionaryValue)
        {
            this.type = VariableType.DICT;
            this.val = inputValue;

            DictionaryValue valueObj = (DictionaryValue) inputValue;
            LinkedHashMap<String, Value> valueMap = (LinkedHashMap<String, Value>) valueObj.get();

            Map<String, Object> objectMap = new LinkedHashMap<>();

            for (Map.Entry<String, Value> entry : valueMap.entrySet())
            {
                ValueObjectMapping mapping = new ValueObjectMapping(entry.getKey(), entry.getValue());

                // add value mapping to output map
                objectMap.put(mapping.name, mapping.obj);
            }

            this.obj = objectMap;

        } else if (inputValue instanceof ListValue)
        {
            this.type = VariableType.LIST;
            this.val = inputValue;

            ListValue valueObj = (ListValue) this.val;

            List<Value> valueList = valueObj.get();

            List<Object> objectList = new ArrayList<>();

            for (Value item : valueList)
            {
                ValueObjectMapping mapping = new ValueObjectMapping(null, item);
                objectList.add(mapping.obj);
            }

            this.obj = objectList;
        }
        /*

        // THIS WILL TURN MY BRAIN INTO SWISS CHEESE
        // BUT IT CAN BE DONE!!

        else if (input instanceof TableValue)
        {
            this.keyPrefix = "TBL|";

            TableValue valueObj = (TableValue) input;
            Table tableObj = valueObj.get();
            List<Schema> schemas = tableObj.getSchema();
            Schema schema = schemas.get(0);
            schema.

        } */
        else
        {
            throw new Exception("Value type not supported - " + inputValue.getClass().toString());
        }
    }

    public ValueObjectMapping(Object inputObj)
            throws Exception
    {
        // store input object
        this.obj = inputObj;

        // declare collection variables
        Map<String, Object> inputMap;
        Map<String, Value> outputMap;
        List<ValueObjectMapping> inputList;
        List<Value> outputList;

        this.type = null;
        this.name = null;

        if (this.obj == null)
        {
            this.val = new StringValue();
        } else if (this.obj instanceof String)
        {
            // try to parse as a ZonedDateTime - if this fails, assume input is a standard string
            Value tempVal;

            try
            {
                tempVal = new DateTimeValue(ZonedDateTime.parse((String) this.obj));
            } catch (DateTimeParseException ex)
            {
                tempVal = new StringValue((String) this.obj);
            }

            this.val = tempVal;

        } else if (this.obj instanceof Number)
        {
            this.val = new NumberValue(this.obj);

        } else if (this.obj instanceof Boolean)
        {
            this.val = new BooleanValue(this.obj);

        } else if (this.obj instanceof Map)
        {
            inputMap = (HashMap) this.obj;
            outputMap = new LinkedHashMap<>();

            // directly insert items from input map to output map
            for (Map.Entry<String, Object> entry : inputMap.entrySet())
            {
                ValueObjectMapping mapping = new ValueObjectMapping(entry.getValue());
                outputMap.put(entry.getKey(), mapping.val);
            }

            this.val = new DictionaryValue(outputMap);

        } else if (this.obj instanceof List)
        {
            // instantiate input and output objects
            inputList = (List) this.obj;
            outputList = new ArrayList<>();

            // create mapping objects from input list and add to output list
            for (Object listItem : inputList)
            {
                ValueObjectMapping mapping = new ValueObjectMapping(listItem);
                outputList.add(mapping.val);
            }

            ListValue output = new ListValue();
            output.set(outputList);
            this.val = output;
        } else
        {
            throw new Exception("Unsupported object type: " + this.obj.getClass().getCanonicalName());
        }
    }

    public enum VariableType
    {
        STR, NUM, BOOL, DATE, DICT, LIST
    }
}