package com.automationanywhere.professional_services.utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationJsonToCsvFormatter
{

    private static final Pattern PATTERN_FIELD_NAME = Pattern.compile("(?<=(Name=))[^,]+(?=,)");
    private static final Pattern PATTERN_FIELD_VALUE = Pattern.compile("(?<=(Value=))[^,]*(?=,)");
    private static final Pattern PATTERN_ISSUE_CODE = Pattern.compile("\\d");

    private static final String HEADER_ROW = "File ID,File name,Failed field counts,Pass field counts,Total field counts,Group ID,Field name,Field value,Issue code,Issue details";

    public static void main(String[] args)
    {
        try
        {
            // validate input from user and get path
            Path pathToInputFolder = validateFolderInput(args);

            // loop through JSON files in folder
            File inputFolder = pathToInputFolder.toFile();

            for (File jsonFile : Objects.requireNonNull(inputFolder.listFiles(new JsonFileFilter())))
            {
                // prep path to output file
                Path outputFilePath = Paths.get(jsonFile.getPath().replace(".json", ".csv"));

                // convert JSON to list of strings (including header)
                List<String> csvData = convertValidationJson(jsonFile);

                // write data to output file
                Files.write(outputFilePath, csvData);
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private static Path validateFolderInput(String[] args)
            throws Exception
    {
        // check at least one argument was provided
        if (args.length == 0 || args[0].length() == 0)
        {
            throw new Exception("Folder not provided");
        }

        // check path is valid - must be a directory
        Path pathToInputFolder = Paths.get(args[0]);

        if (!Files.isDirectory(pathToInputFolder))
        {
            throw new Exception("Folder input is not a directory");
        }

        return pathToInputFolder;
    }

    private static List<String> convertValidationJson(File jsonFile)
            throws IOException
    {
        // instantiate output variable
        List<String> output = new ArrayList<>();

        // add header row
        output.add(HEADER_ROW);

        // load JSON data
        String fileData = Files.readString(jsonFile.toPath(), StandardCharsets.ISO_8859_1);
        JSONObject root = new JSONObject(fileData);

        // get JSON array for validation files
        JSONArray validationFiles = root.getJSONArray("fileDetailsList");

        // process each validation file
        for (int i = 0; i < validationFiles.length(); i++)
        {
            // convert single validation file entry to list of strings
            JSONObject validationFile = validationFiles.getJSONObject(i);
            List<String> validationFileData = processValidationFileEntry(validationFile);

            // add all strings from validation file entry to output
            output.addAll(validationFileData);
        }

        return output;
    }

    private static List<String> processValidationFileEntry(JSONObject validationFile)
    {
        // instantiate output variable
        List<String> output = new ArrayList<>();

        // build row prefix using file-specific data
        List<String> rowPrefixItems = new ArrayList<>();

        rowPrefixItems.add(validationFile.getString("fileId"));
        rowPrefixItems.add(validationFile.getString("fileName"));
        rowPrefixItems.add(String.valueOf(validationFile.getInt("failedFieldCounts")));
        rowPrefixItems.add(String.valueOf(validationFile.getInt("passFieldCounts")));
        rowPrefixItems.add(String.valueOf(validationFile.getInt("totalFieldCounts")));
        rowPrefixItems.add(String.valueOf(validationFile.getInt("classificationId")));

        String rowPrefix = String.join(",", rowPrefixItems);

        // iterate through error fields for this row
        JSONArray errorFields = validationFile.getJSONArray("errorFields");

        Matcher fieldNameMatcher = PATTERN_FIELD_NAME.matcher("");
        Matcher fieldValueMatcher = PATTERN_FIELD_VALUE.matcher("");
        Matcher issueCodeMatcher = PATTERN_ISSUE_CODE.matcher("");

        for (int i = 0; i < errorFields.length(); i++)
        {
            // get error field object
            JSONObject errorField = errorFields.getJSONObject(i);

            // extract field name from fieldDetails object
            fieldNameMatcher.reset(errorField.getString("fieldDetails"));
            fieldNameMatcher.find();
            String fieldName = fieldNameMatcher.group();

            // extract value from fieldValue object
            fieldValueMatcher.reset(errorField.getString("fieldValue"));
            fieldValueMatcher.find();
            String fieldValue = fieldValueMatcher.group();

            // extract issue code from fieldError object
            issueCodeMatcher.reset(errorField.getString("fieldError"));
            issueCodeMatcher.find();
            String issueCode = issueCodeMatcher.group();

            // get error details
            String issueDetails = errorField.getString("fieldErrorDetails");

            // build row
            String row = rowPrefix + "," + fieldName + "," + fieldValue + "," + issueCode + "," + issueDetails;

            // add row to output
            output.add(row);
        }

        return output;
    }

    private static class JsonFileFilter
            implements FileFilter
    {
        public boolean accept(File file)
        {
            return file.getName().endsWith(".json");
        }
    }
}
