package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.professional_services.cr_apis.apis.iqbot.IQBotFileExtractedData;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Test_FileDownloadResponse
{
    public static void main(String[] args)
    {
        try
        {
            // load data from sample file
            // Path pathToInputFile = Paths.get("C:\\testing\\IQ Bot\\API output v1.txt");
            Path pathToInputFile = Paths.get("C:\\testing\\IQ Bot\\API output v2.txt");
            String rawData = Files.readString(pathToInputFile);

            // Path pathToConfigFile = Paths.get("C:\\Users\\Andrew.Lawes\\Desktop\\IQ Bot data config file v3.json.txt");
            Path pathToDataModel = Paths.get("C:\\testing\\IQ Bot\\Data model v3.json.txt");
            String dataModel = Files.readString(pathToDataModel);

            // create extracted data object
            IQBotFileExtractedData dataObj = new IQBotFileExtractedData(rawData, dataModel);
            DictionaryValue getAsDictionaries = dataObj.getAsMappings();

            // get A360 list
            // ListValue output = response.convertToListValue();

            String breakpoint = "";
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}