package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement.users.A_SearchUsers;

public class Test_UserManagement
{
    private static final String domain = null;
    private static final String email = "andrew.lawes@automationanywhere.com";
    private static final Boolean enableAutoLogin = true;
    private static final String username = "api_test_user";
    private static final String firstName = "Andrew";
    private static final String lastName = "Lawes";
    private static final String description = "Org: PS | Cost centre: CC2000_300 Professional Services - UK";
    private static final Boolean disabled = false;
    private static final String password = null;

    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        DictionaryValue searchUsers = searchUsers(sessionType, sessionName);
    }

    public static DictionaryValue searchUsers(String sessionType, String sessionName)
    {
        A_SearchUsers command = new A_SearchUsers();

        return command.searchUsers(sessionType,sessionName,null);
    }
}