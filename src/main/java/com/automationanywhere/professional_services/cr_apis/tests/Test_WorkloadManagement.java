package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.professional_services.cr_apis.botcommands.v3.workloadmanagement.queues.F_SearchQueues;
import com.automationanywhere.professional_services.cr_apis.botcommands.v3.workloadmanagement.workitems.A_InsertWorkitems;
import com.automationanywhere.professional_services.cr_apis.botcommands.v3.workloadmanagement.workitems.E_SearchWorkitems;

import java.util.ArrayList;
import java.util.List;

public class Test_WorkloadManagement
{
    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        // DictionaryValue searchWorkitems = searchForWorkitems(sessionType, sessionName);

        DictionaryValue searchForQueues = searchForQueues(sessionType, sessionName);

        ListValue listOfQueues =  (ListValue) searchForQueues.get("list");
        DictionaryValue firstQueue = (DictionaryValue) listOfQueues.get(0);
        StringValue queueIdString = (StringValue) firstQueue.get("id");
        Double queueId = queueIdString.getAsDouble();

        DictionaryValue insertWorkitems = insertWorkitems(sessionType, sessionName, queueId);
    }

    public static DictionaryValue searchForQueues(String sessionType, String sessionName)
    {
        F_SearchQueues command = new F_SearchQueues();

        String filterRequestJson =
                "{\n" +
                        "    \"filter\": {\n" +
                        "        \"operator\": \"eq\",\n" +
                        "        \"field\": \"name\",\n" +
                        "        \"value\": \"Test queue\"\n" +
                        "    }\n" +
                        "}";

        return command.searchQueues(sessionType,sessionName,filterRequestJson);
    }

    public static DictionaryValue insertWorkitems(String sessionType, String sessionName, Double queueId)
    {
        // create table schemas
        List<Schema> schemas = new ArrayList<Schema>();

        schemas.add(new Schema("id"));
        schemas.add(new Schema("name"));
        schemas.add(new Schema("extension"));

        // create table rows
        List<Row> rows = new ArrayList<Row>();

        rows.add(new Row(new StringValue("IDE id1"),new StringValue("IDE name1"),new StringValue("IDE extension1")));
        rows.add(new Row(new StringValue("IDE id2"),new StringValue("IDE name2"),new StringValue("IDE extension2")));
        rows.add(new Row(new StringValue("IDE id3"),new StringValue("IDE name3"),new StringValue("IDE extension3")));

        // create table object
        Table workitemData = new Table(schemas, rows);

        A_InsertWorkitems command = new A_InsertWorkitems();

        return command.insertWorkitems(sessionType, sessionName, queueId, workitemData);
    }

    public static DictionaryValue searchForWorkitems(String sessionType, String sessionName)
            throws Exception
    {
        E_SearchWorkitems command = new E_SearchWorkitems();

        String filterRequestJson =
                "{\n" +
                        "    \"filter\": {\n" +
                        "        \"operator\": \"eq\",\n" +
                        "        \"field\": \"id\",\n" +
                        "        \"value\": \"95\"\n" +
                        "    }\n" +
                        "}";
        Double queueId = 2.0D;

        return command.searchWorkitems(sessionType, sessionName, queueId, filterRequestJson);
    }
}
