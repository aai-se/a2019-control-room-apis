package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.professional_services.cr_apis.botcommands.authentication.A_SessionStart;
import com.automationanywhere.core.security.SecureString;

import java.nio.charset.StandardCharsets;

public class Test_Multiple_Authentication
{
    public static void main(String[] args)
    {
        String sessionName = "Default";
        String urlInput = "http://jlr-integration-testing-94452.aaiproductdemo.com";
        String usernameInput = "andrew.lawes.api";
        String authMethod = "PASSWORD";
        String authValueInput = "_Yb4)GMu.Xn:Tni";

        SecureString url = new SecureString(urlInput.getBytes(StandardCharsets.UTF_8));
        SecureString username = new SecureString(usernameInput.getBytes(StandardCharsets.UTF_8));
        SecureString authValue = new SecureString(authValueInput.getBytes(StandardCharsets.UTF_8));

        try
        {
            for (int i = 0; i < 100; i++)
            {
                A_SessionStart command = new A_SessionStart();
                command.startSessionAndAuthenticateUser(sessionName, url, username, authMethod, authValue);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        String breakpoint = "";
    }
}
