package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.files.*;
import com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.learninginstances.E_GetLearningInstances;
import com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.learninginstances.G_GetFilenamesWithStatus;
import com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.learninginstances.I_MapGroupsToStatuses;

public class Test_IQ_Bot
{

    private static final String fileId = "f47c9485-08dd-45f9-b5b4-5fac3e553869";
    private static final String learningInstanceId = "8da2a4bd-a6d7-4bf1-99a4-a6fce8099066";
    private static final String pathToFile = "C:\\testing\\Customers\\JLR\\All Jan PDFs\\21PL331020E0003384-FVGRA - JLRLM21010010 - TEAM C - SFD SENT - DEX8000847.pdf";

    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        /*
        DictionaryValue invalidFiles = getFilesInStatus(sessionType, sessionName, learningInstanceId, "VALIDATION");

        String fileId = "";

        Map<String, Value> map = invalidFiles.get();
        for (Map.Entry<String, Value> entry : map.entrySet())
        {
            fileId = entry.getKey().substring(0,36);
            break;
        }

        DictionaryValue validationData = getValidationData(sessionType, sessionName, learningInstanceId, fileId);
         */

        /*
        DictionaryValue uploadFile = uploadFile(sessionType, sessionName);

        StringValue fileId = (StringValue) uploadFile.get("fileId");

        DictionaryValue getFileStatus = getFileStatus(sessionType, sessionName, fileId.get());

        // deleteFile(sessionType, sessionName);

        // ListValue extractedData = downloadExtractedData(sessionType, sessionName, fileId);

        DictionaryValue invalidFiles = getFilesInStatus(sessionType, sessionName, learningInstanceId, "VALIDATION");

        String fileId = ((String) invalidFiles.get().keySet().toArray()[0]).substring(0, 36);

        DictionaryValue statusBefore = getFileStatus(sessionType, sessionName, fileId);
        invalidateFile(sessionType, sessionName, learningInstanceId, fileId);
        DictionaryValue statusAfter = getFileStatus(sessionType, sessionName, fileId);
         */

        DictionaryValue allGroupsAndStatus = getAllGroupsData(sessionType,sessionName, learningInstanceId);
    }

    public static DictionaryValue getLearningInstances(String sessionType, String sessionName)
    {
        E_GetLearningInstances command = new E_GetLearningInstances();

        return command.getLearningInstances(sessionType, sessionName);
    }

    public static DictionaryValue uploadFile(String sessionType, String sessionName)
    {
        A_UploadFile command = new A_UploadFile();

        return command.uploadFile(sessionType, sessionName, learningInstanceId, pathToFile);
    }

    public static DictionaryValue getFileStatus(String sessionType, String sessionName, String fileId)
    {
        B_GetFileStatus command = new B_GetFileStatus();

        return command.getFileStatus(sessionType, sessionName, learningInstanceId, fileId);
    }

    public static void deleteFile(String sessionType, String sessionName)
    {
        D_DeleteFile command = new D_DeleteFile();

        command.deleteFile(sessionType, sessionName, learningInstanceId, fileId);
    }

    public static Value downloadExtractedData(String sessionType, String sessionName, String fileId, String returnType, String configData)
    {
        C_DownloadExtractedData command = new C_DownloadExtractedData();

        return command.downloadExtractedData(sessionType, sessionName, fileId, returnType, configData);
    }

    public static DictionaryValue getFilesInStatus(String sessionType, String sessionName, String learningInstanceId, String docStatusInput)
    {
        G_GetFilenamesWithStatus command = new G_GetFilenamesWithStatus();

        return command.getFilenamesWithStatus(sessionType, sessionName, learningInstanceId, docStatusInput);
    }

    public static void invalidateFile(String sessionType, String sessionName, String learningInstanceId, String fileId)
    {
        E_InvalidateFile command = new E_InvalidateFile();

        command.invalidateFile(sessionType, sessionName, learningInstanceId, fileId);
    }

    public static DictionaryValue getValidationData(String sessionType, String sessionName, String learningInstanceId, String fileId)
    {
        F_GetDataFromValidationApi command = new F_GetDataFromValidationApi();

        return command.getDataFromValidationApi(sessionType, sessionName, learningInstanceId, fileId);
    }

    public static DictionaryValue getAllGroupsData(String sessionType, String sessionName, String learningInstanceId)
    {
        I_MapGroupsToStatuses command = new I_MapGroupsToStatuses();

        return command.getGroupsAndStatuses(sessionType, sessionName, learningInstanceId);
    }
}