package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.files.A_GetFileUrl;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.teams.*;

import java.util.List;

public class Test_AARI
{
    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        /*
        // DictionaryValue searchTeamsOutput = searchTeams(sessionType, sessionName);
        // DictionaryValue getTeamDetails = getTeamDetails(sessionType, sessionName);
        // DictionaryValue getTeamMembers = getTeamMembers(sessionType, sessionName);
        // DictionaryValue getUserDetails = getUserDetails(sessionType, sessionName);

        String teamName = "Test team - Java";
        String teamDescription = "";

        List<NumberValue> teamMemberIds = new ArrayList<NumberValue>();
        teamMemberIds.add(new NumberValue(85D));

        List<NumberValue> processIds = new ArrayList<NumberValue>();

        DictionaryValue createTeam = createTeam(sessionType, sessionName, teamName, teamDescription, teamMemberIds, processIds);

        // Double teamId = createTeam.get("id").getAsDouble();

        // updateTeam(sessionType, sessionName, 15D, teamName, teamDescription, teamMemberIds, processIds);

        // deleteTeam(sessionType, sessionName, teamId);
         */

        Double fileId = 242D;
        String url = getFileUrl(sessionType, sessionName, fileId);
    }

    public static DictionaryValue searchTeams(String sessionType, String sessionName)
    {
        String filterRequestJson = "{}";

        A_SearchTeams command = new A_SearchTeams();

        return command.searchTeams(sessionType, sessionName, filterRequestJson);
    }

    public static DictionaryValue getTeamDetails(String sessionType, String sessionName)
    {
        Double teamId = 8D;

        B_GetTeamDetails command = new B_GetTeamDetails();

        return command.getTeamDetails(sessionType, sessionName, teamId);
    }

    public static DictionaryValue getTeamMembers(String sessionType, String sessionName)
    {
        Double teamId = 8D;

        C_GetTeamMembers command = new C_GetTeamMembers();

        return command.getTeamMembers(sessionType, sessionName, teamId);
    }

    public static DictionaryValue getUserDetails(String sessionType, String sessionName)
    {
        Double userId = 141D;

        D_GetUserDetails command = new D_GetUserDetails();

        return command.getUserDetails(sessionType, sessionName, userId);
    }

    public static DictionaryValue createTeam(String sessionType, String sessionName, String teamName, String teamDescription, List<NumberValue> teamMemberIds,
                                             List<NumberValue> processIds)
    {
        E_CreateTeam command = new E_CreateTeam();

        return command.createTeam(sessionType, sessionName, teamName, teamDescription, teamMemberIds, processIds);
    }

    public static void updateTeam(String sessionType, String sessionName, Double teamId, String teamName, String teamDescription, List<NumberValue> teamMemberIds,
                                  List<NumberValue> processIds)
    {
        F_UpdateTeam command = new F_UpdateTeam();

        command.updateTeam(sessionType, sessionName, teamId, teamName, teamDescription, teamMemberIds, processIds);
    }

    public static void deleteTeam(String sessionType, String sessionName, Double teamId)
    {
        G_DeleteTeam command = new G_DeleteTeam();

        command.deleteTeam(sessionType, sessionName, teamId);
    }

    public static String getFileUrl(String sessionType, String sessionName, Double fileId)
    {
        A_GetFileUrl command = new A_GetFileUrl();

        return command.getFileUrl(sessionType, sessionName, fileId).get();
    }
}