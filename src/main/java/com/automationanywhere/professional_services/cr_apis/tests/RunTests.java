package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.botcommand.data.Value;

public class RunTests
{
    public static final String sessionType = SessionType.CUSTOM_SESSION.name();
    public static final String sessionName = "Default";

    public static void main(String[] args)
            throws Exception
    {
        try
        {
            // get authentication details from args input
            String controlRoomUrl = args[0];
            String username = args[1];
            String authMethod = args[2];
            String authInput = args[3];

            // authenticate
            Value authResponse = Test_Authentication.authenticateUser(sessionName, controlRoomUrl, username, authMethod, authInput);

            // execute tests

            // Test_AARI.runTests(sessionType, sessionName);
            // Test_Authentication.runTests(sessionType, sessionName);
            // Test_Audit.runTests(sessionType, sessionName);
            // Test_Authentication.runTests(client);
            // Test_CredentialVault.runTests(sessionType, sessionName);
            // Test_Devices.runTests(client);
            // Test_RepositoryManagement.runTests(sessionType, sessionName);
            // Test_UserManagement.runTests(sessionType, sessionName);
            // Test_WorkloadManagement.runTests(sessionType, sessionName);
            Test_PrivateApis.runTests(sessionType, sessionName);
            // Test_IQ_Bot.runTests(sessionType, sessionName);
            // Test_BLM.runTests(sessionType, sessionName);

            String breakpoint = "";
        } catch (Exception ex)
        {
            System.err.println(ex.getMessage());
        }
    }
}