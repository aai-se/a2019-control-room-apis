package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.v1.audit.A_SearchAuditLog;

public class Test_Audit
{
    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        DictionaryValue auditOutput = searchAuditLog(sessionType, sessionName);
    }

    public static DictionaryValue searchAuditLog(String sessionType, String sessionName)
    {
        String filterRequestJson = "{\"page\":{\"length\":10}}";

        A_SearchAuditLog command = new A_SearchAuditLog();

        return command.searchCredentials(sessionType, sessionName, filterRequestJson);
    }
}