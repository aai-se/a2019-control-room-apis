package com.automationanywhere.professional_services.cr_apis.tests;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestIQBotJson
{
    public static void main(String[] args)
    {
        try
        {
            // read in input from file
            Path pathToInputFile = Paths.get("C:\\testing\\IQ Bot\\API output.txt");
            String rawData = Files.readString(pathToInputFile);

            // convert to valid JSON
            String validJson = convertRawDataToValidJson(rawData);

            // convert to list of strings
            List<String> csv = convertValidJsonToCsv(validJson);

            String breakpoint = "";

        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private static String convertRawDataToValidJson(String rawInput)
    {
        // remove square brackets
        rawInput = rawInput.replaceAll("\\[\\s*", "");
        rawInput = rawInput.replaceAll("\\s*\\]", "");

        // correct newlines between entries
        rawInput = rawInput.replaceAll("\\},\\s+\\{", "},{");

        // split on commas between entries using lookaheads
        String[] entries = rawInput.split("(?<=\\}),(?=\\{)");

        // loop through individual entries
        for (int i = 0; i < entries.length; i++)
        {
            // trim curly braces and any extra whitespace
            entries[i] = entries[i].replaceAll("\\{\\s*", "");
            entries[i] = entries[i].replaceAll("\\s*\\}", "");

            // replace erroneous newlines
            entries[i] = entries[i].replaceAll("\r\n\\s*", "");

            // split entry into key value pairs using lookahead to select the correct commas
            // lookahead ensures that commas within values are not selected
            String[] keyValuePairs = entries[i].split(", (?=[^,=]+=)");

            // loop through key value pairs
            for (int j = 0; j < keyValuePairs.length; j++)
            {
                // remove newlines and following whitespace
                keyValuePairs[j] = keyValuePairs[j].replaceAll("\r\n\\s+", "");

                // add leading and trailing double quotes
                keyValuePairs[j] = "\"" + keyValuePairs[j] + "\"";

                // replace first equals with colon surrounded by double quotes
                keyValuePairs[j] = keyValuePairs[j].replaceFirst("=", "\":\"");
            }

            // join key value pairs back into a single string using comma separation
            String entry = String.join(",", keyValuePairs);

            // restore the curly braces
            entry = "{" + entry + "}";

            // replace the entry
            entries[i] = entry;
        }

        // join entries back into one string
        String textOutput = String.join(",", entries);

        // restore square brackets
        textOutput = "[" + textOutput + "]";

        // return output
        return textOutput;
    }

    private static List<String> convertValidJsonToCsv(String validJson)
    {
        // create variable to hold output
        List<String> output = new ArrayList<String>();

        // create JSON array from input
        JSONArray extractedData = new JSONArray(validJson);

        // get JSON array representing keys in first element
        JSONArray keys = extractedData.getJSONObject(0).names();

        // use list of strings to store header row to contain keys
        List<String> headerRowList = new ArrayList<String>();

        // iterate through keys
        for (Object keyElement : keys)
        {
            // cast key to string and add to list
            headerRowList.add((String) keyElement);
        }

        // convert list to comma-separated string and add to output
        String headerRow = String.join(",", headerRowList);
        output.add(headerRow);

        // iterate through items in extracted data
        List<String> dataRowList = new ArrayList<String>();

        for (Object data : extractedData)
        {
            // cast data to JSONObject
            JSONObject dataRowObject = (JSONObject) data;

            // clear list of previous data
            dataRowList.clear();

            // iterate through values to extract in order
            for (String key : headerRowList)
            {
                // get value and add to data list with double quotes
                String value = "\"" + dataRowObject.getString(key) + "\"";
                dataRowList.add(value);
            }

            // join values with commas and add to output
            String dataRow = String.join(",", dataRowList);
            output.add(dataRow);
        }

        // return output
        return output;
    }
}
