package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.repositorymanagement.B_GetItemsInFolder;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.repositorymanagement.C_GetItemsInWorkspace;

public class Test_RepositoryManagement
{
    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        String filterRequestJson ="{}";
        Double folderIdInput = 9.0;
        String workspaceTypeInput = "Public";

        DictionaryValue getItemsInFolder = getItemsInFolder(sessionType, sessionName, folderIdInput, filterRequestJson);

        DictionaryValue getItemsInWorkspace = getItemsInWorkspace(sessionType, sessionName, workspaceTypeInput, filterRequestJson);
    }

    public static DictionaryValue getItemsInFolder(String sessionType, String sessionName, Double folderIdInput, String filterRequestJson)
            throws Exception
    {
        B_GetItemsInFolder command = new B_GetItemsInFolder();

        return command.getItemsInFolder(sessionType, sessionName, folderIdInput, filterRequestJson);
    }

    public static DictionaryValue getItemsInWorkspace(String sessionType, String sessionName, String workspaceTypeInput, String filterRequestJson)
            throws Exception
    {
        C_GetItemsInWorkspace command = new C_GetItemsInWorkspace();

        return command.getItemsInWorkspace(sessionType, sessionName, workspaceTypeInput, filterRequestJson);
    }
}
