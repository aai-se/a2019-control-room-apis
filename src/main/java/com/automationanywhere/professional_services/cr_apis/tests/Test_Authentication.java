package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.authentication.B_VerifyToken;
import com.automationanywhere.professional_services.cr_apis.botcommands.authentication.C_RefreshToken;
import com.automationanywhere.professional_services.cr_apis.botcommands.authentication.D_SessionEnd;
import com.automationanywhere.professional_services.cr_apis.botcommands.authentication.A_SessionStart;
import com.automationanywhere.core.security.SecureString;

import java.nio.charset.StandardCharsets;

public class Test_Authentication
{

    public static void runTests(String sessionType, String sessionName, String controlRoomUrl, String username, String authMethod, String authInput)
            throws Exception
    {
        // authenticate and start a session
        DictionaryValue authOutput = Test_Authentication.authenticateUser(sessionName, controlRoomUrl, username, authMethod, authInput);

        // get the token
        String token = ((StringValue) authOutput.get("token")).get();

        // verify the token
        BooleanValue verifyToken = Test_Authentication.verifyToken(sessionType, sessionName, token);

        // refresh the token
        StringValue refreshedToken = Test_Authentication.refreshToken(sessionType, sessionName, token);

        // logout
        Test_Authentication.logout(sessionName);
    }

    public static DictionaryValue authenticateUser(String sessionName, String controlRoomUrl, String username, String authMethod, String authInput)
    {
        A_SessionStart command = new A_SessionStart();

        SecureString controlRoomUrlSecure = new SecureString(controlRoomUrl.getBytes(StandardCharsets.UTF_8));
        SecureString usernameSecure = new SecureString(username.getBytes(StandardCharsets.UTF_8));
        SecureString authInputSecure = new SecureString(authInput.getBytes(StandardCharsets.UTF_8));

        return command.startSessionAndAuthenticateUser(sessionName, controlRoomUrlSecure, usernameSecure, authMethod, authInputSecure);
    }

    public static BooleanValue verifyToken(String sessionType, String sessionName, String authToken)
    {
        B_VerifyToken command = new B_VerifyToken();

        return command.verifyToken(sessionType, sessionName, authToken);
    }

    public static StringValue refreshToken(String sessionType, String sessionName, String authToken)
    {
        C_RefreshToken command = new C_RefreshToken();

        return command.refreshToken(sessionType, sessionName, authToken);
    }

    public static void logout(String sessionName)
    {
        D_SessionEnd command = new D_SessionEnd();

        command.endSession(sessionName);
    }
}
