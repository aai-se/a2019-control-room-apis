package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands._b_private.B_GetBotContent;
import com.automationanywhere.professional_services.cr_apis.botcommands._b_private.C_CopyRepositoryFile;
import com.automationanywhere.professional_services.cr_apis.botcommands._b_private.F_GetDetailedGlobalValues;
import com.automationanywhere.professional_services.cr_apis.botcommands._b_private.H_CancelCheckOut;
import com.automationanywhere.professional_services.cr_apis.botcommands._d_custom_functions.A_AmendBotContent;

public class Test_PrivateApis
{
    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        Double fileIdInput = 1418D;
                /*
        String name = "API file copy";
        Double parentIdInput = 495D;
         */

        // DictionaryValue getBotContent = getBotContent(sessionType, sessionName, fileId);

        // amendBotContent(sessionType, sessionName, fileId);

        // copyRepositoryFile(sessionType, sessionName, fileIdInput, name, parentIdInput);

        // DictionaryValue globalValues = getDetailedGlobalValues(sessionType, sessionName);

        cancelCheckout(sessionType, sessionName, fileIdInput);
    }

    public static void amendBotContent(String sessionType, String sessionName, Double fileIdInput)
    {
        A_AmendBotContent command = new A_AmendBotContent();

        // command.amendBotContent(sessionType, sessionName, fileIdInput, true, true, true, true);
    }

    public static DictionaryValue getBotContent(String sessionType, String sessionName, Double fileIdInput)
            throws Exception
    {
        B_GetBotContent command = new B_GetBotContent();

        return command.getBotContent(sessionType, sessionName, fileIdInput);
    }

    public static DictionaryValue copyRepositoryFile(String sessionType, String sessionName, Double fileIdInput, String name, Double parentIdInput)
    {
        C_CopyRepositoryFile command = new C_CopyRepositoryFile();

        return command.copyRepositoryFile(sessionType, sessionName, fileIdInput, name, parentIdInput);
    }

    public static DictionaryValue getDetailedGlobalValues(String sessionType, String sessionName)
    {
        F_GetDetailedGlobalValues command = new F_GetDetailedGlobalValues();

        return command.getDetailedGlobalValues(sessionType, sessionName);
    }

    public static void cancelCheckout(String sessionType, String sessionName, Double fileIdInput)
    {
        H_CancelCheckOut command = new H_CancelCheckOut();

        command.cancelCheckout(sessionType, sessionName, fileIdInput);
    }
}
