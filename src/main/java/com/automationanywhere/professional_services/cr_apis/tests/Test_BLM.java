package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm.A_ExportBlmPackage;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm.C_CheckBlmStatus;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm.D_DownloadBlmPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Test_BLM
{

    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {
        String packageName = "IDE test 3";

        List<StringValue> fileIds = new ArrayList<>();
        fileIds.add(new StringValue("57"));

        Boolean includePackages = true;

        String archivePassword = "password";

        String pathToOutputFile = "C:\\testing\\CR APIs\\" + packageName + ".zip";

        Map<String, Value> requestId = exportBlmPackage(sessionType, sessionName, packageName, fileIds, includePackages, archivePassword).get();

        /*
        Map<String, Value> status = getBlmStatus(sessionType, sessionName, requestId).get();

        downloadBlmPackage(sessionType, sessionName, requestId, pathToOutputFile);
         */
    }

    public static DictionaryValue exportBlmPackage(String sessionType, String sessionName, String packageName, List<StringValue> fileIds, Boolean includePackages,
                                               String archivePassword)
    {
        A_ExportBlmPackage command = new A_ExportBlmPackage();

        return command.exportBlmPackage(sessionType, sessionName, packageName, fileIds, includePackages, archivePassword);
    }

    public static DictionaryValue getBlmStatus(String sessionType, String sessionName, String requestId)
    {
        C_CheckBlmStatus command = new C_CheckBlmStatus();

        return command.checkBlmStatus(sessionType, sessionName, requestId);
    }

    public static void downloadBlmPackage(String sessionType, String sessionName, String requestId, String pathToOutputFile)
    {
        D_DownloadBlmPackage command = new D_DownloadBlmPackage();

        command.downloadBlmPackage(sessionType, sessionName, requestId, pathToOutputFile);
    }

    public static void uploadBlmPackage(String sessionType, String sessionName, String requestId, String pathToOutputFile)
    {

    }
}