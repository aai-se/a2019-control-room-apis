package com.automationanywhere.professional_services.cr_apis.tests;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.lockers.J_AddOrUpdateLockerMember;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.loginsetting.A_SetDeviceCredentials;
import com.automationanywhere.core.security.SecureString;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Test_CredentialVault
{

    public static void runTests(String sessionType, String sessionName)
            throws Exception
    {

        // searchCredentials(client);
        // createCredential(client);
        // getAttributeValues(client);
        // createAttributeValues(client);
        // createLocker(client);
        // searchLockers(client);
        // getPublicKey(client);
        // addLockerMember(sessionType, sessionName, "4", "71", "PARTICIPATE");
        // setDeviceCredentials(sessionType, sessionName, "deviceName", "devicePassword", "username");
    }

    public static void searchCredentials(ControlRoomRestClient client)
            throws Exception
    {
        String filterRequestJson = "";
        boolean consumed = false;

        ApiResponse searchCredentials = v2_CredentialVault.searchForCredentials(client, filterRequestJson, consumed);
    }

    /*
    public static void createCredential(ControlRoomRestClient client)
            throws Exception
    {
        String attributeName1 = "Test attribute 1";
        String attributeDescription1 = "Test attribute description 1";
        Boolean userProvided1 = false;
        Boolean masked1 = false;
        Boolean passwordFlag1 = false;

        String attributeName2 = "Test attribute 2";
        String attributeDescription2 = "Test attribute description 2";
        Boolean userProvided2 = false;
        Boolean masked2 = false;
        Boolean passwordFlag2 = false;

        List<CredentialAttributePost> attributeDefinitions = new ArrayList<CredentialAttributePost>();
        CredentialAttributePost newAttribute1 = new CredentialAttributePost(attributeName1, attributeDescription1, userProvided1, masked1, passwordFlag1);
        CredentialAttributePost newAttribute2 = new CredentialAttributePost(attributeName2, attributeDescription2, userProvided2, masked2, passwordFlag2);

        attributeDefinitions.add(newAttribute1);
        attributeDefinitions.add(newAttribute2);

        String credentialName = "Test credential";
        String credentialDescription = "Test credential";
        CredentialPostRequest newCredential = new CredentialPostRequest(credentialName, credentialDescription);

        // Credential createdCredential = CredentialVault.createCredential(client, credentialName, credentialDescription, attributeDefinitions);
    }
     */

    public static void getAttributeValues(ControlRoomRestClient client)
            throws Exception
    {
        String credentialId = "24";
        String credentialAttributeId = "97";
        String userId = "";
        String encryptionKey = "";

        ApiResponse getAttributeValues = v2_CredentialVault.getAttributeValues(client, credentialId, credentialAttributeId, userId, encryptionKey);
    }

    public static void createAttributeValues(ControlRoomRestClient client)
            throws Exception
    {
        String credentialId = "27";
        Map<String, String> attributeValue1 = Map.of("credentialAttributeId", "82", "value", "Test value 1");
        Map<String, String> attributeValue2 = Map.of("credentialAttributeId", "83", "value", "Test value 2");

        List<Map<String, String>> listOfAttributeValues = new ArrayList<Map<String, String>>();
        listOfAttributeValues.add(attributeValue1);
        listOfAttributeValues.add(attributeValue2);

        // CredentialAttributeValuesResponse response = CredentialVault.createAttributeValues(client, credentialId, listOfAttributeValues, "");
    }

    public static void createLocker(ControlRoomRestClient client)
            throws Exception
    {
        String lockerName = "Test locker API";
        String lockerDescription = "Test locker API description";

        ApiResponse createLocker = v2_CredentialVault.createLocker(client, lockerName, lockerDescription);
    }

    public static void searchLockers(ControlRoomRestClient client)
            throws Exception
    {
        String filterRequestJson = "";
        boolean consumed = false;

        ApiResponse searchCredentials = v2_CredentialVault.searchLockers(client, filterRequestJson);
    }

    public static void addLockerMember(String sessionType, String sessionName, String lockerId, String userId, String memberPermission)
    {
        J_AddOrUpdateLockerMember command = new J_AddOrUpdateLockerMember();

        command.addOrUpdateLockerMember(sessionType, sessionName, lockerId, userId, memberPermission);
    }

    public static void getPublicKey(String sessionType, String sessionName)
            throws Exception
    {
        // PublicKeyResponse publicKey = v2_CredentialVault.getPublicKey(client);
    }

    public static void setDeviceCredentials(String sessionType, String sessionName, String deviceUsername, String devicePassword, String controlRoomUsername)
    {
        A_SetDeviceCredentials command = new A_SetDeviceCredentials();

        SecureString deviceUsernameInput = new SecureString(deviceUsername.getBytes(StandardCharsets.UTF_8));
        SecureString devicePasswordInput = new SecureString(devicePassword.getBytes(StandardCharsets.UTF_8));
        SecureString controlRoomUsernameInput = new SecureString(controlRoomUsername.getBytes(StandardCharsets.UTF_8));

        command.setDeviceCredentials(sessionType, sessionName, deviceUsernameInput, devicePasswordInput, controlRoomUsernameInput);
    }
}