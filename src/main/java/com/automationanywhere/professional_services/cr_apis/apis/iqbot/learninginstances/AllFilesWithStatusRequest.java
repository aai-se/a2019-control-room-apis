package com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.IQBotStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

public class AllFilesWithStatusRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/IQBot/gateway/learning-instances/[LEARNING_INSTANCE_ID]/files/list";

    // instance properties
    transient private String docStatus;

    public AllFilesWithStatusRequest(String learningInstanceId, IQBotStatus status)
    {
        // set instance properties
        this.docStatus = status.name();

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LEARNING_INSTANCE_ID]", learningInstanceId);
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get GET request object
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add params to request
        URI uri = new URIBuilder(request.getURI()).addParameter("docStatus", this.docStatus).build();
        request.setURI(uri);

        return request;
    }
}