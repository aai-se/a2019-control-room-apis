package com.automationanywhere.professional_services.cr_apis.apis.v3.workloadmanagement.workitems;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class WorkitemFilterRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v3/wlm/queues/[QUEUE_ID]/workitems/list";

    // instance properties
    public final FilterRequest filterRequest;

    public WorkitemFilterRequest(FilterRequest filterRequest, Long queueId)
    {
        // set instance properties
        this.filterRequest = filterRequest;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[QUEUE_ID]", String.valueOf(queueId));
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert filter request string to JSON and add to request
        String json = this.filterRequest.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}