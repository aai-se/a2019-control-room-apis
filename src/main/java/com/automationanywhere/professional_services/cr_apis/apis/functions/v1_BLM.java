package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v1.blm.ExportRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.blm.FileDetailsAndDependenciesRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.blm.BlmDownloadResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class v1_BLM
{
    public static ApiResponse getFileDetailsAndDependencies(ControlRoomRestClient client, Long[] fileIds)
            throws Exception
    {
        // create BLM export request object
        FileDetailsAndDependenciesRequest apiRequest = new FileDetailsAndDependenciesRequest(fileIds);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void export(ControlRoomRestClient client, String pathToOutputFile, String packageName, Long[] selectedFileIds, Long[] fileIds, Boolean excludeMetaBots,
                              String password)
            throws Exception
    {
        // create BLM status request object
        ExportRequest apiRequest = new ExportRequest(packageName, selectedFileIds, fileIds, excludeMetaBots, password);

        // get request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into BlmDownloadResponse object
        BlmDownloadResponse apiResponse = new BlmDownloadResponse(httpResponse);

        // close response
        httpResponse.close();

        // write data to output file
        apiResponse.writeDataToFile(pathToOutputFile);
    }
}
