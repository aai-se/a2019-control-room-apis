package com.automationanywhere.professional_services.cr_apis.apis.iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class FileDeletionRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.DELETE;
    transient public static final String ENDPOINT = "/IQBot/gateway/learning-instances/[LEARNING_INSTANCE_ID]/files/[FILE_ID]";

    public FileDeletionRequest(String learningInstanceId, String fileId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LEARNING_INSTANCE_ID]", learningInstanceId).replace("[FILE_ID]", fileId);
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get DELETE request object
        HttpDelete request = (HttpDelete) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}