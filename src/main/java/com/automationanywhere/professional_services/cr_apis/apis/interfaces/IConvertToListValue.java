package com.automationanywhere.professional_services.cr_apis.apis.interfaces;

import com.automationanywhere.botcommand.data.impl.ListValue;

public interface IConvertToListValue
{
    public ListValue convertToListValue()
            throws Exception;
}
