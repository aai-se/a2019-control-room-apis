package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.lockers;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class LockerAddCredentialRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.PUT;
    transient public static final String ENDPOINT = "/v2/credentialvault/lockers/[LOCKER_ID]/credentials/[CREDENTIAL_ID]";

    public LockerAddCredentialRequest(String lockerId, String credentialId)
    {

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LOCKER_ID]", lockerId).replace("[CREDENTIAL_ID]", credentialId);
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}