package com.automationanywhere.professional_services.cr_apis.apis.v3.migration;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class StartMigrationRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.POST;
    public static final String ENDPOINT = "/v3/migration/start";

    // instance properties
    public final String name;
    public final String description;
    public final Boolean overwriteBots;
    public final Long[] userIds;
    public final Long[] botIds;
    public final Long[] folderIds;
    public final Boolean includeChildFolders;
    public final Boolean legacyExcelCellRow;
    public final Boolean emailEwsSettings;
    public final String emailEwsExchangeVersion;
    public final String emailEwsAuthenticationType;
    public final Boolean citrixChannelTimeoutGvSetting;

    public StartMigrationRequest(String name, String description, Boolean overwriteBots, Long[] botIds, Long[] userIds,
                                 Boolean legacyExcelCellRow, Boolean includeChildFolders, Long[] folderIds, Boolean emailEwsSettings, String emailEwsExchangeVersion,
                                 String emailEwsAuthenticationType, Boolean citrixChannelTimeoutGvSetting)
    {
        // set instance properties
        this.name = name;
        this.description = description;
        this.overwriteBots = overwriteBots;
        this.userIds = userIds;
        this.botIds = botIds;
        this.folderIds = folderIds;
        this.includeChildFolders = includeChildFolders;

        if (legacyExcelCellRow == null)
        {
            this.legacyExcelCellRow = true;
        } else
        {
            this.legacyExcelCellRow = legacyExcelCellRow;
        }

        if (emailEwsSettings == null)
        {
            this.emailEwsSettings = false;
        } else
        {
            this.emailEwsSettings = emailEwsSettings;
        }

        if (emailEwsExchangeVersion == null || emailEwsExchangeVersion.isEmpty())
        {
            this.emailEwsExchangeVersion = "Exchange2010";
        } else
        {
            this.emailEwsExchangeVersion = emailEwsExchangeVersion;
        }

        if (emailEwsAuthenticationType == null || emailEwsAuthenticationType.isEmpty())
        {
            this.emailEwsAuthenticationType = "Basic";
        } else
        {
            this.emailEwsAuthenticationType = emailEwsAuthenticationType;
        }

        if (citrixChannelTimeoutGvSetting == null)
        {
            this.citrixChannelTimeoutGvSetting = false;
        }
        else
        {
            this.citrixChannelTimeoutGvSetting = citrixChannelTimeoutGvSetting;
        }

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert filter request string to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}