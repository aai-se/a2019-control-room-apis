package com.automationanywhere.professional_services.cr_apis.apis.v2.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.enums.WorkspaceType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class PackagesVersionUpdateRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v2/repository/files/packagesVersionUpdate";

    // instance properties
    public String body;


    public PackagesVersionUpdateRequest(String body)
    {
        // set instance properties
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
        this.body = body;
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert authentication object properties to JSON and add to request
        StringEntity jsonEntity = new StringEntity(this.body);
        request.setEntity(jsonEntity);

        return request;
    }
}