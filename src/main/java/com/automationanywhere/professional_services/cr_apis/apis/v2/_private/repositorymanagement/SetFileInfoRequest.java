package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class SetFileInfoRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.PUT;
    public static final String ENDPOINT = "/v2/repository/files/[FILE_ID]";

    // instance properties
    private String fileInfoJson;

    public SetFileInfoRequest(Long fileId, String fileInfoJson)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", fileId.toString());

        // set instance properties
        this.fileInfoJson = fileInfoJson;
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add file info to body
        StringEntity jsonEntity = new StringEntity(this.fileInfoJson);
        request.setEntity(jsonEntity);

        return request;
    }
}