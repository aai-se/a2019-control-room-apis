package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v3.workloadmanagement.queues.QueueFilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v3.workloadmanagement.workitems.WorkitemCreationRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v3.workloadmanagement.workitems.WorkitemFilterRequest;

public class v3_WorkloadManagement
{
    // QUEUES

    public static ApiResponse searchForQueues(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create queue search request object
        QueueFilterRequest apiRequest = new QueueFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    // WORKITEMS

    public static ApiResponse createWorkitems(ControlRoomRestClient client, Long queueId, Table workitemData)
            throws Exception
    {
        // create workitem creation request object
        WorkitemCreationRequest apiRequest = new WorkitemCreationRequest(queueId, workitemData);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse searchForWorkitems(ControlRoomRestClient client, String filterRequestJson, Long queueId)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create role search request object
        WorkitemFilterRequest apiRequest = new WorkitemFilterRequest(filterRequest, queueId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
