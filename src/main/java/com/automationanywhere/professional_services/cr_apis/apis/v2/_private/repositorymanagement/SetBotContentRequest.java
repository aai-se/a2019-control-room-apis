package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class SetBotContentRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.PUT;
    public static final String ENDPOINT = "/v2/repository/files/[FILE_ID]/content";

    // instance properties
    private String botContent;
    private boolean hasErrors;

    public SetBotContentRequest(Long fileId, String botContent, boolean hasErrors)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", String.valueOf(fileId));

        // set instance properties
        this.botContent = botContent;
        this.hasErrors = hasErrors;
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create parameter
        List<NameValuePair> parameters = new ArrayList<>();
        if(hasErrors){
            parameters.add(new BasicNameValuePair("hasErrors", "true"));
        }else {
            parameters.add(new BasicNameValuePair("hasErrors", "false"));
        }

        // create PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, parameters);

        // add bot content to body
        StringEntity jsonEntity = new StringEntity(this.botContent);
        request.setEntity(jsonEntity);

        return request;
    }
}