package com.automationanywhere.professional_services.cr_apis.apis.generic;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IApiRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 Abstract class to use as a base for all request objects.
 */
public abstract class BaseRequest
        extends BaseJsonObject
        implements IApiRequest
{
    // instance properties
    transient public RequestType requestType;
    transient public String endpoint;

    /**
     Protected method for creating a request object. Can only be called by subclasses.

     @param controlRoomUrl Control Room URL
     @param authToken      authentication token (if required)

     @return HTTP request object (subclass of org.apache.http.clients.methods.HttpRequestBase)

     @throws InvalidHttpTypeException exception thrown if an invalid HTTP type is set
     @throws URISyntaxException       exception thrown if URI syntax is invalid
     */
    protected HttpRequestBase createDefaultRequestObject(String controlRoomUrl, String authToken, List<NameValuePair> parameters)
            throws InvalidHttpTypeException, URISyntaxException
    {
        // create path to endpoint
        URIBuilder builder = new URIBuilder(controlRoomUrl).setPath(this.endpoint);

        // add parameters if required
        if (parameters != null)
        {
            builder.addParameters(parameters);
        }

        // generate full path
        URI path = builder.build();

        // define variable to hold request object
        HttpRequestBase request;

        // create request object based on request type, including full path to REST endpoint
        switch (this.requestType)
        {
            case GET:
                request = new HttpGet(path);
                break;
            case POST:
                request = new HttpPost(path);
                break;
            case PUT:
                request = new HttpPut(path);
                break;
            case DELETE:
                request = new HttpDelete(path);
                break;
            default:
                throw new InvalidHttpTypeException();
        }

        // add default headers
        // TODO: change this if needing to implement non-JSON API requests! (e.g. BLM)
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-Type", "application/json");

        // add authentication token as header if required (all API calls except initial authentication request)
        if (authToken != null && !authToken.isEmpty())
        {
            request.setHeader("X-Authorization", authToken);
        }

        // return request object
        return request;
    }

    /**
     Protected method for creating a request object. Can only be called by subclasses.

     @param controlRoomUrl Control Room URL
     @param authToken      authentication token (if required)

     @return HTTP request object (subclass of org.apache.http.clients.methods.HttpRequestBase)

     @throws InvalidHttpTypeException exception thrown if an invalid HTTP type is set
     @throws URISyntaxException       exception thrown if URI syntax is invalid
     */
    protected HttpPost createFormMultipartRequest(String controlRoomUrl, String authToken)
            throws InvalidHttpTypeException, URISyntaxException
    {
        // create path to endpoint
        URIBuilder builder = new URIBuilder(controlRoomUrl).setPath(this.endpoint);

        // generate full path
        URI path = builder.build();

        // define variable to hold request object
        HttpPost request = new HttpPost(path);

        // add authentication token as header
        request.setHeader("X-Authorization", authToken);

        // return request object
        return request;
    }
}