package com.automationanywhere.professional_services.cr_apis.apis.v2.aari;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;

import java.util.List;

public class TeamBasic
        extends BaseJsonObject
{
    private String name;
    private String description;
    private List<Long> userIds;
    private List<Long> processIds;

    public TeamBasic(String name, String description, List<Long> userIds, List<Long> processIds)
    {
        this.name = name;
        this.description = description;
        this.userIds = userIds;
        this.processIds = processIds;
    }
}