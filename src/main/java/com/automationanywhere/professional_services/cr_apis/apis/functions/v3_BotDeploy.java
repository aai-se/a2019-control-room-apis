package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.v3.botdeploy.DeployRequest;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.util.EntityUtils;

public class v3_BotDeploy
{

    public static String deploy(ControlRoomRestClient client, int fileId, int[] runAsUserIds, int[] poolIds, boolean overrideDefaultDevice,
                                int numOfRunAsUsersToUse)
            throws Exception
    {
        // create deploy request object
        DeployRequest apiRequest = new DeployRequest(fileId, runAsUserIds, poolIds, overrideDefaultDevice, numOfRunAsUsersToUse);

        // get request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert body from response into string
        String deploymentId = EntityUtils.toString(httpResponse.getEntity());

        // close response
        httpResponse.close();

        // return deployment ID
        return deploymentId;
    }
}