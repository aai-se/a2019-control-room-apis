package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement.*;

import java.util.List;

public class _private_v2_RepositoryManagement
{
    public static ApiResponse getBotContent(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create request object
        GetBotContentRequest apiRequest = new GetBotContentRequest(fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse setBotContent(ControlRoomRestClient client, Long fileId, String botContent, boolean hasErrors)
            throws Exception
    {
        // create request object
        SetBotContentRequest apiRequest = new SetBotContentRequest(fileId, botContent, hasErrors);

        // execute request only
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse copyRepositoryFile(ControlRoomRestClient client, Long fileId, String name, Long parentId)
            throws Exception
    {
        // create request object
        CopyRepositoryFileRequest apiRequest = new CopyRepositoryFileRequest(fileId, name, parentId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse createRepositoryFolder(ControlRoomRestClient client, Long folderId, String folderName)
            throws Exception
    {
        // create request object
        CreateRepositoryFolderRequest apiRequest = new CreateRepositoryFolderRequest(folderId, folderName);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getRepositoryFolder(ControlRoomRestClient client, Long folderId)
            throws Exception
    {
        // create request object
        GetRepositoryFolderRequest apiRequest = new GetRepositoryFolderRequest(folderId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getFileInfo(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create request object
        GetFileInfoRequest apiRequest = new GetFileInfoRequest(fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse setFileInfo(ControlRoomRestClient client, Long fileId, String fileInfoJson)
            throws Exception
    {
        // create request object
        SetFileInfoRequest apiRequest = new SetFileInfoRequest(fileId, fileInfoJson);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse checkOut(ControlRoomRestClient client, Long fileId, List<Long> dependencyIds, Long versionNumber)
            throws Exception
    {
        // create request object
        CheckOutRequest apiRequest = new CheckOutRequest(fileId, dependencyIds, versionNumber);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void cancelCheckout(ControlRoomRestClient client, List<Long> fileIds)
            throws Exception
    {
        // create request object
        CancelCheckOutRequest apiRequest = new CancelCheckOutRequest(fileIds);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static ApiResponse checkIn(ControlRoomRestClient client, String description, List<Long> fileIds)
            throws Exception
    {
        // create request object
        CheckInRequest apiRequest = new CheckInRequest(description, fileIds);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void deleteFile(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create request object
        DeleteFileRequest apiRequest = new DeleteFileRequest(fileId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static ApiResponse getFileDownstreamDependencies(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create request object
        GetFileDownstreamDependenciesRequest apiRequest = new GetFileDownstreamDependenciesRequest(fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getFileUpstreamDependents(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create request object
        GetFileUpstreamDependentsRequest apiRequest = new GetFileUpstreamDependentsRequest(fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse setBotDependencies(ControlRoomRestClient client, Long fileId, List<Long> childFileIds)
            throws Exception
    {
        // create request object
        SetFileDependenciesRequest apiRequest = new SetFileDependenciesRequest(fileId, childFileIds);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getDependenciesForFiles(ControlRoomRestClient client, List<Long> fileIds)
            throws Exception
    {
        // create request object
        GetDependenciesForFilesRequest apiRequest = new GetDependenciesForFilesRequest(fileIds);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}