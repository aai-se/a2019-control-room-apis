package com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.users;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;
import com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.roles.RoleId;

import java.util.ArrayList;
import java.util.List;

public class UserDetailsModify
        extends BaseJsonObject
{
    // instance vars
    public final List<RoleId> roles;
    public final String domain;
    public final String email;
    public final Boolean enableAutoLogin;
    public final String username;
    public final String firstName;
    public final String lastName;
    public final String description;
    public final Boolean disabled;
    public final String password;
    private List<String> licenseFeatures;

    public UserDetailsModify(boolean newUser, String username, String password, String email, List<Long> roleIds, String firstName,
                             String lastName, String domain, String description, List<String> licenseFeatures, Boolean enableAutoLogin, Boolean disabled)
            throws Exception
    {
        // populate mandatory variables (exceptions will be thrown by the CR if these are not populated)
        this.roles = new ArrayList<RoleId>();

        for (Long roleIdValue : roleIds)
        {
            RoleId roleId = new RoleId(roleIdValue);
            this.roles.add(roleId);
        }

        this.email = email;

        // populate username for new user creation; ignore username for existing user creation
        if (newUser)
        {
            if (username == null || username.isEmpty())
            {
                throw new Exception("Username field is mandatory for a new user!");
            } else
            {
                this.username = username;
            }
        } else
        {
            this.username = null;
        }

        // populate optional variables - set to null if not supplied for omission in the request to the CR
        if (password != null && !password.isEmpty())
        {
            this.password = password;
        } else
        {
            this.password = null;
        }

        if (firstName != null && !firstName.isEmpty())
        {
            this.firstName = firstName;
        } else
        {
            this.firstName = null;
        }

        if (lastName != null && !lastName.isEmpty())
        {
            this.lastName = lastName;
        } else
        {
            this.lastName = null;
        }

        if (domain != null && !domain.isEmpty())
        {
            this.domain = domain;
        } else
        {
            this.domain = null;
        }

        if (description != null && !description.isEmpty())
        {
            this.description = description;
        } else
        {
            this.description = null;
        }

        // create license features list
        if (licenseFeatures != null && !licenseFeatures.isEmpty())
        {
            this.licenseFeatures = licenseFeatures;
        } else
        {
            this.licenseFeatures = null;
        }

        if (enableAutoLogin != null)
        {
            this.enableAutoLogin = enableAutoLogin;
        } else
        {
            this.enableAutoLogin = null;
        }

        if (disabled != null)
        {
            this.disabled = disabled;
        } else
        {
            this.disabled = null;
        }
    }
}