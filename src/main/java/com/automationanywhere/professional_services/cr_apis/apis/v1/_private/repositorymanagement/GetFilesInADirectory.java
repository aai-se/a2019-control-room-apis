package com.automationanywhere.professional_services.cr_apis.apis.v1._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class GetFilesInADirectory
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.POST;
    public static final String ENDPOINT = "/v1/repository/directories/[FOLDER_ID]/files/list";
    public final FilterRequest filterRequest;

    public GetFilesInADirectory(Long folderId, FilterRequest filterRequest)
    {
        // set instance properties
        this.filterRequest = filterRequest;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FOLDER_ID]", folderId.toString());
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);
        // convert authentication object properties to JSON and add to request
        String json = this.filterRequest.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}