package com.automationanywhere.professional_services.cr_apis.apis.enums;

public enum LockerMemberPermission
{
    PARTICIPATE, MANAGE, OWN
}