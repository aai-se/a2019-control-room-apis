package com.automationanywhere.professional_services.cr_apis.apis.generic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.professional_services.utilities.converters.JsonConverter;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ApiResponse
{

    public final String json;

    private JSONObject jsonObject;
    private JSONArray jsonArray;

    private DictionaryValue dictValue;
    private ListValue listValue;

    public ApiResponse(CloseableHttpResponse response)
            throws IOException
    {
        // extract JSON
        this.json = EntityUtils.toString(response.getEntity());
    }

    public JSONObject getJsonObject()
    {
        // check if JSONObject has been created from JSON data - if not, do this now
        if (this.jsonObject == null)
        {
            this.jsonObject = new JSONObject(this.json);
        }

        // return instantiated JSON object
        return this.jsonObject;
    }

    public JSONArray getJsonArray()
    {
        // check if JSONArray has been created from JSON data - if not, do this now
        if (this.jsonArray == null)
        {
            this.jsonArray = new JSONArray(this.json);
        }

        // return instantiated JSON object
        return this.jsonArray;
    }

    public DictionaryValue getDictionaryValue()
            throws Exception
    {
        // check if DictionaryValue has been created from JSON data - if not, do this now
        if (this.dictValue == null)
        {
            // convert JSON to map
            Map<String, Value> map = JsonConverter.convertJsonToMapOfValues(this.json);

            // set DictionaryValue object
            this.dictValue = new DictionaryValue(map);
        }

        // return instantiated dictionary
        return this.dictValue;
    }

    public ListValue getListValue()
            throws Exception
    {
        // check if ListValue has been created from JSON data - if not, do this now
        if (this.dictValue == null)
        {
            // convert JSON to list of values
            List<Value> list = JsonConverter.convertJsonToListOfValues(this.json);

            // set ListValue object
            this.listValue = new ListValue();
            this.listValue.set(list);
        }

        // return instantiated list
        return this.listValue;
    }
}