package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.IQBotStatus;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances.AllFilesUploadedRequest;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances.AllFilesWithStatusRequest;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances.AllGroupsDataRequest;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances.LearningInstancesListRequest;

public class IQBot_LearningInstances
{
    public static ApiResponse getLearningInstancesList(ControlRoomRestClient client)
            throws Exception
    {
        // create file upload request object
        LearningInstancesListRequest apiRequest = new LearningInstancesListRequest();

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getAllFilesUploadedToLearningInstance(ControlRoomRestClient client, String learningInstanceId)
            throws Exception
    {
        // create request object
        AllFilesUploadedRequest apiRequest = new AllFilesUploadedRequest(learningInstanceId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getAllFilesWithStatus(ControlRoomRestClient client, String learningInstanceId, IQBotStatus status)
            throws Exception
    {
        // create request object
        AllFilesWithStatusRequest apiRequest = new AllFilesWithStatusRequest(learningInstanceId, status);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getGroupsAndStatuses(ControlRoomRestClient client, String learningInstanceId)
            throws Exception
    {
        // create request object
        AllGroupsDataRequest apiRequest = new AllGroupsDataRequest(learningInstanceId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
