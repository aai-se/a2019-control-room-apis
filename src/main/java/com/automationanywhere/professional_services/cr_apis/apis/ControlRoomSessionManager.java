package com.automationanywhere.professional_services.cr_apis.apis;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.utilities.globals.GlobalVariablesProvider;

public class ControlRoomSessionManager
{

    public static void setSession(String sessionName, ControlRoomRestClient client)
            throws Exception
    {
        GlobalVariablesProvider.INSTANCE.addValue(sessionName, client);
    }

    public static ControlRoomRestClient getSession(String sessionName)
            throws Exception
    {
        return (ControlRoomRestClient) GlobalVariablesProvider.INSTANCE.retrieveValue(sessionName);
    }

    public static boolean checkIfSessionExists(String sessionName)
    {
        return GlobalVariablesProvider.INSTANCE.checkIfVaLueExists(sessionName);
    }

    public static void deleteSession(String sessionName)
            throws Exception
    {
        GlobalVariablesProvider.INSTANCE.removeValue(sessionName);
    }
}