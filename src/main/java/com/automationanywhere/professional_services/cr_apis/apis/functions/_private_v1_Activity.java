package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v1._private.activity.ActivityListRequest;

public class _private_v1_Activity
{
    public static ApiResponse listActivities(ControlRoomRestClient client)
            throws Exception
    {

        // create credential search request object
        ActivityListRequest apiRequest = new ActivityListRequest();

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}