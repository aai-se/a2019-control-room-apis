package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.LockerMemberPermission;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributePost;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributeValuePost;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributeValuePostListRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributeValuesRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.credentials.CredentialFilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.credentials.CredentialOwnershipUpdateRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.credentials.CredentialPostRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.keys.PublicKeyRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.lockers.*;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.loginsetting.LoginSettingFetch;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.loginsetting.LoginSettingRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.loginsetting.LoginSettingRequestWithUserId;

import java.util.List;

public class v2_CredentialVault
{
    // CREDENTIALS

    public static ApiResponse searchForCredentials(ControlRoomRestClient client, String filterRequestJson, boolean consumed)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        CredentialFilterRequest apiRequest = new CredentialFilterRequest(filterRequest, consumed);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse createCredential(ControlRoomRestClient client, String credentialName, String credentialDescription, List<CredentialAttributePost> attributeDefinitions)
            throws Exception
    {
        // create credential search request object
        CredentialPostRequest apiRequest = new CredentialPostRequest(credentialName, credentialDescription);

        // add attribute definitions if provided
        if (attributeDefinitions != null)
        {
            // loop through attribute definitions to convert to objects and add to request
            for (CredentialAttributePost attributeDefinition : attributeDefinitions)
            {
                apiRequest.addAttribute(attributeDefinition);
            }
        }

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void getCredential()
    {
    }

    public static void updateCredential()
    {
    }

    public static void deleteCredential()
    {
    }

    public static void updateCredentialOwnership(ControlRoomRestClient client, String credentialId, String credentialOwnerId)
            throws Exception
    {
        // create locker credential addition request object
        CredentialOwnershipUpdateRequest apiRequest = new CredentialOwnershipUpdateRequest(credentialId, credentialOwnerId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }


    // ATTRIBUTE VALUES

    public static ApiResponse getAttributeValues(ControlRoomRestClient client, String credentialId, String credentialAttributeId, String userId,
                                                 String encryptionKey)
            throws Exception
    {
        // create credential search request object
        CredentialAttributeValuesRequest apiRequest = new CredentialAttributeValuesRequest(credentialId, credentialAttributeId, userId, encryptionKey);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse createAttributeValues(ControlRoomRestClient client, String credentialId, List<CredentialAttributeValuePost> attributeValues,
                                                    String encryptionKey)
            throws Exception
    {
        // create attribute creation request object
        CredentialAttributeValuePostListRequest apiRequest = new CredentialAttributeValuePostListRequest(credentialId, attributeValues, encryptionKey);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void updateAttributeValue()
    {
    }

    public static void deleteAttributeValue()
    {
    }


    // LOCKERS

    public static ApiResponse createLocker(ControlRoomRestClient client, String lockerName, String lockerDescription)
            throws Exception
    {
        // create credential search request object
        LockerPostRequest apiRequest = new LockerPostRequest(lockerName, lockerDescription);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse searchLockers(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create locker search request object
        LockerFilterRequest apiRequest = new LockerFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void getLocker()
    {
    }

    public static void updateLocker()
    {
    }

    public static void deleteLocker()
    {
    }

    public static ApiResponse getLockerConsumers(ControlRoomRestClient client, String lockerId)
            throws Exception
    {
        // create locker consumers request object
        LockerGetConsumersRequest apiRequest = new LockerGetConsumersRequest(lockerId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void addLockerConsumer(ControlRoomRestClient client, String lockerId, String roleId)
            throws Exception
    {
        // create locker credential addition request object
        LockerAddConsumerRequest apiRequest = new LockerAddConsumerRequest(lockerId, roleId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void removeLockerConsumer()
    {
    }

    public static void getLockerMembers()
    {
    }

    public static void addOrUpdateLockerMember(ControlRoomRestClient client, String lockerId, String userId, LockerMemberPermission memberPermission)
            throws Exception
    {
        // create locker credential addition request object
        LockerAddOrUpdateMemberRequest apiRequest = new LockerAddOrUpdateMemberRequest(lockerId, userId, memberPermission);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void removeLockerMember()
    {
    }

    public static void getLockerCredentials()
    {
    }

    public static void addCredentialToLocker(ControlRoomRestClient client, String lockerId, String credentialId)
            throws Exception
    {
        // create locker credential addition request object
        LockerAddCredentialRequest apiRequest = new LockerAddCredentialRequest(lockerId, credentialId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void removeCredentialFromLocker()
    {
    }


    // MODES

    public static void getCurrentMode()
    {
    }

    public static void updateCurrentMode()
    {
    }


    // KEYS

    public static void createKeyPair()
    {
    }

    public static void saveKeyPairAndMode()
    {
    }

    public static void getPrivateKeyStatus()
    {
    }

    public static void applyPrivateKey()
    {
    }

    public static ApiResponse getPublicKey(ControlRoomRestClient client)
            throws Exception
    {
        // create credential search request object
        PublicKeyRequest apiRequest = new PublicKeyRequest();

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }


    // LOGIN SETTING

    public static void setDeviceCredentials(ControlRoomRestClient client, String loginUsername, String loginPassword, String controlRoomUsername)
            throws Exception
    {
        // create credential search request object
        LoginSettingRequest apiRequest = new LoginSettingRequest(loginUsername, loginPassword, controlRoomUsername);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void setDeviceCredentials(ControlRoomRestClient client, String loginUsername, String loginPassword, Long userid)
            throws Exception
    {
        // create credential search request object
        LoginSettingRequestWithUserId apiRequest = new LoginSettingRequestWithUserId(loginUsername, loginPassword, userid);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static ApiResponse getDeviceCredentials(ControlRoomRestClient client, Long[] userIds)
            throws Exception
    {
        // create credential search request object
        LoginSettingFetch apiRequest = new LoginSettingFetch(userIds);

        // execute request only
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
