package com.automationanywhere.professional_services.cr_apis.apis.v2.blm;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class BlmDownloadResponse
{
    public final byte[] downloadData;

    public BlmDownloadResponse(CloseableHttpResponse response)
            throws IOException
    {
        // copy data from response
        this.downloadData = EntityUtils.toByteArray(response.getEntity());
    }

    public void writeDataToFile(String pathToFile)
            throws Exception
    {
        Files.write(Path.of(pathToFile), this.downloadData);
    }
}