package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1._private.repositorymanagement.GetFilesInADirectory;

public class _private_v1_RepositoryManagement {

    public static ApiResponse searchForFilesInAFolder(ControlRoomRestClient client, String filterRequestJson, Long folderId)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        GetFilesInADirectory apiRequest = new GetFilesInADirectory(folderId, filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
