package com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.users;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class UpdateUserRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.PUT;
    transient public static final String ENDPOINT = "/v1/usermanagement/users/[USER_ID]";

    // instance properties
    public final UserDetailsModify userDetails;

    public UpdateUserRequest(UserDetailsModify userDetails, String userId)
    {
        // set instance properties
        this.userDetails = userDetails;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[USER_ID]", userId);
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert authentication object properties to JSON and add to request
        String json = this.userDetails.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}