package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.WorkspaceType;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.repositorymanagement.FolderItemsFilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.repositorymanagement.PackagesVersionUpdateRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.repositorymanagement.WorkspaceItemsFilterRequest;

public class v2_RepositoryManagement
{
    public static ApiResponse getItemsInFolder(ControlRoomRestClient client, Long folderId, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        FolderItemsFilterRequest apiRequest = new FolderItemsFilterRequest(folderId, filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getItemsInWorkspace(ControlRoomRestClient client, WorkspaceType workspaceType, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        WorkspaceItemsFilterRequest apiRequest = new WorkspaceItemsFilterRequest(workspaceType, filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse updatePackages(ControlRoomRestClient client, String body)
            throws Exception
    {
        // create credential search request object
        PackagesVersionUpdateRequest apiRequest = new PackagesVersionUpdateRequest(body);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
