package com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.roles;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;

public class RoleId
        extends BaseJsonObject
{
    public final long id;

    public RoleId(long id)
    {
        this.id = id;
    }
}