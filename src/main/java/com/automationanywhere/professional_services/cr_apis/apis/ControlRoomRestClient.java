package com.automationanywhere.professional_services.cr_apis.apis;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidAuthenticationMethodException;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_Authentication;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.enums.AuthenticationMethod;
import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.professional_services.cr_apis.apis.v1.authentication.AuthRequest;
import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.KeyStore;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 Implementation of central class to hold details corresponding to a single Control Room connection.
 */
public class ControlRoomRestClient
{
    // class properties
    public static final String DEFAULT_CONNECT_TIMEOUT = "60000";
    public static final String DEFAULT_SOCKET_TIMEOUT = "60000";
    public static final String DEFAULT_MAX_ATTEMPTS = "3";
    public static final String DEFAULT_EXPONENT_LIMIT = "4";

    public int connectionTimeOutValue;
    public int socketTimeOutValue;
    public int connectionRequestTimeout = 60000;
    private SSLContext ctx;


    // instance properties
    public final SessionType sessionType;
    public final GlobalSessionContext globalSessionContext;
    public final String controlRoomUrl;
    public final AuthRequest authRequest;
    public CloseableHttpClient client;
    private ApiResponse authResponse;
    private String token;
    private int maxAttempts;
    private int exponentLimit;
    private RandomDataGenerator randomNumberGenerator = new RandomDataGenerator();
    public  PrintWriter externalLogWriter;

    public boolean retryAllFailures = false;
    public boolean useWinCertStore = false;
    public boolean autoIncreaseTimeOutsOnFailure = true;

    /**
     Constructor to create new ControlRoomRestClient object.

     @param controlRoomUrl URL to Control Room
     @param username       username
     @param authInput      input for authentication (i.e. a password or an API key)
     @param authMethod     String containing either "PASSWORD" or "API_KEY"
     */
    public ControlRoomRestClient(SessionType sessionType, GlobalSessionContext globalSessionContext, String controlRoomUrl, String username, String authMethod, String authInput)
            throws Exception

    {

        // store session type and global session context
        this.sessionType = sessionType;

        // create new client object using default connection parameters
        this.connectionTimeOutValue = Integer.valueOf(DEFAULT_CONNECT_TIMEOUT);
        this.socketTimeOutValue = Integer.valueOf(DEFAULT_CONNECT_TIMEOUT);
        this.maxAttempts = Integer.valueOf(DEFAULT_MAX_ATTEMPTS);
        this.exponentLimit = Integer.valueOf(DEFAULT_EXPONENT_LIMIT);


        this.configureConnectionAndCreateClient(this.connectionTimeOutValue, this.socketTimeOutValue, this.maxAttempts, this.exponentLimit);

        // store token as empty string
        this.token = "";

        if (sessionType == SessionType.BOT_RUNNER_SESSION)
        {
            // store global session context for future reference
            this.globalSessionContext = globalSessionContext;

            // set Control Room URL using global session context
            this.controlRoomUrl = this.globalSessionContext.getCrUrl();

            // do not create AuthRequest object
            this.authRequest = null;

        } else
        {
            // do not store global session context
            this.globalSessionContext = null;

            // set Control Room URL based on input
            this.controlRoomUrl = controlRoomUrl;

            // create AuthRequest object
            this.authRequest = new AuthRequest(username, authInput, AuthenticationMethod.valueOf(authMethod.toUpperCase()));
        }
    }

    public synchronized void configureConnectionAndCreateClient(int connectTimeout, int socketTimeout, int connectionRequestTimeout, int maxAttempts, int exponentLimit) throws Exception {
        // record new value for max attempts
        this.maxAttempts = maxAttempts;

        // record new value for exponent limit
        this.exponentLimit = exponentLimit;

        this.connectionTimeOutValue = connectTimeout;
        this.socketTimeOutValue = socketTimeout;
        this.connectionRequestTimeout = connectionRequestTimeout;

        // create request config using updated timeout parameters
        RequestConfig requestConfig = RequestConfig
                .custom()
                .setConnectionRequestTimeout(connectionRequestTimeout)
                .setConnectTimeout(connectTimeout)
                .setSocketTimeout(socketTimeout)
                .build();

        // create client using config
        if(useWinCertStore){
            if(ctx == null){
                setSSLContextForWinTrustStore();
            }
            this.client = HttpClientBuilder.create()
                    .setSSLContext(ctx)
                    .setDefaultRequestConfig(requestConfig)
                    .build();
        }else{
            this.client = HttpClientBuilder.create()
                    .setDefaultRequestConfig(requestConfig)
                    .build();
        }
    }
    public synchronized void configureConnectionAndCreateClient(int connectTimeout, int socketTimeout, int maxAttempts, int exponentLimit) throws Exception {
        configureConnectionAndCreateClient(connectTimeout, socketTimeout, this.connectionRequestTimeout, maxAttempts, exponentLimit);
    }

    public synchronized void configureConnectionAndCreateClient(int connectTimeout, int socketTimeout, int connectionRequestTimeout, int maxAttempts, int exponentLimit, PrintWriter logWriter) throws Exception {
        // save external log writer
        this.externalLogWriter = logWriter;
        // record new value for max attempts
        configureConnectionAndCreateClient(connectTimeout, socketTimeout, connectionRequestTimeout, maxAttempts, exponentLimit);
    }

    public void useWindowsSSLCertificateStore() throws Exception {
        this.useWinCertStore = true;
        configureConnectionAndCreateClient(connectionTimeOutValue, socketTimeOutValue, connectionRequestTimeout, maxAttempts, exponentLimit);
    }

    private void setSSLContextForWinTrustStore() throws Exception {
        KeyStore ks = KeyStore.getInstance("Windows-MY");
        ks.load(null, null);

        KeyStore ts = KeyStore.getInstance("Windows-ROOT");
        ts.load(null, null);

        TrustManagerFactory tmf = TrustManagerFactory
                .getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ts);

        KeyManagerFactory kmf = KeyManagerFactory
                .getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, new char[0]);

        ctx = SSLContext.getInstance("TLS");
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    }

    public synchronized ApiResponse getAuthResponse()
    {
        return authResponse;
    }

    /**
     Method to execute authentication against Control Room using authentication details stored from constructor.
     */
    public synchronized void updateToken()
            throws Exception
    {
        // perform full authentication if not using bot runner session
        if (this.sessionType == SessionType.CUSTOM_SESSION)
        {
            // confirm object properties have been set
            if (this.authRequest == null)
            {
                throw new Exception("AuthRequest object has not been created. Please construct a ControlRoomRestClient object with the required parameters.");
            }

            // attempt to authenticate
            try
            {
                this.authResponse = v1_Authentication.authenticateUser(this);
                JSONObject jsonObj = this.authResponse.getJsonObject();
                this.token = jsonObj.getString("token");
            } catch (Exception ex)
            {
                throw new Exception("An error occurred during authentication: " + ex.getMessage());
            }
        } else
        {
            // store token from bot runner session
            this.token = this.globalSessionContext.getUserToken();
        }
    }

    /**
     Method to return the authentication token for this client. Executes authentication if not already performed.

     @return authentication token
     */
    public synchronized String getAuthToken()
    {
        // return token
        return this.token;
    }

    /**
     * Overrides an existing token, for cached executions and for old systems
     * @param token
     */
    public void setAuthToken(String token){
        this.token = token;
    }

    /**
     Method to execute an HTTP request and return the response. Throws an exception if the request is not successful.

     @param request HTTP request to be executed

     @return HTTP response
     */
    public synchronized CloseableHttpResponse executeRequest(HttpRequestBase request)
            throws Exception
    {
        // instantiate request object
        CloseableHttpResponse response = null;
        String failureResponseBody = "";

        // set variables to track attempts
        int attempts = 0;
        int delayInMilliseconds = 0;
        int responseCode = 0;

        // keep trying until the number of attempts made reaches the maximum number of attempts allowed
        while (attempts < this.maxAttempts)
        {
            try
            {
                // if an attempt has already been made, add a delay using exponential backoff
                if (attempts > 0)
                {
                    delayInMilliseconds = getDelayInMilliseconds(attempts);
                    Thread.sleep(delayInMilliseconds);
                }

                // attempt to execute request
                attempts++;
                response = this.client.execute(request);

                // get response code
                responseCode = response.getStatusLine().getStatusCode();

                // handle scenario where response code 401 is encountered to check if a stale token caused a failure
                if (responseCode == 401)
                {

                    //  check if a token was required for the request
                    //  this excludes a retry for a login attempt with an incorrect username and password
                    boolean tokenRequired = request.getHeaders("X-Authorization").length == 1;

                    if (tokenRequired)
                    {

                        // check if current token is valid
                        // this excludes a retry for a scenario where the token is valid and the user truly lacks permission to make this request
                        boolean tokenValid = v1_Authentication.verifyToken(this, this.token);

                        if (!tokenValid)
                        {

                            // update token
                            this.updateToken();

                            // reset token in request header
                            request.setHeader("X-Authorization", this.token);

                            // execute request
                            // response = this.client.execute(request);

                            //close the previous response
                            if(response != null){
                                try{
                                    EntityUtils.consume(response.getEntity());
                                }catch (Exception e){
                                    printStackTraces(e);
                                }
                            }

                            // execute request and return response via recursion
                            return this.executeRequest(request);

                            // update response code
                            // responseCode = response.getStatusLine().getStatusCode();
                        }
                    }
                }

                // break loop to return response if the request was successful (2xx)
                if (responseCode >= 200 && responseCode <= 299)
                {
                    return response;
                }
                // throw exception if a client error was returned (4xx)
                else if (responseCode >= 400 && responseCode <= 499)
                {
                    // try to get the response body in the exception message
                    failureResponseBody = getResponseBody(response);

                    String exceptionMessage = String.format("%s failed with the following status: %d with the following reason: %s and the response body was: %s",
                            request.getClass().getSimpleName(),
                            responseCode,
                            response.getStatusLine().getReasonPhrase(),
                            failureResponseBody
                    );
                    printStackTraces(exceptionMessage);
                    if(!retryAllFailures){
                        throw new Exception(exceptionMessage);
                    }
                }
                // throw exception if an informational response (1xx) or a redirect (3xx) was encountered - not expected
                else if ((responseCode >= 100 && responseCode <= 199) || (responseCode >= 300 && responseCode <= 399))
                {
                    // try to get the response body in the exception message
                    failureResponseBody = getResponseBody(response);
                    printStackTraces(failureResponseBody);

                    throw new Exception("PLEASE REPORT THIS TO THE DEVELOPER // "
                            + request.getClass().getSimpleName()
                            + " provided an unexpected status: "
                            + responseCode + " "
                            + "with the following reason: "
                            + response.getStatusLine().getReasonPhrase()
                            + " the response body was: " +
                            failureResponseBody);
                }
                // allow server errors (5xx) to proceed through retry loop
                else
                {
                    // try to get the response body in the exception message
                    failureResponseBody = getResponseBody(response);
                    String exceptionMessage = String.format("%s failed with the following status: %d with the following reason: %s and the response body was: %s",
                            request.getClass().getSimpleName(),
                            responseCode,
                            response.getStatusLine().getReasonPhrase(),
                            failureResponseBody
                    );
                    printStackTraces(exceptionMessage);
                }

            } catch(SocketTimeoutException ex) {
                // a timeout occurred - increment the number of attempts made
                printStackTraces(ex);
                
                if(autoIncreaseTimeOutsOnFailure){
                    // increase the timeouts
                    this.socketTimeOutValue = increaseTimeOutValue(this.socketTimeOutValue);

                    // reconfigure the client
                    this.configureConnectionAndCreateClient(this.connectionTimeOutValue, this.socketTimeOutValue, this.maxAttempts, this.exponentLimit);
                }
            } catch (SocketException socketException){
                printStackTraces(socketException);

            }
            catch (ConnectTimeoutException ex) {
                // a timeout occurred - increment the number of attempts made
                printStackTraces(ex);
                if(autoIncreaseTimeOutsOnFailure){
                    // increase the timeouts
                    this.connectionTimeOutValue = increaseTimeOutValue(this.connectionTimeOutValue);

                    // reconfigure the client
                    this.configureConnectionAndCreateClient(this.connectionTimeOutValue, this.socketTimeOutValue, this.maxAttempts, this.exponentLimit);
                }
            } catch (Exception ex)
            {
                printStackTraces(ex);
                // an unexpected exception occurred - throw it
                if(!retryAllFailures){
                    throw new Exception("Unexpected exception encountered: " + ex.getMessage());
                }
            }
            //close the previous response
            try{
                if(response != null){
                    EntityUtils.consume(response.getEntity());
                }
            }catch (Exception e){
            }
        }


        // throw an exception if the maximum number of attempts was reached
        // no possibility of this code being reached with a successful request
        if (attempts == this.maxAttempts)
        {
            String exceptionMessage = "";
            // try to get the response body in the exception message
            try {
                if(response != null){
                    // break loop to return response if the request was successful (2xx)
                    if (responseCode >= 200 && responseCode <= 299)
                    {
                        return response;
                    }else{
                        failureResponseBody = getResponseBody(response);
                        exceptionMessage = String.format("Limit of '%d' timeouts reached without a successful response, last response code was: '%d', last http failure reason was: '%s' and response body was: '%s'",
                                this.maxAttempts,
                                response.getStatusLine().getStatusCode(),
                                response.getStatusLine().getReasonPhrase(),
                                failureResponseBody);
                    }
                } else{
                    exceptionMessage = String.format("Limit of '%d' timeouts reached without any response",
                            this.maxAttempts);
                }

            }catch (Exception responseBodyException){
                exceptionMessage = String.format("Limit of '%d' timeouts reached without any response",
                        this.maxAttempts);
                printStackTraces(responseBodyException);
            }
            throw new Exception(exceptionMessage);
        }

        return response;
    }

    private int increaseTimeOutValue(int currentValue){
        int newValue = currentValue + (currentValue*30/100);

        return newValue;
    }

    private String getResponseBody(CloseableHttpResponse response){
        String responseBody = "";
        if(response != null){
            try {
                responseBody = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                // no body available
            }
        }
        return responseBody;
    }

    private int getDelayInMilliseconds(int attempts)
    {
        // calculate number of seconds to wait based on exponential backoff algorithm
        int seconds = (int) Math.pow(2, Math.min(attempts - 1, this.exponentLimit));

        // transform seconds into milliseconds and add additional randomised delay
        return (seconds * 1000) + this.randomNumberGenerator.nextInt(0, 999);
    }

    public synchronized DictionaryValue getTokenAsDictionary()
    {
        Map<String, Value> outputMap = new LinkedHashMap<String, Value>();

        outputMap.put("token", new StringValue(this.token));

        DictionaryValue output = new DictionaryValue();
        output.set(outputMap);
        return output;
    }

    private void printStackTraces(Exception exception){
        exception.printStackTrace();
        if(this.externalLogWriter != null){
            try {
                exception.printStackTrace(this.externalLogWriter);
            }catch (Exception e){
                System.out.println("Cannot use the log writer");
                e.printStackTrace();
            }
        }
    }

    private void printStackTraces(String exception){
        System.out.println(exception);
        if(this.externalLogWriter != null){
            try {
                this.externalLogWriter.println(exception);
            }catch (Exception e){
                System.out.println("Cannot use the log writer");
                e.printStackTrace();
            }
        }
    }
}