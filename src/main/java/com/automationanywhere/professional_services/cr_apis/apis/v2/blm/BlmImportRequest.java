package com.automationanywhere.professional_services.cr_apis.apis.v2.blm;

import com.automationanywhere.professional_services.cr_apis.apis.enums.ImportActionIfExisting;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class BlmImportRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.POST;
    public static final String ENDPOINT = "/v2/blm/import";

    // instance properties
    public final String pathToBlmPackage;
    public final ImportActionIfExisting actionIfExisting;
    public final Boolean publicWorkspace;
    public final String archivePassword;

    public BlmImportRequest(String pathToBlmPackage, ImportActionIfExisting actionIfExisting, Boolean publicWorkspace, String archivePassword)
    {
        // set instance properties
        this.pathToBlmPackage = pathToBlmPackage;
        this.actionIfExisting = actionIfExisting;
        this.publicWorkspace = publicWorkspace;
        this.archivePassword = archivePassword;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get POST request object
        HttpPost request = super.createFormMultipartRequest(controlRoomUrl, authToken);

        // build multipart entity
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        // add package file as binary content
        builder.addBinaryBody("upload", new File(this.pathToBlmPackage));

        // add form fields
        builder.addTextBody("actionIfExisting", this.actionIfExisting.name());
        builder.addTextBody("publicWorkspace", this.publicWorkspace.toString());

        if (this.archivePassword != null && !this.archivePassword.isEmpty())
        {
            builder.addTextBody("archivePassword", this.archivePassword);
        }

        // set entity in request
        request.setEntity(builder.build());

        return request;
    }
}