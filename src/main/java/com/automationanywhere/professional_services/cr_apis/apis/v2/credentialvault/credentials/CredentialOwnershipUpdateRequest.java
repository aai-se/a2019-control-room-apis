package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.credentials;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class CredentialOwnershipUpdateRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.PUT;
    transient public static final String ENDPOINT = "/v2/credentialvault/credentials/[CREDENTIAL_ID]/owner/[CREDENTIAL_OWNER_ID]";

    public CredentialOwnershipUpdateRequest(String credentialId, String credentialOwnerId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[CREDENTIAL_ID]", credentialId).replace("[CREDENTIAL_OWNER_ID]", credentialOwnerId);
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}