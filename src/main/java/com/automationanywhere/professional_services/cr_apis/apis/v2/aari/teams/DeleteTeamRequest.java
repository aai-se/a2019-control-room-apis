package com.automationanywhere.professional_services.cr_apis.apis.v2.aari.teams;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class DeleteTeamRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.DELETE;
    transient public static final String ENDPOINT = "/aari/v2/teams/[TEAM_ID]";

    public DeleteTeamRequest(Long teamId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[TEAM_ID]", teamId.toString());
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to DELETE request
        HttpDelete request = (HttpDelete) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}