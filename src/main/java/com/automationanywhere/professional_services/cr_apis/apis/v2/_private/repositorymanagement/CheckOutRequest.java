package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;


import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CheckOutRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.PUT;
    public static final String ENDPOINT = "/v2/repository/checkout/[FILE_ID]";

    // instance properties
    private List<String> ids;
    private String versionNumber;

    public CheckOutRequest(Long fileId, List<Long> dependencyIds, Long versionNumber)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", fileId.toString());

        // set instance properties
        this.ids = new ArrayList<>();

        if (dependencyIds != null && dependencyIds.size() > 0)
        {
            this.ids = new ArrayList<>();

            for (Long id : dependencyIds)
            {
                this.ids.add(String.valueOf(id));
            }
        }

        if (versionNumber == null)
        {
            this.versionNumber = null;
        } else
        {
            this.versionNumber = String.valueOf(versionNumber);
        }
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add bot content to body
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}