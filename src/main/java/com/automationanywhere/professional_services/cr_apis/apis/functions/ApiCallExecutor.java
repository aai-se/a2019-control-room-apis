package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.files.FileDownloadResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.util.EntityUtils;

public class ApiCallExecutor
{
    public static ApiResponse executeApiRequestAndReturnResponse(ControlRoomRestClient client, BaseRequest apiRequest)
            throws Exception
    {
        // get generic executable request object from specific request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert response into ApiResponse object
        ApiResponse apiResponse = new ApiResponse(httpResponse);

        // close response
        httpResponse.close();

        // return output
        return apiResponse;
    }

    public static void executeApiRequestOnly(ControlRoomRestClient client, BaseRequest apiRequest)
            throws Exception
    {
        // get generic executable request object from specific request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // close response
        httpResponse.close();
    }
}