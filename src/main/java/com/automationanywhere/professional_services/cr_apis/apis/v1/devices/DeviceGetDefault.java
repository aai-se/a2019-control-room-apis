package com.automationanywhere.professional_services.cr_apis.apis.v1.devices;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class DeviceGetDefault
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static String ENDPOINT = "/v1/devices/runasusers/default/[USER_ID]";

    // instance properties


    public DeviceGetDefault(Long userId)
    {
        // set instance properties
        ENDPOINT = ENDPOINT.replace("[USER_ID]", ""+userId);

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}