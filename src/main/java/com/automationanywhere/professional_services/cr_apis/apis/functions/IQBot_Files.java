package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.files.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class IQBot_Files
{
    public static ApiResponse uploadFile(ControlRoomRestClient client, String learningInstanceId, String pathToFile)
            throws Exception
    {
        // create file upload request object
        FileUploadRequest apiRequest = new FileUploadRequest(learningInstanceId, pathToFile);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getFileStatus(ControlRoomRestClient client, String learningInstanceId, String fileId)
            throws Exception
    {
        // create file upload request object
        FileStatusRequest apiRequest = new FileStatusRequest(learningInstanceId, fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static FileDownloadResponse downloadExtractedData(ControlRoomRestClient client, String fileId, String fieldsAndTablesConfig)
            throws Exception
    {
        // create file upload request object
        FileDownloadRequest apiRequest = new FileDownloadRequest(fileId);

        // get request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into FileDownloadResponse object
        FileDownloadResponse apiResponse = new FileDownloadResponse(httpResponse, fieldsAndTablesConfig);

        // close response
        httpResponse.close();

        // return output
        return apiResponse;
    }

    public static void deleteFile(ControlRoomRestClient client, String learningInstanceId, String fileId)
            throws Exception
    {
        // create file upload request object
        FileDeletionRequest apiRequest = new FileDeletionRequest(learningInstanceId, fileId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void invalidateFile(ControlRoomRestClient client, String learningInstanceId, String fileId)
            throws Exception
    {
        // create file upload request object
        FileInvalidationRequest apiRequest = new FileInvalidationRequest(learningInstanceId, fileId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static ApiResponse getDataFromValidationApi(ControlRoomRestClient client, String learningInstanceId, String fileId)
            throws Exception
    {
        // create file upload request object
        FileValidationRequest apiRequest = new FileValidationRequest(learningInstanceId, fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
