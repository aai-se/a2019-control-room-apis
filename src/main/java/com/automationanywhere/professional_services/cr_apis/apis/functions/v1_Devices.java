package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.devices.DeviceGetDefault;
import com.automationanywhere.professional_services.cr_apis.apis.v1.devices.DeviceRunAsUserRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.devices.DeviceUnset;

public class v1_Devices
{
    public static ApiResponse searchRunAsUsers(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        DeviceRunAsUserRequest apiRequest = new DeviceRunAsUserRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void unsetDefaultDevice(ControlRoomRestClient client, Long deviceId, Long userId)
            throws Exception
    {
        // create credential search request object
        DeviceUnset apiRequest = new DeviceUnset(deviceId, userId);

        // execute request and return output
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static ApiResponse getDefaultDevice(ControlRoomRestClient client, Long userId)
            throws Exception
    {
        // create credential search request object
        DeviceGetDefault apiRequest = new DeviceGetDefault(userId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}
