package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;

public class CredentialAttributePost
        extends BaseJsonObject
{
    private String name;
    private String description;
    private Boolean userProvided;
    private Boolean masked;
    private Boolean passwordFlag;

    public CredentialAttributePost(String name, String description, Boolean userProvided, Boolean masked, Boolean passwordFlag)
    {
        this.name = name;
        this.description = description;
        this.userProvided = userProvided;
        this.masked = masked;
        this.passwordFlag = passwordFlag;
    }
}
