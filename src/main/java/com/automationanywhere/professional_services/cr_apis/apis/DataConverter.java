package com.automationanywhere.professional_services.cr_apis.apis;

import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IConvertToDictionaryValue;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.*;
import com.automationanywhere.botcommand.data.model.table.Table;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataConverter
{

    public static ListValue convertListOfDictionaryValues(List<? extends IConvertToDictionaryValue> inputList)
            throws Exception
    {
        List<Value> list = new ArrayList<Value>();

        if (inputList != null && inputList.size() > 0)
        {
            for (IConvertToDictionaryValue obj : inputList)
            {
                list.add(obj.convertToDictionaryValue());
            }
        }

        ListValue output = new ListValue();
        output.set(list);
        return output;
    }

    public static ListValue convertListOfStrings(List<String> inputList)
    {
        List<Value> list = new ArrayList<Value>();

        if (inputList != null && inputList.size() > 0)
        {
            for (String str : inputList)
            {
                list.add(new StringValue(str));
            }
        }

        ListValue output = new ListValue();
        output.set(list);
        return output;
    }

    public static Value convertToValue(Object input)
            throws Exception
    {
        if (input == null)
        {
            return new StringValue();
        }

        if (input instanceof String)
        {
            return new StringValue(input);
        } else if (input instanceof Long || input instanceof Double)
        {
            return new NumberValue(input);
        } else if (input instanceof Boolean)
        {
            return new BooleanValue(input);
        } else if (input instanceof Table)
        {
            Table inputObj = (Table) input;
            return new TableValue(inputObj);
        } else
        {
            throw new Exception("Class could not be converted - " + input.getClass().getSimpleName());
        }

        /*
        String className = input.getClass().getSimpleName();

        switch (className)
        {
            case "String":
                return new StringValue(input);
            case "Long":
            case "Double":
                return new NumberValue(input);
            case "Boolean":
                return new BooleanValue(input);
            default:
                throw new Exception("Class could not be converted - " + className);
        }
         */
    }

    public static Value convertToValueIfNotNull(Object input)
            throws Exception
    {
        if (input == null)
        {
            throw new Exception();
        }

        if (input instanceof String)
        {
            return new StringValue(input);
        } else if (input instanceof Long || input instanceof Double)
        {
            return new NumberValue(input);
        } else if (input instanceof Boolean)
        {
            return new BooleanValue(input);
        } else if (input instanceof Table)
        {
            Table inputObj = (Table) input;
            return new TableValue(inputObj);
        } else
        {
            throw new Exception("Class could not be converted - " + input.getClass().getSimpleName());
        }

        /*
        String className = input.getClass().getSimpleName();

        switch (className)
        {
            case "String":
                return new StringValue(()input);
            case "Long":
            case "Double":
                return new NumberValue(input);
            case "Boolean":
                return new BooleanValue(input);
            case "Table":
                return new TableValue(input);
            default:
                throw new Exception("Class could not be converted - " + className);
        }
         */
    }

    public static DictionaryValue convertMapToDictionary(Map<String, Object> inputMap)
            throws Exception
    {
        Map<String, Value> outputMap = new LinkedHashMap<String, Value>();

        for (Map.Entry<String, Object> keyValuePair : inputMap.entrySet())
        {
            String key = keyValuePair.getKey();
            Value value = convertToValue(keyValuePair.getValue());

            outputMap.put(key, value);
        }

        return new DictionaryValue(outputMap);
    }
}
