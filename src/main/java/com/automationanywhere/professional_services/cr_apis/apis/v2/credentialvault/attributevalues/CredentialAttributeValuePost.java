package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;

public class CredentialAttributeValuePost
        extends BaseJsonObject
{
    private String credentialAttributeId;
    private String value;

    public CredentialAttributeValuePost(String credentialAttributeId, String value)
    {
        this.credentialAttributeId = credentialAttributeId;
        this.value = value;
    }
}