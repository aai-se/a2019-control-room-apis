package com.automationanywhere.professional_services.cr_apis.apis.iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IConvertToDictionaryValue;
import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IConvertToListValue;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.IQBotFileExtractedData;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

public class FileDownloadResponse
        implements IConvertToListValue, IConvertToDictionaryValue
{

    public final IQBotFileExtractedData data;

    public FileDownloadResponse(CloseableHttpResponse httpResponse, String fieldsAndTablesConfig)
            throws Exception
    {
        String json = EntityUtils.toString(httpResponse.getEntity());
        this.data = new IQBotFileExtractedData(json, fieldsAndTablesConfig);
    }

    @Override
    public ListValue convertToListValue()
            throws Exception
    {
        return data.getAsList();
    }

    @Override
    public DictionaryValue convertToDictionaryValue()
            throws Exception
    {
        return data.getAsMappings();
    }
}






/*
ORIGINAL DEPRECATED CODE - NECESSARY TO PARSE NON-JSON INPUT PRIOR TO A360.22

package com.automationanywhere.apis.iqbot.files;

import com.automationanywhere.apis.generic.BaseResponse;
import com.automationanywhere.apis.interfaces.IConvertToListValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileDownloadResponse
        extends BaseResponse
        implements IConvertToListValue
{
    public final String rawData;
    public final String validJsonData;
    public final List<String> csvData;

    public FileDownloadResponse(String rawData)
    {
        // extract JSON from response
        this.rawData = rawData;

        // convert JSON received from response into standards-compliant JSON
        this.validJsonData = convertRawDataToValidJson(this.rawData);

        // convert JSON into CSV
        this.csvData = convertValidJsonToCsv(this.validJsonData);
    }

    public FileDownloadResponse(CloseableHttpResponse response)
            throws IOException
    {
        this(EntityUtils.toString(response.getEntity()));
    }

    private static String convertRawDataToValidJson(String rawInput)
    {
        // remove square brackets
        rawInput = rawInput.replaceAll("\\[\\s*", "");
        rawInput = rawInput.replaceAll("\\s*\\]", "");

        // correct newlines between entries
        rawInput = rawInput.replaceAll("\\},\\s+\\{", "},{");

        // split on commas between entries using lookaheads
        String[] entries = rawInput.split("(?<=\\}),(?=\\{)");

        // loop through individual entries
        for (int i = 0; i < entries.length; i++)
        {
            // trim curly braces and any extra whitespace
            entries[i] = entries[i].replaceAll("\\{\\s*", "");
            entries[i] = entries[i].replaceAll("\\s*\\}", "");

            // replace erroneous newlines
            entries[i] = entries[i].replaceAll("\r\n\\s*", "");

            // split entry into key value pairs using lookahead to select the correct commas
            // lookahead ensures that commas within values are not selected
            String[] keyValuePairs = entries[i].split(", (?=[^,=]+=)");

            // loop through key value pairs
            for (int j = 0; j < keyValuePairs.length; j++)
            {
                // remove newlines and following whitespace
                keyValuePairs[j] = keyValuePairs[j].replaceAll("\r\n\\s+", "");

                // add escape character to any instances of backslashes
                keyValuePairs[j] = keyValuePairs[j].replaceAll("\\\\", "\\\\\\\\");

                // add escape character to any instances of quotes
                keyValuePairs[j] = keyValuePairs[j].replaceAll("\"", "\\\\\"");

                // add leading and trailing double quotes
                keyValuePairs[j] = "\"" + keyValuePairs[j] + "\"";

                // replace first equals with colon surrounded by double quotes
                keyValuePairs[j] = keyValuePairs[j].replaceFirst("=", "\":\"");
            }

            // join key value pairs back into a single string using comma separation
            String entry = String.join(",", keyValuePairs);

            // restore the curly braces
            entry = "{" + entry + "}";

            // replace the entry
            entries[i] = entry;
        }

        // join entries back into one string
        String textOutput = String.join(",", entries);

        // restore square brackets
        textOutput = "[" + textOutput + "]";

        // return output
        return textOutput;
    }

    private static List<String> convertValidJsonToCsv(String validJson)
    {
        // create variable to hold output
        List<String> output = new ArrayList<String>();

        // create JSON array from input
        JSONArray extractedData = new JSONArray(validJson);

        // get JSON array representing keys in first element
        JSONArray keys = extractedData.getJSONObject(0).names();

        // use list of strings to store header row to contain keys
        List<String> headerRowList = new ArrayList<String>();

        // iterate through keys
        for (Object keyElement : keys)
        {
            // cast key to string and add to list
            headerRowList.add((String) keyElement);
        }

        // convert list to comma-separated string and add to output
        String headerRow = String.join(",", headerRowList);
        output.add(headerRow);

        // iterate through items in extracted data
        List<String> dataRowList = new ArrayList<String>();

        for (Object data : extractedData)
        {
            // cast data to JSONObject
            JSONObject dataRowObject = (JSONObject) data;

            // clear list of previous data
            dataRowList.clear();

            // iterate through values to extract in order
            for (String key : headerRowList)
            {
                // get value
                String value = dataRowObject.getString(key);

                // replace any existing double quotes with an additional double quote as per CSV formatting rules
                value = value.replace("\"", "\"\"");

                // add double quotes surrounding entire value
                value = "\"" + value + "\"";

                // add value to list
                dataRowList.add(value);
            }

            // join values with commas and add to output
            String dataRow = String.join(",", dataRowList);
            output.add(dataRow);
        }

        // return output
        return output;
    }

    @Override
    public ListValue convertToListValue()
            throws Exception
    {
        StringValue[] outputArray = new StringValue[this.csvData.size()];

        for (int i = 0; i < this.csvData.size(); i++)
        {
            outputArray[i] = new StringValue(this.csvData.get(i));
        }

        return new ListValue(outputArray);
    }
}
 */