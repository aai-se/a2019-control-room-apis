package com.automationanywhere.professional_services.cr_apis.apis.generic;

import com.google.gson.Gson;

/**
 Base object providing functionality to convert objects to/from JSON-formatted data.
 */
public abstract class BaseJsonObject
{

    /**
     Method to convert a correctly defined object to its JSON representation.

     @return String containing JSON data for this object
     */
    public String toJson()
    {
        return new Gson().toJson(this);
    }
}