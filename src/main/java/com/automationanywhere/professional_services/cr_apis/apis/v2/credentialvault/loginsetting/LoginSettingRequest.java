package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.loginsetting;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class LoginSettingRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.PUT;
    transient public static final String ENDPOINT = "/v2/credentialvault/loginsetting";

    // instance properties
    public final String username;
    public final String loginUsername;
    public final String loginPassword;

    public LoginSettingRequest(String loginUsername, String loginPassword, String username)
    {
        // set instance properties
        this.loginUsername = loginUsername;
        this.loginPassword = loginPassword;

        // only set username if provided - only used when setting device credentials for a different user
        if (username != null & !username.isBlank())
        {
            this.username = username;
        } else
        {
            this.username = null;
        }

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}