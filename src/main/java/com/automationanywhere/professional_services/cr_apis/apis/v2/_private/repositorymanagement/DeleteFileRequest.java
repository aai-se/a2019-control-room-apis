package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;


import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class DeleteFileRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.DELETE;
    public static final String ENDPOINT = "/v2/repository/files/[FILE_ID]";

    // instance properties

    public DeleteFileRequest(Long fileId)
            throws Exception
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", String.valueOf(fileId));
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpDelete request = (HttpDelete) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}