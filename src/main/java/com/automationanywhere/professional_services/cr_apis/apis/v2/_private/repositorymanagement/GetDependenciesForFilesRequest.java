package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class GetDependenciesForFilesRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.POST;
    public static final String ENDPOINT = "/v2/repository/dependencies";

    // instance properties
    private List<String> fileIds;

    public GetDependenciesForFilesRequest(List<Long> fileIdsInput)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;

        // set instance properties
        this.fileIds = new ArrayList<>();

        if (fileIdsInput != null && fileIdsInput.size() > 0)
        {
            for (Long id : fileIdsInput)
            {
                this.fileIds.add(String.valueOf(id));
            }
        }
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add info to body
        StringEntity jsonEntity = new StringEntity(this.toJson());
        request.setEntity(jsonEntity);

        return request;
    }
}