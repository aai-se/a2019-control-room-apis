package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2._private.packages.PackageDetailsRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2._private.packages.PackagesFilterRequest;

public class _private_v2_Packages
{
    public static ApiResponse searchForPackages(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create credential search request object
        PackagesFilterRequest apiRequest = new PackagesFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getPackageInfo(ControlRoomRestClient client, Long packageId)
            throws Exception
    {
        // create request
        PackageDetailsRequest apiRequest = new PackageDetailsRequest(packageId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}