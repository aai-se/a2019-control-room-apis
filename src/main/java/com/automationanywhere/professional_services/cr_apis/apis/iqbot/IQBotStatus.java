package com.automationanywhere.professional_services.cr_apis.apis.iqbot;

public enum IQBotStatus
{
    SUCCESS, VALIDATION, INVALID, UNTRAINED, UNCLASSIFIED
}
