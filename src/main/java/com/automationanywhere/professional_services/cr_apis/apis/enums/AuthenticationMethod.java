package com.automationanywhere.professional_services.cr_apis.apis.enums;

/**
 Enum to control permitted types of Control Room authentication.
 */
public enum AuthenticationMethod
{
    PASSWORD, API_KEY
}