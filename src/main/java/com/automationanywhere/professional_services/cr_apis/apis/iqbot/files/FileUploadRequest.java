package com.automationanywhere.professional_services.cr_apis.apis.iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class FileUploadRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/IQBot/gateway/organizations/1/projects/[LEARNING_INSTANCE_ID]/files/upload/1";

    // instance properties
    transient private String pathToFile;

    public FileUploadRequest(String learningInstanceId, String pathToFile)
    {
        // set instance properties
        this.pathToFile = pathToFile;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LEARNING_INSTANCE_ID]", learningInstanceId);
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get POST request object
        HttpPost request = super.createFormMultipartRequest(controlRoomUrl, authToken);

        // build multipart entity
        MultipartEntityBuilder builder = MultipartEntityBuilder.create().addBinaryBody("files", new File(pathToFile));

        // set entity in request
        request.setEntity(builder.build());

        return request;
    }
}