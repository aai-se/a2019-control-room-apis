package com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.users;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class DeleteUserRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.DELETE;
    transient public static final String ENDPOINT = "/v1/usermanagement/users/[USER_ID]";

    public DeleteUserRequest(String userId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[USER_ID]", userId);
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to GET request
        HttpDelete request = (HttpDelete) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}