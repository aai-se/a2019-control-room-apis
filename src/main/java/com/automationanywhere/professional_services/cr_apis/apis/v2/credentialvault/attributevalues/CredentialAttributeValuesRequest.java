package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CredentialAttributeValuesRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/v2/credentialvault/credentials/[CREDENTIAL_ID]/attributevalues";

    // instance properties
    public final String credentialId;
    public final String credentialAttributeId;
    public final String userId;
    public final String encryptionKey;

    public CredentialAttributeValuesRequest(String credentialId, String credentialAttributeId, String userId, String encryptionKey)
    {
        // set instance properties
        this.credentialId = credentialId;
        this.credentialAttributeId = credentialAttributeId;
        this.userId = userId;
        this.encryptionKey = encryptionKey;

        // amend endpoint
        String amendedEndpoint = ENDPOINT.replace("[CREDENTIAL_ID]", this.credentialId);

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = amendedEndpoint;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create parameters
        List<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("credentialAttributeId", this.credentialAttributeId));

        if (!this.userId.isEmpty())
        {
            parameters.add(new BasicNameValuePair("userId", this.userId));
        }

        if (!this.encryptionKey.isEmpty())
        {
            parameters.add(new BasicNameValuePair("encryptionKey", this.encryptionKey));
        }

        // get request object and cast to POST request
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, parameters);

        return request;
    }
}