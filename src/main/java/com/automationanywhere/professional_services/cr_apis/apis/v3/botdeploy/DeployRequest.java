package com.automationanywhere.professional_services.cr_apis.apis.v3.botdeploy;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class DeployRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v3/automations/deploy";

    // instance vars
    public final int fileId;
    public final int[] runAsUserIds;
    public final int[] poolIds;
    public final boolean overrideDefaultDevice;
    public final int numOfRunAsUsersToUse;

    public DeployRequest(int fileId, int[] runAsUserIds, int[] poolIds, boolean overrideDefaultDevice,
                         int numOfRunAsUsersToUse)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;

        // store instance variables
        this.fileId = fileId;
        this.runAsUserIds = runAsUserIds;
        this.poolIds = poolIds;
        this.overrideDefaultDevice = overrideDefaultDevice;
        this.numOfRunAsUsersToUse = numOfRunAsUsersToUse;
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert authentication object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}