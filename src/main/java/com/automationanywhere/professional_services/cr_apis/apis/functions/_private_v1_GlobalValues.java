package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v1._private.globalvalues.GlobalValuesDetailRequest;

public class _private_v1_GlobalValues
{
    public static ApiResponse getAllGlobalValues(ControlRoomRestClient client)
            throws Exception
    {
        // create FilterRequest object
        GlobalValuesDetailRequest apiRequest = new GlobalValuesDetailRequest();

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}