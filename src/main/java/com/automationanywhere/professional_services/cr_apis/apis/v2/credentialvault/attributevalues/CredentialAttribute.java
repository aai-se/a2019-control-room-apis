package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.DataConverter;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;
import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IConvertToDictionaryValue;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.util.LinkedHashMap;
import java.util.Map;

public class CredentialAttribute
        extends BaseJsonObject
        implements IConvertToDictionaryValue
{
    private String id;
    private String name;
    private String description;
    private Boolean userProvided;
    private Boolean masked;
    private Boolean passwordFlag;
    private String createdBy;
    private String createdOn;
    private String updatedBy;
    private String updatedOn;
    private String version;

    public DictionaryValue convertToDictionaryValue()
            throws Exception
    {
        // create map as String:String
        Map<String, Value> map = new LinkedHashMap<String, Value>();

        map.put("id", DataConverter.convertToValue(id));
        map.put("name", DataConverter.convertToValue(name));
        map.put("description", DataConverter.convertToValue(description));
        map.put("userProvided", DataConverter.convertToValue(userProvided));
        map.put("masked", DataConverter.convertToValue(masked));
        map.put("passwordFlag", DataConverter.convertToValue(passwordFlag));
        map.put("createdBy", DataConverter.convertToValue(createdBy));
        map.put("createdOn", DataConverter.convertToValue(createdOn));
        map.put("updatedBy", DataConverter.convertToValue(updatedBy));
        map.put("updatedOn", DataConverter.convertToValue(updatedOn));
        map.put("version", DataConverter.convertToValue(version));

        // convert map to DictionaryValue
        DictionaryValue output = new DictionaryValue(map);

        return output;
    }
}