package com.automationanywhere.professional_services.cr_apis.apis.v3.workloadmanagement.workitems;

import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WorkitemsData
        extends BaseJsonObject
{

    private List<Workitem> workItems;

    public WorkitemsData(Table workitemDataInput)
    {
        // create output list
        this.workItems = new ArrayList<Workitem>();

        // get schema as list from from table object
        List<Schema> schemas = workitemDataInput.getSchema();

        // get rows as list from table object
        List<Row> rows = workitemDataInput.getRows();

        // iterate through table rows
        for (Row row : rows)
        {
            // create new workitem object for this row
            Workitem workitem = new Workitem();

            // get values as list from row object
            List<Value> values = row.getValues();

            // iterate through row values by index
            for (int i = 0; i < values.size(); i++)
            {
                // add key/value pair to map
                workitem.addKeyValuePair(schemas.get(i).getName(), values.get(i).toString());
            }

            // add map to list of maps
            this.workItems.add(workitem);
        }
    }

    public class Workitem
            extends BaseJsonObject
    {
        private Map<String, String> json;

        public Workitem()
        {
            this.json = new LinkedHashMap<String, String>();
        }

        private void addKeyValuePair(String key, String value)
        {
            this.json.put(key, value);
        }
    }
}