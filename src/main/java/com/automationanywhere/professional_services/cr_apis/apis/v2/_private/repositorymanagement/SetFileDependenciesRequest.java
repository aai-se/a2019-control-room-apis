package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class SetFileDependenciesRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.PUT;
    public static final String ENDPOINT = "/v2/repository/files/[FILE_ID]/dependencies";

    // instance properties
    private List<String> childFileIds;

    public SetFileDependenciesRequest(Long fileId, List<Long> childFileIds)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", fileId.toString());

        // set instance properties
        this.childFileIds = new ArrayList<>();

        if (childFileIds != null && childFileIds.size() > 0)
        {
            for (Long id : childFileIds)
            {
                this.childFileIds.add(String.valueOf(id));
            }
        }
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add info to body
        StringEntity jsonEntity = new StringEntity(this.toJson());
        request.setEntity(jsonEntity);

        return request;
    }
}