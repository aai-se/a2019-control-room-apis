package com.automationanywhere.professional_services.cr_apis.apis.v1.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.enums.AuthenticationMethod;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidAuthenticationMethodException;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

/**
 Implementation of AuthRequest model.
 */
public class AuthRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v1/authentication";

    // instance properties
    public final String username;
    public final String password;
    public final String apiKey;

    /**
     Constructor to create new AuthRequest object.

     @param username   String containing username
     @param authInput  String containing authentication input (password or API key)
     @param authMethod enum to dictate the type of authentication method
     */
    public AuthRequest(String username, String authInput, AuthenticationMethod authMethod)
            throws InvalidAuthenticationMethodException
    {
        // set instance properties
        switch (authMethod)
        {
            case PASSWORD:
                this.username = username;
                this.password = authInput;
                this.apiKey = null;
                break;
            case API_KEY:
                this.username = username;
                this.password = null;
                this.apiKey = authInput;
                break;
            default:
                throw new InvalidAuthenticationMethodException();
        }

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object without authentication and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, null, null);

        // convert authentication object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}