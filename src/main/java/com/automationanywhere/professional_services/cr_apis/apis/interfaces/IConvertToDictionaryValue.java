package com.automationanywhere.professional_services.cr_apis.apis.interfaces;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;

public interface IConvertToDictionaryValue
{
    public DictionaryValue convertToDictionaryValue()
            throws Exception;
}