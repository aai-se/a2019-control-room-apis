package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.lockers;

import com.automationanywhere.professional_services.cr_apis.apis.enums.LockerMemberPermission;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class LockerAddOrUpdateMemberRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.PUT;
    transient public static final String ENDPOINT = "/v2/credentialvault/lockers/[LOCKER_ID]/members/[USER_ID]";

    // instance properties
    List<String> permissions = new ArrayList<>();

    public LockerAddOrUpdateMemberRequest(String lockerId, String userId, LockerMemberPermission memberPermission)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LOCKER_ID]", lockerId).replace("[USER_ID]", userId);

        // set instance properties
        // note switch case fall-through is deliberate!
        switch (memberPermission)
        {
            case OWN:
                this.permissions.add("own");
            case MANAGE:
                this.permissions.add("manage");
            case PARTICIPATE:
                this.permissions.add("participate");
        }
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert member permissions list to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}