package com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

public class AllFilesUploadedUtility
{
    public static DictionaryValue getFilenamesToGroupMappings(ApiResponse apiResponse)
    {
        // get root JSONObject from response
        JSONObject rootObj = apiResponse.getJsonObject();

        // get JSONArray of uploaded files from root object
        JSONArray files = rootObj.getJSONArray("data");

        // create map to hold output
        Map<String, Value> map = new LinkedHashMap<String, Value>();

        // loop through data to add filenames and group IDs to map
        for (Object fileObj : files)
        {
            // cast to JSONObject
            JSONObject file = (JSONObject) fileObj;

            // get file name and group ID
            String fileName = file.getString("fileName");
            String groupId = file.getString("classificationId");

            // add data to map
            map.put(fileName, new StringValue(groupId));
        }

        // convert map to dictionary
        DictionaryValue output = new DictionaryValue();
        output.set(map);
        return output;
    }
}
