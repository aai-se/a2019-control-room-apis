package com.automationanywhere.professional_services.cr_apis.apis.v1.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;

public class VerifyTokenRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/v1/authentication/token";

    public VerifyTokenRequest()
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException
    {
        // create GET request without default settings
        URI uri = new URIBuilder(controlRoomUrl).setPath(this.endpoint).setParameter("token", authToken).build();
        HttpGet request = new HttpGet(uri);
        return request;
    }
}