package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v3.migration.*;

public class v3_Migration
{

    public static ApiResponse startMigration(ControlRoomRestClient client, String name, String description, Boolean overwriteBots, Long[] botIds, Long[] userIds,
                                             Boolean legacyExcelCellRow, Boolean includeChildFolders, Long[] folderIds, Boolean emailEwsSettings, String emailEwsExchangeVersion,
                                             String emailEwsAuthenticationType, Boolean citrixChannelTimeoutGvSetting)
            throws Exception
    {
        // create request
        StartMigrationRequest apiRequest = new StartMigrationRequest(name, description, overwriteBots, botIds, userIds, legacyExcelCellRow, includeChildFolders, folderIds,
                emailEwsSettings, emailEwsExchangeVersion, emailEwsAuthenticationType, citrixChannelTimeoutGvSetting);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getMigrationResults(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create queue search request object
        GetMigrationResultsRequest apiRequest = new GetMigrationResultsRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getMigrationStatus(ControlRoomRestClient client, Long migrationId)
            throws Exception
    {
        // create request
        GetMigrationStatusRequest apiRequest = new GetMigrationStatusRequest(migrationId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getMigrationDetailedResults(ControlRoomRestClient client, Long migrationId, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create queue search request object
        GetSpecificMigrationResultsRequest apiRequest = new GetSpecificMigrationResultsRequest(migrationId, filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getDetailedStatusForMigratedFile(ControlRoomRestClient client, Long migrationId, Long migrationFileId, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create queue search request object
        GetSpecificMigratedFileResultsRequest apiRequest = new GetSpecificMigratedFileResultsRequest(migrationId, migrationFileId, filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}