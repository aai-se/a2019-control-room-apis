package com.automationanywhere.professional_services.cr_apis.apis.v1.blm;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class ExportRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.POST;
    public static final String ENDPOINT = "/v1/blm/export";

    // instance properties
    public final String packageName;
    public final Long[] selectedFileIds;
    public final Long[] fileIds;
    public final Boolean excludeMetaBots;
    public final String password;

    public ExportRequest(String packageName, Long[] selectedFileIds, Long[] fileIds, Boolean excludeMetaBots, String password)
    {
        // set instance properties
        this.packageName = packageName;
        this.selectedFileIds = selectedFileIds;
        this.fileIds = fileIds;
        this.excludeMetaBots = excludeMetaBots;

        if (password != null && !password.isEmpty())
        {
            this.password = password;
        } else
        {
            this.password = null;
        }

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}