package com.automationanywhere.professional_services.cr_apis.apis.enums;

/**
 Enum to control HTTP request types.
 */
public enum RequestType
{
    GET, POST, PUT, DELETE
}