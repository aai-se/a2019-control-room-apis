package com.automationanywhere.professional_services.cr_apis.apis.v1.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IApiRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class RefreshAuthRequest
        extends BaseRequest
        implements IApiRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v1/authentication/token";

    // instance properties
    public final String token;

    public RefreshAuthRequest(String token)
    {
        // set instance properties
        this.token = token;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    /**
     Implementation of IApiRequest method.
     */
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object without authentication and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, null, null);

        // convert authentication object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}