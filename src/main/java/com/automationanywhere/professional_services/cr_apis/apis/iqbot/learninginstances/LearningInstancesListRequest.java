package com.automationanywhere.professional_services.cr_apis.apis.iqbot.learninginstances;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class LearningInstancesListRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/IQBot/gateway/tenants/1/learning-instance/select-options";

    public LearningInstancesListRequest()
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get GET request object
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}