package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.keys;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class PublicKeyRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/v2/credentialvault/keys/transport/public";

    public PublicKeyRequest()
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to GET request
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}