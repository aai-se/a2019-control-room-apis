package com.automationanywhere.professional_services.cr_apis.apis.generic;

import org.json.JSONArray;
import org.json.JSONObject;

public class FilterRequest
        extends BaseJsonObject
{
    // instance properties
    public final JSONArray fields;
    public final JSONObject filter;
    public final JSONArray sort;
    public final JSONObject page;

    public FilterRequest(String filterRequestText)
    {
        JSONObject filterRequestObj;

        if (filterRequestText != null && !filterRequestText.isEmpty())
        {
            filterRequestObj = new JSONObject(filterRequestText);
        }
        else
        {
            filterRequestObj = new JSONObject();
        }

        this.fields = filterRequestObj.has("fields") ? (JSONArray) filterRequestObj.get("fields") : null;
        this.filter = filterRequestObj.has("filter") ? (JSONObject) filterRequestObj.get("filter") : null;
        this.sort = filterRequestObj.has("sort") ? (JSONArray) filterRequestObj.get("sort") : null;
        this.page = filterRequestObj.has("page") ? (JSONObject) filterRequestObj.get("page") : null;
    }

    @Override
    public String toJson()
    {
        JSONObject outputObj = new JSONObject();

        if (this.fields != null)
        {
            outputObj.put("fields", this.fields);
        }

        if (this.filter != null)
        {
            outputObj.put("filter", this.filter);
        }

        if (this.sort != null)
        {
            outputObj.put("sort", this.sort);
        }

        if (this.page != null)
        {
            outputObj.put("page", this.page);
        }

        String output = outputObj.toString();
        return output;
    }
}