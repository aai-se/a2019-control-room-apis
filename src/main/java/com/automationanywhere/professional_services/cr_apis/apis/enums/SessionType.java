package com.automationanywhere.professional_services.cr_apis.apis.enums;

public enum SessionType
{
    BOT_RUNNER_SESSION, CUSTOM_SESSION;
}
