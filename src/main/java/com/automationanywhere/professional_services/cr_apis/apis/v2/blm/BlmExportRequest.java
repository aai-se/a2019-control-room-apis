package com.automationanywhere.professional_services.cr_apis.apis.v2.blm;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class BlmExportRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v2/blm/export";

    // instance properties
    public final String name;
    public final Integer[] fileIds;
    public final Boolean includePackages;
    public final String archivePassword;

    public BlmExportRequest(String name, String[] fileIdsInput, Boolean includePackages, String archivePassword)
    {
        // set instance properties
        this.fileIds = new Integer[fileIdsInput.length];

        for (int i = 0; i < fileIdsInput.length; i++)
        {
            fileIds[i] = Integer.valueOf(fileIdsInput[i]);
        }

        this.name = name;
        this.includePackages = includePackages;
        this.archivePassword = archivePassword;

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // convert object properties to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}