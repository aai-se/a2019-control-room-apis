package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.aari.TeamBasic;
import com.automationanywhere.professional_services.cr_apis.apis.v2.aari.files.GetFileDownloadTokenRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.aari.files.GetFileDownloadUrlRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v2.aari.teams.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.List;

public class v2_AARI
{
    public static ApiResponse getFilteredListOfTeams(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create team search request object
        GetTeamFilterRequest apiRequest = new GetTeamFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getTeamDetails(ControlRoomRestClient client, Long teamId)
            throws Exception
    {
        // create team details request object
        GetTeamDetailsRequest apiRequest = new GetTeamDetailsRequest(teamId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getTeamMembers(ControlRoomRestClient client, Long teamId)
            throws Exception
    {
        // create team members request object
        GetTeamMembersRequest apiRequest = new GetTeamMembersRequest(teamId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getUserDetails(ControlRoomRestClient client, Long userId)
            throws Exception
    {
        // create user details request object
        GetUserDetailsRequest apiRequest = new GetUserDetailsRequest(userId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse createTeam(ControlRoomRestClient client, String name, String description, List<Long> userIds, List<Long> processIds)
            throws Exception
    {
        // create basic team object
        TeamBasic teamInfo = new TeamBasic(name, description, userIds, processIds);

        // create create team request object
        CreateTeamRequest apiRequest = new CreateTeamRequest(teamInfo);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void updateTeam(ControlRoomRestClient client, Long teamId, String name, String description, List<Long> userIds, List<Long> processIds)
            throws Exception
    {
        // create basic team object
        TeamBasic teamInfo = new TeamBasic(name, description, userIds, processIds);

        // create update team request object
        UpdateTeamRequest apiRequest = new UpdateTeamRequest(teamId, teamInfo);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static void deleteTeam(ControlRoomRestClient client, Long teamId)
            throws Exception
    {
        // create delete team request object
        DeleteTeamRequest apiRequest = new DeleteTeamRequest(teamId);

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }

    public static String getFileUrl(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create references to reused objects
        HttpRequestBase httpRequest;
        CloseableHttpResponse httpResponse;

        // create download token request
        GetFileDownloadTokenRequest apiRequestToken = new GetFileDownloadTokenRequest(fileId);
        httpRequest = apiRequestToken.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get token from response
        httpResponse = client.executeRequest(httpRequest);
        ApiResponse apiResponseToken = new ApiResponse(httpResponse);
        String accessToken = apiResponseToken.getJsonObject().getString("fileAccessToken");
        httpResponse.close();

        // create download url request
        GetFileDownloadUrlRequest apiRequestUrl = new GetFileDownloadUrlRequest(accessToken);
        httpRequest = apiRequestUrl.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get url from response
        httpResponse = client.executeRequest(httpRequest);
        ApiResponse apiResponseUrl = new ApiResponse(httpResponse);
        String url = apiResponseUrl.getJsonObject().getJSONObject("secureUrls").getString("secureUrl");
        httpResponse.close();

        // return url
        return url;
    }
}