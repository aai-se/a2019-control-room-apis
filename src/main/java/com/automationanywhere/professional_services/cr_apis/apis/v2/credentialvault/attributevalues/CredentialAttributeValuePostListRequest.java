package com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CredentialAttributeValuePostListRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v2/credentialvault/credentials/[CREDENTIAL_ID]/attributevalues";

    // instance properties
    transient public final String id;
    public final List<CredentialAttributeValuePost> list;
    transient public final String encryptionKey;

    public CredentialAttributeValuePostListRequest(String credentialId, List<CredentialAttributeValuePost> list, String encryptionKey)
    {
        // set instance properties
        this.id = credentialId;
        this.list = list;
        this.encryptionKey = encryptionKey;

        // amend endpoint
        String amendedEndpoint = ENDPOINT.replace("[CREDENTIAL_ID]", this.id);

        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = amendedEndpoint;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create parameters if required
        List<NameValuePair> parameters = null;

        if (this.encryptionKey != null && !this.encryptionKey.isEmpty())
        {
            parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("encryptionKey", this.encryptionKey));
        }

        // get request object and cast to POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, parameters);

        // convert list of attribute values to JSON and add to request
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}