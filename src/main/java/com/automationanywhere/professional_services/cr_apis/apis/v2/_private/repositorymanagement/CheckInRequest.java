package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;


import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CheckInRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.PUT;
    public static final String ENDPOINT = "/v2/repository/checkin";

    // instance properties
    private String description;
    private List<String> ids;

    public CheckInRequest(String description, List<Long> fileIds)
            throws Exception
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT;

        // set instance properties
        this.description = description;

        if (fileIds != null && fileIds.size() > 0)
        {
            this.ids = new ArrayList<>();

            for (Long id : fileIds)
            {
                this.ids.add(String.valueOf(id));
            }
        } else
        {
            throw new Exception("List of file IDs cannot be null or empty");
        }
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create PUT request
        HttpPut request = (HttpPut) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add bot content to body
        String json = this.toJson();
        StringEntity jsonEntity = new StringEntity(json);
        request.setEntity(jsonEntity);

        return request;
    }
}