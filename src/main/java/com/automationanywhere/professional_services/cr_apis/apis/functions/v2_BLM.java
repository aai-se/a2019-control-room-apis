package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.ImportActionIfExisting;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v2.blm.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class v2_BLM
{
    public static ApiResponse exportBlmPackage(ControlRoomRestClient client, String name, String[] fileIds, Boolean includePackages, String archivePassword)
            throws Exception
    {
        // create BLM export request object
        BlmExportRequest apiRequest = new BlmExportRequest(name, fileIds, includePackages, archivePassword);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getBlmStatus(ControlRoomRestClient client, String requestId)
            throws Exception
    {
        // create BLM status request object
        BlmStatusRequest apiRequest = new BlmStatusRequest(requestId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void downloadBlmPackage(ControlRoomRestClient client, String requestId, String pathToOutputFile)
            throws Exception
    {
        // create BLM download request object
        BlmDownloadRequest apiRequest = new BlmDownloadRequest(requestId);

        // get request object
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into BlmDownloadResponse object
        BlmDownloadResponse apiResponse = new BlmDownloadResponse(httpResponse);

        // close response
        httpResponse.close();

        // write data to output file
        apiResponse.writeDataToFile(pathToOutputFile);
    }

    public static ApiResponse importBlmPackage(ControlRoomRestClient client, String pathToInputFile, ImportActionIfExisting importActionIfExisting, Boolean publicWorkspace, String archivePassword)
            throws Exception
    {
        // create BLM export request object
        BlmImportRequest apiRequest = new BlmImportRequest(pathToInputFile, importActionIfExisting, publicWorkspace, archivePassword);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}