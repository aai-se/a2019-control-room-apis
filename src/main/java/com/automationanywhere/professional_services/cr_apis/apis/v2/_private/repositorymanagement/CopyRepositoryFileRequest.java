package com.automationanywhere.professional_services.cr_apis.apis.v2._private.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class CopyRepositoryFileRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.POST;
    transient public static final String ENDPOINT = "/v2/repository/files/[FILE_ID]/copy";

    // instance properties
    private String name;
    private String parentId;

    public CopyRepositoryFileRequest(Long fileId, String name, Long parentId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[FILE_ID]", fileId.toString());

        // set instance properties
        this.name = name;
        this.parentId = parentId.toString();
    }

    @Override
    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create POST request
        HttpPost request = (HttpPost) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        // add bot content to body
        StringEntity jsonEntity = new StringEntity(this.toJson());
        request.setEntity(jsonEntity);

        return request;
    }
}