package com.automationanywhere.professional_services.cr_apis.apis.interfaces;

import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

/**
 Interface to define methods provided by API request objects.
 */
public interface IApiRequest
{
    /**
     Method to get a request object containing the required definition and data for an implemented API request.

     @param controlRoomUrl Control Room URL
     @param authToken      authentication token (if available)

     @return HTTP request object
     */
    HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException;
}