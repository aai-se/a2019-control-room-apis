package com.automationanywhere.professional_services.cr_apis.apis.v2.aari;

import com.automationanywhere.professional_services.cr_apis.apis.DataConverter;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseJsonObject;
import com.automationanywhere.professional_services.cr_apis.apis.interfaces.IConvertToDictionaryValue;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Process
        extends BaseJsonObject
        implements IConvertToDictionaryValue
{
    private Long id;
    private String title;
    private List<String> tags;


    @Override
    public DictionaryValue convertToDictionaryValue()
            throws Exception
    {
        Map<String, Value> outputMap = new LinkedHashMap<String, Value>();

        outputMap.put("id", DataConverter.convertToValue(id));
        outputMap.put("title", DataConverter.convertToValue(title));
        outputMap.put("tags", DataConverter.convertListOfStrings(tags));

        return new DictionaryValue(outputMap);
    }
}
