package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.v1.authentication.LogoutRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.authentication.RefreshAuthRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.authentication.VerifyTokenRequest;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class v1_Authentication
{
    /**
     API call to authenticate a user. All logic and data remains inside ControlRoomRestClient class.

     @param client ControlRoomRestClient object containing authentication data
     */
    public static ApiResponse authenticateUser(ControlRoomRestClient client)
            throws Exception
    {
        // create request object
        HttpRequestBase httpRequest = client.authRequest.getRequestObject(client.controlRoomUrl, null);

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into AuthResponse object
        ApiResponse authResponse = new ApiResponse(httpResponse);

        // close response
        httpResponse.close();

        // return output
        return authResponse;
    }

    /**
     API call to verify if an authentication token is still valid.

     @param client ControlRoomRestClient object containing authentication data

     @return boolean for valid (true) or not valid (false)
     */
    public static boolean verifyToken(ControlRoomRestClient client, String authToken)
            throws Exception
    {
        // create request objects
        VerifyTokenRequest apiRequest = new VerifyTokenRequest();
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, authToken);

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into VerifyTokenResponse object
        ApiResponse apiResponse = new ApiResponse(httpResponse);

        // close response
        httpResponse.close();

        // get boolean for validity from response object
        boolean isValid = apiResponse.getJsonObject().getBoolean("valid");

        // return result
        return isValid;
    }

    /**
     API call to refresh an existing API token.

     @param client ControlRoomRestClient object containing authentication data
     */
    public static ApiResponse refreshToken(ControlRoomRestClient client, String authToken)
            throws Exception
    {
        // create request objects
        RefreshAuthRequest apiRequest = new RefreshAuthRequest(authToken);
        HttpRequestBase httpRequest = apiRequest.getRequestObject(client.controlRoomUrl, null);

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(httpRequest);

        // convert generic response into AuthResponse object
        ApiResponse apiResponse = new ApiResponse(httpResponse);

        // close response
        httpResponse.close();

        // return output
        return apiResponse;
    }

    public static void logoutUser(ControlRoomRestClient client)
            throws Exception
    {
        // create request object
        LogoutRequest apiRequest = new LogoutRequest();
        HttpRequestBase requestObj = apiRequest.getRequestObject(client.controlRoomUrl, client.getAuthToken());

        // execute request and get response
        CloseableHttpResponse httpResponse = client.executeRequest(requestObj);

        // close response
        httpResponse.close();
    }
}