package com.automationanywhere.professional_services.cr_apis.apis.iqbot;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcore.api.dto.AttributeType;
import com.automationanywhere.professional_services.utilities.converters.JsonConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class IQBotFileExtractedData
{

    public final String extractedDataJson;
    public final String dataModelJson;

    private List<String> dataModelFields;
    private Map<String, List<String>> dataModelTables;

    private JSONArray extractedDataRows;

    private Map<String, Value> dataFields;
    private Map<String, TableValue> dataTables;

    public IQBotFileExtractedData(String extractedDataJson, String configJson)
    {
        this.extractedDataJson = extractedDataJson;

        if (configJson == null || configJson.isBlank())
        {
            this.dataModelJson = null;
        } else
        {
            this.dataModelJson = configJson;
        }
    }

    public ListValue getAsList()
            throws Exception
    {
        List<Value> data = JsonConverter.convertJsonToListOfValues(extractedDataJson);
        return new ListValue(data.toArray(new Value[0]));
    }

    public DictionaryValue getAsMappings()
            throws Exception
    {
        // parse config data to populate fields and tables definitions
        parseConfig();

        // transform data from JSON to JSONObject for use in parsing field and table data
        this.extractedDataRows = new JSONArray(extractedDataJson);

        // parse fields
        parseFields();

        // parse tables
        parseTables();

        // create map to hold output
        Map<String, Value> outputMap = new LinkedHashMap<>();

        // add fields to output map
        DictionaryValue fieldsDictionary = new DictionaryValue(this.dataFields);
        outputMap.put("fields", fieldsDictionary);

        // create map to hold tables
        Map<String, Value> tablesMap = new LinkedHashMap<>();

        // add tables to tables map
        for (String tableName : this.dataTables.keySet())
        {
            TableValue tableValue = this.dataTables.get(tableName);
            tablesMap.put(tableName, tableValue);
        }

        // add tables map to output map
        outputMap.put("tables", new DictionaryValue(tablesMap));

        // return DictionaryValue
        return new DictionaryValue(outputMap);
    }

    private void parseConfig()
            throws Exception
    {
        // confirm config is populated
        if (this.dataModelJson == null)
        {
            throw new Exception("Config is not populated - cannot generate dictionaries");
        }

        // transform config data into JSONObject
        JSONObject dataModelObj;

        try
        {
            dataModelObj = new JSONObject(this.dataModelJson);
        } catch (JSONException exception)
        {
            throw new Exception("Config input is invalid - please check with a JSON validator and try compressing to a single line of JSON if necessary");
        }

        // instantiate objects
        this.dataModelFields = new ArrayList<>();
        this.dataModelTables = new LinkedHashMap<>();

        // extract all form field names
        JSONArray fieldsArray = dataModelObj.getJSONArray("fields");

        for (Object obj : fieldsArray)
        {
            // cast to JSONObject
            JSONObject field = (JSONObject) obj;

            // get field name
            dataModelFields.add(field.getString("name"));
        }

        // extract tables
        JSONArray tablesArray = dataModelObj.getJSONArray("tables");

        // iterate over items in tables array to extract data for each table
        for (Object obj : tablesArray)
        {
            // cast to JSONObject
            JSONObject tableObj = (JSONObject) obj;

            // get table name
            String tableName = tableObj.getString("name");

            // instantiate list to hold table fields
            List<String> tableFieldNames = new ArrayList<>();

            // iterate through array of fields to add to list
            JSONArray tableFieldsObj = tableObj.getJSONArray("fields");

            for (Object arrayObj : tableFieldsObj)
            {
                // cast to JSONObject
                JSONObject fieldObj = (JSONObject) arrayObj;

                // get column name
                String fieldName = fieldObj.getString("name");

                // add column name to list
                tableFieldNames.add(fieldName);
            }

            // add table to list
            dataModelTables.put(tableName, tableFieldNames);
        }
    }

    private void parseFields()
    {
        // instantiate data form fields map
        this.dataFields = new LinkedHashMap<>();

        // get first "row" of data from input data
        JSONObject firstRow = this.extractedDataRows.getJSONObject(0);

        // for each form field, extract the value and add to the map
        for (String fieldName : this.dataModelFields)
        {
            String fieldValue = firstRow.getString(fieldName);
            this.dataFields.put(fieldName, new StringValue(fieldValue));
        }
    }

    private void parseTables()
    {
        // instantiate map to hold table data
        Map<String, Table> tablesMap = new LinkedHashMap<>();

        // create table objects
        for (String tableName : this.dataModelTables.keySet())
        {
            // get column names
            List<String> columnNames = this.dataModelTables.get(tableName);

            // create new list of schema objects
            List<Schema> columns = new ArrayList<>();

            // create and add a schema object for each column name
            for (String columnName : columnNames)
            {
                Schema column = new Schema();
                column.setName(columnName);
                column.setType(AttributeType.STRING);

                columns.add(column);
            }

            // create table using schemas
            Table tableObj = new Table();
            tableObj.setSchema(columns);

            // add to map for use in output
            tablesMap.put(tableName, tableObj);
        }

        // iterate through tables to populate rows where values are found in data
        for (String tableName : dataModelTables.keySet())
        {
            // get table object
            Table table = tablesMap.get(tableName);

            // get table rows object
            List<Row> tableRows = table.getRows();

            // get column names from config
            List<String> columnNames = dataModelTables.get(tableName);

            // iterate through rows of data to populate rows in tables
            for (Object rowObj : this.extractedDataRows)
            {
                // cast to JSONObject
                JSONObject dataRow = (JSONObject) rowObj;

                // create list to hold row values
                List<Value> rowValues = new ArrayList<>();

                // set flag to check if any values are found
                boolean validInputFound = false;

                // iterate through column names to populate row fields
                for (String columnName : columnNames)
                {
                    // get value from row
                    String fieldValue = dataRow.optString(columnName);

                    // add to list of row values
                    rowValues.add(new StringValue(fieldValue));

                    if (!fieldValue.isBlank())
                    {
                        validInputFound = true;
                    }
                }

                // add list to table object if valid input was found
                if (validInputFound)
                {
                    // create new row using list of values
                    Row newRow = new Row(rowValues);

                    // add new row to existing rows
                    tableRows.add(newRow);
                }
            }
        }

        // instantiate map to hold output data
        this.dataTables = new LinkedHashMap<>();

        // create TableValue from each Table object and add to output data
        for (String tableName : tablesMap.keySet())
        {
            Table tableObj = tablesMap.get(tableName);
            TableValue tableValue = new TableValue(tableObj);
            this.dataTables.put(tableName, tableValue);
        }
    }
}