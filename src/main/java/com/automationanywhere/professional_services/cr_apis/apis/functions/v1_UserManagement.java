package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.generic.FilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.roles.RoleFilterRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.roles.RoleGetRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1.usermanagement.users.*;

import java.util.List;

public class v1_UserManagement
{

    // ROLES

    public static ApiResponse searchForRoles(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create role search request object
        RoleFilterRequest apiRequest = new RoleFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void createRole(ControlRoomRestClient client)
    {
    }

    public static ApiResponse getRole(ControlRoomRestClient client, String roleId)
            throws Exception
    {
        // create role get request object
        RoleGetRequest apiRequest = new RoleGetRequest(roleId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void updateRole(ControlRoomRestClient client)
    {
    }

    public static void deleteRole(ControlRoomRestClient client)
    {
    }

    // USERS

    public static ApiResponse searchForUsers(ControlRoomRestClient client, String filterRequestJson)
            throws Exception
    {
        // create FilterRequest object
        FilterRequest filterRequest = new FilterRequest(filterRequestJson);

        // create user search request object
        UserFilterRequest apiRequest = new UserFilterRequest(filterRequest);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse createUser(ControlRoomRestClient client, String username, String password, String email, List<Long> roleIds, String firstName,
                                         String lastName, String domain, String description, List<String> licenseFeatures, Boolean enableAutoLogin, Boolean disabled)
            throws Exception
    {
        // create user details object
        UserDetailsModify userDetails = new UserDetailsModify(true, username, password, email, roleIds, firstName, lastName, domain, description, licenseFeatures,
                enableAutoLogin, disabled);

        // add user details object into new request object
        CreateUserRequest apiRequest = new CreateUserRequest(userDetails);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse getUser(ControlRoomRestClient client, Long userId)
            throws Exception
    {
        // create new request object
        GetUserRequest apiRequest = new GetUserRequest(userId.toString());

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static ApiResponse updateUser(ControlRoomRestClient client, Long userId, String password, String email, List<Long> roleIds, String firstName,
                                         String lastName, String description, List<String> licenseFeatures, Boolean enableAutoLogin, Boolean disabled)
            throws Exception
    {
        // create user details object
        UserDetailsModify userDetails = new UserDetailsModify(false, null, password, email, roleIds, firstName, lastName, null, description, licenseFeatures,
                enableAutoLogin, disabled);

        // add user details object into new request object
        UpdateUserRequest apiRequest = new UpdateUserRequest(userDetails, userId.toString());

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }

    public static void deleteUser(ControlRoomRestClient client, Long userId)
            throws Exception
    {
        // create new request object
        DeleteUserRequest apiRequest = new DeleteUserRequest(userId.toString());

        // execute request only
        ApiCallExecutor.executeApiRequestOnly(client, apiRequest);
    }
}