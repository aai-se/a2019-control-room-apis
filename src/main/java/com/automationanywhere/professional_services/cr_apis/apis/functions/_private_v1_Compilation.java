package com.automationanywhere.professional_services.cr_apis.apis.functions;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v1._private.compilation.BotCompilationRequest;
import com.automationanywhere.professional_services.cr_apis.apis.v1._private.globalvalues.GlobalValuesDetailRequest;

public class _private_v1_Compilation
{
    public static ApiResponse compileBot(ControlRoomRestClient client, Long fileId)
            throws Exception
    {
        // create FilterRequest object
        BotCompilationRequest apiRequest = new BotCompilationRequest(fileId);

        // execute request and return output
        return ApiCallExecutor.executeApiRequestAndReturnResponse(client, apiRequest);
    }
}