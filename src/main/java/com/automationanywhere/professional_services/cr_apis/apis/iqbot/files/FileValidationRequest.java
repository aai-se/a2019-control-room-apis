package com.automationanywhere.professional_services.cr_apis.apis.iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class FileValidationRequest
        extends BaseRequest
{
    // class properties
    transient public static final RequestType REQUEST_TYPE = RequestType.GET;
    transient public static final String ENDPOINT = "/IQBot/api/validator/[LEARNING_INSTANCE_ID]";


    // instance properties
    transient private String fileId;

    public FileValidationRequest(String learningInstanceId, String fileId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[LEARNING_INSTANCE_ID]", learningInstanceId);

        // set instance property
        this.fileId = fileId;
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // create parameters
        NameValuePair param = new BasicNameValuePair("fileid", this.fileId);

        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(param);

        // get GET request object
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, parameters);

        return request;
    }
}