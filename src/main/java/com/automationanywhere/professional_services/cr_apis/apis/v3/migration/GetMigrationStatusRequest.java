package com.automationanywhere.professional_services.cr_apis.apis.v3.migration;

import com.automationanywhere.professional_services.cr_apis.apis.enums.RequestType;
import com.automationanywhere.professional_services.cr_apis.apis.exceptions.InvalidHttpTypeException;
import com.automationanywhere.professional_services.cr_apis.apis.generic.BaseRequest;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

public class GetMigrationStatusRequest
        extends BaseRequest
{
    // class properties
    public static final RequestType REQUEST_TYPE = RequestType.GET;
    public static final String ENDPOINT = "/v3/migration/[MIGRATION_ID]";

    public GetMigrationStatusRequest(Long migrationId)
    {
        // set base properties
        this.requestType = REQUEST_TYPE;
        this.endpoint = ENDPOINT.replace("[MIGRATION_ID]", String.valueOf(migrationId));
    }

    public HttpRequestBase getRequestObject(String controlRoomUrl, String authToken)
            throws URISyntaxException, InvalidHttpTypeException, UnsupportedEncodingException
    {
        // get request object and cast to POST request
        HttpGet request = (HttpGet) super.createDefaultRequestObject(controlRoomUrl, authToken, null);

        return request;
    }
}