package com.automationanywhere.professional_services.cr_apis.botvariables.tables;

import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcore.api.dto.AttributeType;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseTableObject
{

    protected Table table;

    public BaseTableObject()
    {
    }

    public BaseTableObject(Table table)
            throws Exception
    {
        // validate schema
        if (!isInputSchemaValid(table.getSchema()))
        {
            throw new Exception("Schema is invalid");
        }

        // set table
        this.table = table;
    }

    public Table getEmptyTable()
    {
        // build table schema
        List<Schema> schemas = getSchema();

        // build single empty row
        Row emptyRow = getEmptyRow();
        List<Row> rows = new ArrayList<Row>();
        rows.add(emptyRow);

        // convert to Table object; set schema and empty row
        Table table = new Table();
        table.setSchema(schemas);
        table.setRows(rows);

        return table;
    }

    public abstract List<Schema> getSchema();

    public abstract Row getEmptyRow();

    public boolean isInputSchemaValid(List<Schema> inputSchema)
    {
        // get expected schema
        List<Schema> expectedSchema = this.getSchema();

        // if schema sizes are not identical, return false
        if (inputSchema.size() != expectedSchema.size())
        {
            return false;
        }

        // iterate over list of objects
        for (int index = 0; index < inputSchema.size(); index++)
        {
            // get field names
            String key1 = inputSchema.get(index).getName();
            String key2 = expectedSchema.get(index).getName();

            // get field types
            AttributeType type1 = inputSchema.get(index).getType();
            AttributeType type2 = expectedSchema.get(index).getType();

            // if both the name and the type are not identical, return false
            if (!key1.equals(key2) || type1 != type2)
            {
                return false;
            }
        }

        // code has completed iterating over schemas without differences being found - return true
        return true;
    }

    public void addRow(Row row)
    {
        this.table.getRows().add(row);
    }

    public TableValue get()
    {
        // get Table object with predefined schema and one empty row
        Table table = getEmptyTable();

        // convert to TableValue object and return
        TableValue output = new TableValue();
        output.set(table);
        return output;
    }

    public TableValue getAsTableValue()
    {
        return new TableValue(this.table);
    }
}