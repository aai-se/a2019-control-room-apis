package com.automationanywhere.professional_services.cr_apis.botvariables.tables.iqbot;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcore.api.dto.AttributeType;
import com.automationanywhere.professional_services.cr_apis.botvariables.tables.BaseTableObject;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.VariableExecute;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.commandsdk.annotations.BotCommand.CommandType.Variable;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand(commandType = Variable)

//CommandPks adds required information to be displayable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "TableIQBotValidationItems", label = "[[TableIQBotValidationItems.label]]",
        description = "[[TableIQBotValidationItems.description]]", variable_return_type = DataType.TABLE
)

public class TableIQBotValidationItems
        extends BaseTableObject
{

    public static List<Schema> schema = new ArrayList<Schema>(List.of(
            new Schema("File ID", AttributeType.STRING),
            new Schema("Group ID", AttributeType.STRING),
            new Schema("Field type", AttributeType.STRING),
            new Schema("Table name", AttributeType.STRING),
            new Schema("Field name", AttributeType.STRING),
            new Schema("Validation code", AttributeType.STRING)
    ));

    public static Row emptyRow = new Row(new ArrayList<Value>(List.of(
            new StringValue(""),
            new StringValue(""),
            new StringValue(""),
            new StringValue(""),
            new StringValue(""),
            new StringValue("")
    )));

    public TableIQBotValidationItems()
    {
        super();
    }

    public TableIQBotValidationItems(Table table)
            throws Exception
    {
        super(table);
    }

    public void addRow(String fileId, String groupId, String fieldType, String tableName, String fieldName, int validationCode)
    {
        List<Value> newRowValues = new ArrayList<Value>();

        newRowValues.add(new StringValue(fileId));
        newRowValues.add(new StringValue(groupId));
        newRowValues.add(new StringValue(fieldType));
        newRowValues.add(new StringValue(tableName));
        newRowValues.add(new StringValue(fieldName));
        newRowValues.add(new NumberValue(validationCode));

        Row newRow = new Row(newRowValues);

        super.addRow(newRow);
    }

    @VariableExecute
    public TableValue get()
    {
        return super.get();
    }

    @Override
    public List<Schema> getSchema()
    {
        return schema;
    }

    @Override
    public Row getEmptyRow()
    {
        return emptyRow;
    }
}
