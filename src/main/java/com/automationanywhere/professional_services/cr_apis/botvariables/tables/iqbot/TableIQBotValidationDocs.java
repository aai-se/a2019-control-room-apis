package com.automationanywhere.professional_services.cr_apis.botvariables.tables.iqbot;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcore.api.dto.AttributeType;
import com.automationanywhere.professional_services.cr_apis.botvariables.tables.BaseTableObject;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.VariableExecute;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.commandsdk.annotations.BotCommand.CommandType.Variable;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand(commandType = Variable)

//CommandPks adds required information to be displayable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "TableIQBotValidationDocs", label = "[[TableIQBotValidationDocs.label]]",
        description = "[[TableIQBotValidationDocs.description]]", variable_return_type = DataType.TABLE
)

public class TableIQBotValidationDocs
        extends BaseTableObject
{

    public static List<Schema> schema = new ArrayList<Schema>(List.of(
            new Schema("File name", AttributeType.STRING),
            new Schema("File ID", AttributeType.STRING),
            new Schema("Learning instance ID", AttributeType.STRING),
            new Schema("Group ID", AttributeType.STRING),
            new Schema("Fields - total", AttributeType.NUMBER),
            new Schema("Fields - validation", AttributeType.NUMBER),
            new Schema("URL", AttributeType.STRING)
    ));

    public static Row emptyRow = new Row(new ArrayList<Value>(List.of(
            new StringValue(""),
            new StringValue(""),
            new StringValue(""),
            new StringValue(""),
            new NumberValue(0),
            new NumberValue(0),
            new StringValue("")
    )));

    public TableIQBotValidationDocs()
    {
        super();
    }

    public TableIQBotValidationDocs(Table table)
            throws Exception
    {
        super(table);
    }

    public void addRow(String fileName, String fileId, String learningInstanceId, String groupId, int fieldsTotal, int fieldsValidation, String url)
    {
        List<Value> newRowValues = new ArrayList<Value>();

        newRowValues.add(new StringValue(fileName));
        newRowValues.add(new StringValue(fileId));
        newRowValues.add(new StringValue(learningInstanceId));
        newRowValues.add(new StringValue(groupId));
        newRowValues.add(new NumberValue(fieldsTotal));
        newRowValues.add(new NumberValue(fieldsValidation));
        newRowValues.add(new StringValue(url));

        Row newRow = new Row(newRowValues);

        super.addRow(newRow);
    }

    @VariableExecute
    public TableValue get()
    {
        return super.get();
    }

    @Override
    public List<Schema> getSchema()
    {
        return schema;
    }

    @Override
    public Row getEmptyRow()
    {
        return emptyRow;
    }
}
