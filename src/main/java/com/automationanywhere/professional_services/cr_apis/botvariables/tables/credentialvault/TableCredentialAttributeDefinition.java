package com.automationanywhere.professional_services.cr_apis.botvariables.tables.credentialvault;

import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributePost;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.impl.TableValue;
import com.automationanywhere.botcommand.data.model.Schema;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcore.api.dto.AttributeType;
import com.automationanywhere.professional_services.cr_apis.botvariables.tables.BaseTableObject;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.VariableExecute;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.commandsdk.annotations.BotCommand.CommandType.Variable;

//BotCommand makes a class eligible for being considered as an action.
@BotCommand(commandType = Variable)

//CommandPks adds required information to be displayable on GUI.
@CommandPkg(
        //Unique name inside a package and label to display.
        name = "TableCredentialAttributeDefinition", label = "[[TableCredentialAttributeDefinition.label]]",
        description = "[[TableCredentialAttributeDefinition.description]]", variable_return_type = DataType.TABLE
)

public class TableCredentialAttributeDefinition
        extends BaseTableObject
{

    public static List<Schema> schema = new ArrayList<Schema>(List.of(
            new Schema("name", AttributeType.STRING),
            new Schema("description", AttributeType.STRING),
            new Schema("userProvided", AttributeType.BOOLEAN),
            new Schema("masked", AttributeType.BOOLEAN),
            new Schema("passwordFlag", AttributeType.BOOLEAN)
    ));

    public static Row emptyRow = new Row(new ArrayList<Value>(List.of(
            new StringValue(""),
            new StringValue(""),
            new BooleanValue(""),
            new BooleanValue(""),
            new BooleanValue("")
    )));

    public TableCredentialAttributeDefinition()
    {
        super();
    }

    public TableCredentialAttributeDefinition(Table table)
            throws Exception
    {
        super(table);
    }

    @VariableExecute
    public TableValue get()
    {
        // get Table object with predefined schema and one empty row
        Table table = getEmptyTable();

        // convert to TableValue object and return
        TableValue output = new TableValue();
        output.set(table);
        return output;
    }

    public List<Schema> getSchema()
    {
        return schema;
    }

    public Row getEmptyRow()
    {
        return emptyRow;
    }

    public List<CredentialAttributePost> convertToListOfObjects()
            throws Exception
    {
        // confirm table is not null
        if (this.table == null)
        {
            throw new Exception("Table is null");
        }

        // confirm table rows are not empty
        if (this.table.getRows().isEmpty())
        {
            throw new Exception("Rows are empty");
        }

        // setup output list
        List<CredentialAttributePost> output = new ArrayList<CredentialAttributePost>();

        for (Row row : this.table.getRows())
        {
            List<Value> rowValues = row.getValues();

            String name = (String) rowValues.get(0).get();
            String description = (String) rowValues.get(1).get();
            Boolean userProvided = (Boolean) rowValues.get(2).get();
            Boolean masked = (Boolean) rowValues.get(3).get();
            Boolean passwordFlag = (Boolean) rowValues.get(4).get();

            CredentialAttributePost attribute = new CredentialAttributePost(name, description, userProvided, masked, passwordFlag);

            output.add(attribute);
        }

        return output;
    }
}
