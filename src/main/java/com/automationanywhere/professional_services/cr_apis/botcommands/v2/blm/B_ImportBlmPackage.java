package com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.LocalFile;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.cr_apis.apis.enums.ImportActionIfExisting;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_BLM;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@BotCommand

@CommandPkg(
        name = "ImportBlmPackage",
        label = "[[ImportBlmPackage.label]]",
        group_label = "[[BLM.group_label]]",
        node_label = "[[ImportBlmPackage.node_label]]",
        description = "[[ImportBlmPackage.description]]",
        icon = "pkg.svg",

        return_label = "[[BLM.requestId.label]]",
        return_type = DataType.DICTIONARY,
        return_required = true
)

public class B_ImportBlmPackage
        extends BaseBotCommand
{
    // additional static variables
    public static final String SKIP = "Skip";
    public static final String OVERWRITE = "Overwrite";

    // standard static variables
    private static final Logger logger = LogManager.getLogger(B_ImportBlmPackage.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue importBlmPackage(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                String sessionNameInput,

            // 2: path to input file
            @Idx(index = "2", type = AttributeType.FILE)
            @Pkg(label = "[[ImportBlmPackage.pathToInputFile.label]]")
            @NotEmpty
            @LocalFile
             String pathToInputFile,

            // 3: action if existing
            @Idx(index = "3", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(
                            node_label = "[[ImportBlmPackage.actionIfExisting.skip.label]]",
                            label = "[[ImportBlmPackage.actionIfExisting.skip.label]]",
                            value = SKIP)
                    ),
                    @Idx.Option(index = "3.2", pkg = @Pkg(
                            node_label = "[[ImportBlmPackage.actionIfExisting.overwrite.label]",
                            label = "[[ImportBlmPackage.actionIfExisting.overwrite.label]]",
                            value = OVERWRITE)
                    )
            })
            @Pkg(label = "[[ImportBlmPackage.actionIfExisting.label]]", default_value = SKIP, default_value_type = DataType.STRING)
            @Inject
            @NotEmpty
              String actionIfExistingInput,

            // 4: public workspace
            @Idx(index = "4", type = AttributeType.BOOLEAN)
            @Pkg(label = "[[ImportBlmPackage.publicWorkspace.label]]", default_value = "true", default_value_type = DataType.BOOLEAN)
            @NotEmpty
                Boolean publicWorkspace,

            // 5: archive password
            @Idx(index = "5", type = AttributeType.TEXT)
            @Pkg(label = "[[BLM.archivePassword.label]]")
              String archivePassword
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // transform input parameters
            ImportActionIfExisting importAction = ImportActionIfExisting.valueOf(actionIfExistingInput.toUpperCase());

            if (archivePassword.isBlank())
            {
                archivePassword = null;
            }

            // log input parameters
            logger.debug("Path to input file: {}", pathToInputFile);
            logger.debug("Action if existing: {}", actionIfExistingInput);
            logger.debug("Public workspace: {}", publicWorkspace);
            logger.debug("Archive password: {}", archivePassword);

            if (archivePassword != null)
            {
                logger.debug("Archive password: {}", archivePassword);
            }

            // execute API
            logger.debug("Executing API to import package");
            ApiResponse response = v2_BLM.importBlmPackage(client, pathToInputFile, importAction, publicWorkspace, archivePassword);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}