package com.automationanywhere.professional_services.cr_apis.botcommands._c_config;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomSessionManager;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "ConfigureHttpClient",
        label = "[[ConfigureHttpClient.label]]",
        group_label = "[[Configuration.group_label]]",
        node_label = "[[ConfigureHttpClient.node_label]]",
        description = "[[ConfigureHttpClient.description]]",
        icon = "pkg.svg"
)

public class A_ConfigureHttpClient
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_ConfigureHttpClient.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void configureHttpClient(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: connect timeout
            @Idx(index = "2", type = NUMBER)
            @Pkg(
                    label = "[[ConfigureHttpClient.connectTimeout.label]]", description = "[[ConfigureHttpClient.connectTimeout.description]]",
                    default_value_type = DataType.NUMBER, default_value = ControlRoomRestClient.DEFAULT_CONNECT_TIMEOUT
            )
            @NotEmpty
                    Double connectTimeoutInput,

            // 3: socket timeout
            @Idx(index = "3", type = NUMBER)
            @Pkg(
                    label = "[[ConfigureHttpClient.socketTimeout.label]]", description = "[[ConfigureHttpClient.socketTimeout.description]]",
                    default_value_type = DataType.NUMBER, default_value = ControlRoomRestClient.DEFAULT_SOCKET_TIMEOUT
            )
            @NotEmpty
                    Double socketTimeoutInput,

            // 4: max attempts
            @Idx(index = "4", type = NUMBER)
            @Pkg(
                    label = "[[ConfigureHttpClient.maxAttempts.label]]", description = "[[ConfigureHttpClient.maxAttempts.description]]",
                    default_value_type = DataType.NUMBER, default_value = ControlRoomRestClient.DEFAULT_MAX_ATTEMPTS
            )
            @NotEmpty
                    Double maxAttemptsInput,

            // 5: exponent limit
            @Idx(index = "5", type = NUMBER)
            @Pkg(
                    label = "[[ConfigureHttpClient.exponentLimit.label]]", description = "[[ConfigureHttpClient.exponentLimit.description]]",
                    default_value_type = DataType.NUMBER, default_value = ControlRoomRestClient.DEFAULT_EXPONENT_LIMIT
            )
            @NotEmpty
                    Double exponentLimitInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs from doubles to ints
            int connectTimeout = connectTimeoutInput.intValue();
            int socketTimeout = socketTimeoutInput.intValue();
            int maxAttempts = maxAttemptsInput.intValue();
            int exponentLimit = exponentLimitInput.intValue();

            // log input parameters
            logger.debug("Input parameters for HTTP client configuration:");
            logger.debug("ConnectTimeout: {}", connectTimeout);
            logger.debug("SocketTimeout: {}", socketTimeout);
            logger.debug("Max attempts: {}", maxAttempts);
            logger.debug("Exponent limit: {}", exponentLimit);

            // validate input parameters
            if (maxAttempts < 1)
            {
                throw new Exception("Value for maximum attempts must be at least 1");
            }

            if (exponentLimit < 1)
            {
                throw new Exception("Value for exponent limit must be at least 1");
            }

            // update HTTP client settings locally
            super.client.configureConnectionAndCreateClient(connectTimeout, socketTimeout, maxAttempts, exponentLimit);

            // update HTTP client settings in session (requires ending current session and starting new one)
            ControlRoomSessionManager.deleteSession(super.sessionName);
            ControlRoomSessionManager.setSession(super.sessionName, super.client);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}
