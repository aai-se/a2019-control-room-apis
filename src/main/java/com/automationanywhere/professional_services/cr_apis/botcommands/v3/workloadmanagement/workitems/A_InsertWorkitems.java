package com.automationanywhere.professional_services.cr_apis.botcommands.v3.workloadmanagement.workitems;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v3_WorkloadManagement;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;

@BotCommand
@CommandPkg(
        name = "InsertWorkitems",
        group_label = "[[WorkloadManagement.Workitems.group_label]]",
        label = "[[InsertWorkitems.label]]",
        node_label = "[[InsertWorkitems.node_label]]",
        description = "[[InsertWorkitems.description]]",
        icon = "pkg.svg",

        return_label = "[[InsertWorkitems.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = false
)

public class A_InsertWorkitems
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_InsertWorkitems.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue insertWorkitems(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: queue ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[WorkloadManagement.queueId.label]]")
            @NotEmpty
                    Double queueIdInput,

            // 3: workitem data
            @Idx(index = "3", type = TABLE)
            @Pkg(
                    label = "[[WorkloadManagement.workitemData.label]]",
                    description = "[[WorkloadManagement.workitemData.description]]"
            )
                    Table workitemData
    )
    {
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs from doubles to longs
            long queueId = queueIdInput.longValue();

            // log workitem search input parameters
            logger.debug("Input parameters for workitems search:");
            logger.debug("Queue ID: {}", queueId);

            // execute API
            logger.debug("Executing API to insert workitems");
            ApiResponse response = v3_WorkloadManagement.createWorkitems(client, queueId, workitemData);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}
