package com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_BLM;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

@BotCommand

@CommandPkg(
        name = "ExportBlmPackage",
        label = "[[ExportBlmPackage.label]]",
        group_label = "[[BLM.group_label]]",
        node_label = "[[ExportBlmPackage.node_label]]",
        description = "[[ExportBlmPackage.description]]",
        icon = "pkg.svg",

        return_label = "[[BLM.requestId.label]]",
        return_type = DataType.DICTIONARY,
        return_required = true
)

public class A_ExportBlmPackage
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_ExportBlmPackage.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue exportBlmPackage(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: package name
            @Idx(index = "2", type = AttributeType.TEXT)
            @Pkg(label = "[[ExportBlmPackage.packageName.label]]")
            @NotEmpty
                    String packageName,

            // 3: file IDs
            @Idx(index = "3", type = AttributeType.LIST)
            @Pkg(label = "[[ExportBlmPackage.fileIdsList.label]]")
            @NotEmpty
                    List<StringValue> fileIdsInput,

            // 4: include packages
            @Idx(index = "4", type = AttributeType.BOOLEAN)
            @Pkg(label = "[[ExportBlmPackage.includePackages.label]]")
            @NotEmpty
                    Boolean includePackages,

            // 5: archive password
            @Idx(index = "5", type = AttributeType.TEXT)
            @Pkg(label = "[[BLM.archivePassword.label]]")
                    String archivePassword
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // transform input parameters
            String[] fileIds = new String[fileIdsInput.size()];

            for (int i = 0; i < fileIdsInput.size(); i++)
            {
                fileIds[i] = fileIdsInput.get(i).get();
            }

            // String[] fileIds = fileIdsInput.toArray(new String[0]);

            if (archivePassword.isBlank())
            {
                archivePassword = null;
            }

            // log input parameters
            logger.debug("Input parameters for export package generation:");
            logger.debug("Package name: {}", packageName);
            logger.debug("File IDs: {}", StringUtils.join(fileIds, ","));
            logger.debug("Include packages: {}", includePackages);

            if (archivePassword != null)
            {
                logger.debug("Archive password: {}", archivePassword);
            }

            // execute API
            logger.debug("Executing API to generate export package");
            ApiResponse response = v2_BLM.exportBlmPackage(client, packageName, fileIds, includePackages, archivePassword);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}