package com.automationanywhere.professional_services.cr_apis.botcommands._b_private;

import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v2_RepositoryManagement;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@BotCommand

@CommandPkg(
        name = "CheckIn",
        group_label = "[[Private.group_label]]",
        label = "[[CheckIn.label]]",
        node_label = "[[CheckIn.node_label]]",
        description = "[[CheckIn.description]]",
        icon = "pkg.svg",

        return_label = "[[CheckIn.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = false
)

public class I_CheckIn
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(I_CheckIn.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue checkIn(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: description
            @Idx(index = "2", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.folderId.label]]")
            @NotEmpty
                    String description,

            // 3: file IDs
            @Idx(index = "3", type = AttributeType.LIST)
            @Pkg(label = "[[Generic.fileIds.label]]")
            @NotEmpty
                    List<NumberValue> fileIdsInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log input parameters
            logger.debug("Input parameters for check in:");
            logger.debug("Description: {}", description);

            // convert list of file IDs  to list of longs
            List<Long> fileIds = new ArrayList<>();

            for (NumberValue fileIdInput : fileIdsInput)
            {
                fileIds.add(fileIdInput.get().longValue());
                logger.debug("File ID: {}", fileIdInput.get());
            }

            // execute API
            logger.debug("Executing API to check in");
            ApiResponse response = _private_v2_RepositoryManagement.checkIn(this.client, description, fileIds);
            return response.getDictionaryValue();

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}