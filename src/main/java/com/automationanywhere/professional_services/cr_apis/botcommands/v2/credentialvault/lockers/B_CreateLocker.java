package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.lockers;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "CreateLocker",
        label = "[[CreateLocker.label]]",
        group_label = "[[CredentialVault.Lockers.group_label]]",
        node_label = "[[CreateLocker.node_label]]",
        description = "[[CreateLocker.description]]",
        icon = "pkg.svg",

        return_label = "[[CreateLocker.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY
)

public class B_CreateLocker
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(B_CreateLocker.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue createLocker(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: locker name
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CreateLocker.lockerName.label]]")
            @NotEmpty
                    String lockerName,

            // 3: locker description
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[CreateLocker.lockerDescription.label]]")
                    String lockerDescription
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log user creation with input parameters
            logger.debug("Input parameters for locker creation:");
            logger.debug("Locker name: {}", lockerName);
            logger.debug("Locker description: {}", lockerDescription);

            logger.debug("Executing API to create locker");

            // execute API
            ApiResponse response = v2_CredentialVault.createLocker(client, lockerName, lockerDescription);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}