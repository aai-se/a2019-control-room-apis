package com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.teams;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_AARI;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "UpdateTeam",
        label = "[[UpdateTeam.label]]",
        group_label = "[[AARI.Teams.group_label]]",
        node_label = "[[UpdateTeam.node_label]]",
        description = "[[UpdateTeam.description]]",
        icon = "pkg.svg"
)

public class F_UpdateTeam
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(F_UpdateTeam.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void updateTeam(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: team ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[AARI.teamId.label]]")
            @NotEmpty
                    Double teamIdInput,

            // 3: team name
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[AARI.teamName.label]]")
            @NotEmpty
                    String teamName,

            // 3: team description
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "[[AARI.teamDescription.label]]")
                    String teamDescription,

            // 4: list of member IDs
            @Idx(index = "5", type = LIST)
            @Pkg(label = "[[AARI.memberIds.label]]")
                    List<NumberValue> teamMemberIdsInput,

            // 5: list of process IDs
            @Idx(index = "6", type = LIST)
            @Pkg(label = "[[AARI.processIds.label]]")
                    List<NumberValue> processIdsInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert numeric inputs
            Long teamId = teamIdInput.longValue();

            // convert lists with numeric inputs
            List<Long> teamMemberIds = new ArrayList<Long>();

            for (NumberValue teamMemberId : teamMemberIdsInput)
            {
                teamMemberIds.add(teamMemberId.get().longValue());
            }

            List<Long> processIds = new ArrayList<Long>();

            for (NumberValue processId : processIdsInput)
            {
                processIds.add(processId.get().longValue());
            }

            // log credential search input parameters
            logger.debug("Input parameters for get user details:");
            logger.debug("Team ID: {}", teamId);
            logger.debug("Team name: {}", teamName);
            logger.debug("Team description: {}", teamDescription);
            logger.debug("Team member IDs: {}", teamMemberIds.toString());
            logger.debug("Team description: {}", processIds.toString());

            // execute API
            logger.debug("Executing API to update team");
            v2_AARI.updateTeam(client, teamId, teamName, teamDescription, teamMemberIds, processIds);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex.getMessage());
        }
    }
}