package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributeValuePost;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.professional_services.cr_apis.botvariables.tables.credentialvault.TableCredentialAttributeValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.TABLE;
import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.LIST;
import static com.automationanywhere.commandsdk.model.DataType.*;

@BotCommand

@CommandPkg(
        name = "CreateCredentialAttributeValues",
        label = "[[CreateCredentialAttributeValues.label]]",
        group_label = "[[CredentialVault.AttributeValues.group_label]]",
        node_label = "[[CreateCredentialAttributeValues.node_label]]",
        description = "[[CreateCredentialAttributeValues.description]]",
        icon = "pkg.svg",

        return_label = "[[CreateCredentialAttributeValues.return_label]]",
        return_type = LIST,
        return_sub_type = ANY
)

public class B_CreateCredentialAttributeValues
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(B_CreateCredentialAttributeValues.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public ListValue createCredentialAttributeValues(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: credential name
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CredentialVault.credentialId.label]]")
            @NotEmpty
                    String credentialId,

            // 3: attribute definitions
            @Idx(index = "3", type = TABLE)
            @Pkg(label = "[[CreateCredential.attributeDefinitions.label]]")
            @NotEmpty
                    Table attributeValuesTable,

            // 4: encryption key
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "[[CredentialVault.encryptionKey.label]]", description = "[[CredentialVault.encryptionKey.description]]")
                    String encryptionKey
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log user creation with input parameters
            logger.debug("Input parameters for attribute values creation:");
            logger.debug("Credential ID: {}", credentialId);
            if (!encryptionKey.isEmpty())
            {
                logger.debug("RSA encryption key: {}", encryptionKey);
            }

            // validate and convert table to list of objects
            logger.debug("Converting attribute values table");
            List<CredentialAttributeValuePost> attributeValues = new TableCredentialAttributeValue(attributeValuesTable).convertToListOfObjects();
            logger.debug("Attribute definitions converted successfully");

            // execute API
            logger.debug("Executing API to create credential");
            ApiResponse response = v2_CredentialVault.createAttributeValues(client, credentialId, attributeValues, encryptionKey);

            // convert output for bot
            logger.debug("Converting response into bot output");
            ListValue output = response.getListValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}