package com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.learninginstances;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.IQBot_LearningInstances;
import com.automationanywhere.professional_services.cr_apis.apis.iqbot.IQBotStatus;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetFilenamesWithStatus",
        label = "[[GetFilenamesWithStatus.label]]",
        group_label = "[[IQBot_LearningInstances.group.label]]",
        node_label = "[[GetFilenamesWithStatus.node_label]]",
        description = "[[GetFilenamesWithStatus.description]]",
        icon = "pkg.svg",

        return_label = "[[GetFilenamesWithStatus.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class G_GetFilenamesWithStatus
        extends BaseBotCommand
{
    // specific static variables
    public static final String SUCCESS = "SUCCESS";
    public static final String VALIDATION = "VALIDATION";
    public static final String INVALID = "INVALID";
    public static final String UNTRAINED = "UNTRAINED";
    public static final String UNCLASSIFIED = "UNCLASSIFIED";

    // standard static variables
    private static final Logger logger = LogManager.getLogger(G_GetFilenamesWithStatus.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue getFilenamesWithStatus(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: learning instance ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[IQBot.learningInstanceId.label]]")
            @NotEmpty
                    String learningInstanceId,

            // 3: doc status
            @Idx(index = "3", type = RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(
                            node_label = "[[IQBot.status.success.label]]",
                            label = "[[IQBot.status.success.label]]",
                            value = SUCCESS)
                    ),
                    @Idx.Option(index = "3.2", pkg = @Pkg(
                            node_label = "[[IQBot.status.validation.label]",
                            label = "[[IQBot.status.validation.label]]",
                            value = VALIDATION)
                    ),
                    @Idx.Option(index = "3.3", pkg = @Pkg(
                            node_label = "[[IQBot.status.invalid.label]",
                            label = "[[IQBot.status.invalid.label]]",
                            value = INVALID)
                    ),
                    @Idx.Option(index = "3.4", pkg = @Pkg(
                            node_label = "[[IQBot.status.untrained.label]",
                            label = "[[IQBot.status.untrained.label]]",
                            value = UNTRAINED)
                    ),
                    @Idx.Option(index = "3.5", pkg = @Pkg(
                            node_label = "[[IQBot.status.unclassified.label]",
                            label = "[[IQBot.status.unclassified.label]]",
                            value = UNCLASSIFIED)
                    )
            })
            @Pkg(label = "[[IQBot.status.label]]", default_value = SUCCESS, default_value_type = STRING)
            @Inject
                    String docStatusInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for IQ Bot get filenames with status:");
            logger.debug("Learning instance ID: {}", learningInstanceId);
            logger.debug("Doc status: {}", docStatusInput);

            // transform status into enum
            IQBotStatus docStatus = IQBotStatus.valueOf(docStatusInput);

            // execute API
            logger.debug("Executing API to get filenames with status");
            ApiResponse response = IQBot_LearningInstances.getAllFilesWithStatus(client, learningInstanceId, docStatus);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}