package com.automationanywhere.professional_services.cr_apis.botcommands._d_custom_functions;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.utilities.converters.RepositoryPathToUriConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "TransformRepositoryPathToUri",
        label = "[[TransformRepositoryPathToUri.label]]",
        group_label = "[[Custom_functions.group_label]]",
        node_label = "[[TransformRepositoryPathToUri.node_label]]",
        description = "[[TransformRepositoryPathToUri.description]]",
        icon = "pkg.svg",

        return_label = "Repository path in URI format",
        return_type = DataType.STRING,
        return_required = true
)

public class B_TransformRepositoryPathToUri
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(B_TransformRepositoryPathToUri.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public StringValue transformRepositoryPathToUri(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: repository path
            @Idx(index = "2", type = AttributeType.TEXT)
            @Pkg(label = "[[TransformRepositoryPathToUri.repositoryPath.label]]")
            @NotEmpty
                    String repositoryPath
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log input parameters
            logger.debug("Input parameters for transform repository path to URI:");
            logger.debug("Repository path: {}", repositoryPath);

            // transform output
            return new StringValue(RepositoryPathToUriConverter.convertPathToUri(repositoryPath));

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}