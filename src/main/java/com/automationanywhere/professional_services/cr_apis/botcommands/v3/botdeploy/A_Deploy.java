package com.automationanywhere.professional_services.cr_apis.botcommands.v3.botdeploy;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v3_BotDeploy;
import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "Deploy",
        label = "[[Deploy.label]]",
        group_label = "[[BotDeploy.group_label]]",
        node_label = "[[Deploy.node_label]]",
        description = "[[Deploy.description]]",
        icon = "pkg.svg",

        return_label = "[[Deploy.return_label]]",
        return_type = STRING
)

public class A_Deploy
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(A_Deploy.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public Value<String> deploy(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: file ID for bot to be deployed
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[Generic.botFileId.label]]")
            @NotEmpty
                    String fileIdString,

            // 3: comma-separated list of run as user IDs
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[Deploy.runAsUserIds.label]]")
            @NotEmpty
                    String runAsUserIdsString,

            // 4: comma-separated list of pool IDs (not required)
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "[[Deploy.poolIds.label]]")
                    String poolIdsString,

            // 5: boolean value to override default device
            @Idx(index = "5", type = AttributeType.BOOLEAN)
            @Pkg(label = "[[Deploy.overrideDefaultDevice.label]]", default_value_type = DataType.BOOLEAN, default_value = "false")
            @NotEmpty
                    Boolean overrideDefaultDevice,

            // 6: number of run as users to use
            @Idx(index = "6", type = TEXT)
            @Pkg(label = "[[Deploy.numOfRunAsUsersToUse.label]]")
                    String numOfRunAsUsersToUseString
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log deployment request with input parameters
            logger.debug("Input parameters for deployment:");
            logger.debug("File ID: {}", fileIdString);
            logger.debug("Run as user IDs: {}", runAsUserIdsString);
            logger.debug("Pool IDs: {}", poolIdsString);
            logger.debug("Overwrite default device: {}", overrideDefaultDevice.toString());
            logger.debug("Number of run as users to use: {}", numOfRunAsUsersToUseString);

            // transform input data to required types
            logger.debug("Transforming input data");

            int fileId = Integer.parseInt(fileIdString);
            int numOfRunAsUsersToUse = Integer.parseInt(numOfRunAsUsersToUseString);

            String[] runAsUserIdsStringArray = runAsUserIdsString.split(",");
            int[] runAsUserIdsIntArray = new int[runAsUserIdsStringArray.length];
            for (int i = 0; i < runAsUserIdsStringArray.length; i++)
            {
                runAsUserIdsIntArray[i] = Integer.parseInt(runAsUserIdsStringArray[i].trim());
            }

            String[] poolIdsStringArray = poolIdsString.split(",");
            int[] poolIdsIntArray = new int[poolIdsStringArray.length];
            for (int i = 0; i < poolIdsStringArray.length; i++)
            {
                poolIdsIntArray[i] = Integer.parseInt(poolIdsStringArray[i].trim());
            }

            logger.debug("Input data transformed successfully");

            // execute API
            logger.debug("Executing API to deploy bot");
            String deploymentId = v3_BotDeploy.deploy(client, fileId, runAsUserIdsIntArray, poolIdsIntArray, overrideDefaultDevice, numOfRunAsUsersToUse);
            logger.debug("Deployment successful with ID {}", deploymentId);

            return new StringValue(deploymentId);
        } catch (Exception ex)
        {
            throw new BotException(ex);
        }
    }
}