package com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.functions.IQBot_Files;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "DeleteFile",
        label = "[[DeleteFile.label]]",
        group_label = "[[IQBot_Files.group.label]]",
        node_label = "[[DeleteFile.node_label]]",
        description = "[[DeleteFile.description]]",
        icon = "pkg.svg"
)

public class D_DeleteFile
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(D_DeleteFile.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void deleteFile(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: learning instance ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[IQBot.learningInstanceId.label]]")
            @NotEmpty
                    String learningInstanceId,

            // 3: file ID
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[IQBot.fileId.label]]")
            @NotEmpty
                    String fileId
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for IQ Bot file deletion:");
            logger.debug("Learning instance ID: {}", learningInstanceId);
            logger.debug("File ID: {}", fileId);

            // execute API
            logger.debug("Executing API to delete file");
            IQBot_Files.deleteFile(client, learningInstanceId, fileId);
            logger.debug("File deletion successful");

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}