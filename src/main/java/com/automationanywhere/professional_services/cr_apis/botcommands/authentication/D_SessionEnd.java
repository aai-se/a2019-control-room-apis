/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.professional_services.cr_apis.botcommands.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_Authentication;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomSessionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;

@BotCommand

@CommandPkg(
        name = "SessionEnd",
        label = "[[SessionEnd.label]]",
        group_label = "[[Authentication.group_label]]",
        node_label = "[[SessionEnd.node_label]]",
        description = "[[SessionEnd.description]]",
        icon = "pkg.svg"
)

public class D_SessionEnd
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(D_SessionEnd.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void endSession(

            /*
            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,
             */

            // 1: session name
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(SessionType.CUSTOM_SESSION.name(), sessionNameInput);
            logger.debug("Session retrieved");

            // logout user
            v1_Authentication.logoutUser(client);
            logger.debug("User logged out");

            // remove client from sessions map
            ControlRoomSessionManager.deleteSession(sessionName);

            // log session name
            logger.debug("Session removed with name {}", sessionName);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex.getMessage());
        }
    }
}