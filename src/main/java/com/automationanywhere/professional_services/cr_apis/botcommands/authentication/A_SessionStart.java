package com.automationanywhere.professional_services.cr_apis.botcommands.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.bot.service.BotException;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomSessionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.ANY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "SessionStart",
        label = "[[SessionStart.label]]",
        group_label = "[[Authentication.group_label]]",
        node_label = "[[SessionStart.node_label]]",
        description = "[[SessionStart.description]]",
        icon = "pkg.svg",

        return_label = "[[SessionStart.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = ANY
)

public class A_SessionStart
        extends BaseBotCommand
{
    // custom static variables
    private static final String PASSWORD = "PASSWORD";
    private static final String API_KEY = "API_KEY";

    // standard static variables
    private static Logger logger = LogManager.getLogger(A_SessionStart.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue startSessionAndAuthenticateUser(

            // 1: session name
            @Idx(index = "1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: Control Room URL
            @Idx(index = "2", type = CREDENTIAL)
            @Pkg(label = "[[SessionStart.controlRoomUrl.label]]")
            @NotEmpty
                    SecureString controlRoomUrlInput,

            // 3: username
            @Idx(index = "3", type = CREDENTIAL)
            @Pkg(label = "[[SessionStart.username.label]]")
            @NotEmpty
                    SecureString usernameInput,


            // 4: authentication method
            @Idx(index = "4", type = RADIO, options = {
                    @Idx.Option(index = "4.1", pkg = @Pkg(
                            node_label = "[[SessionStart.authMethod.password.label]]",
                            label = "[[SessionStart.authMethod.password.label]]",
                            value = PASSWORD)
                    ),
                    @Idx.Option(index = "4.2", pkg = @Pkg(
                            node_label = "[[SessionStart.authMethod.apiKey.label]]",
                            label = "[[SessionStart.authMethod.apiKey.label]]",
                            value = API_KEY)
                    )
            })
            @Pkg(label = "[[SessionStart.authMethod.label]]", default_value = PASSWORD, default_value_type = STRING)
            @Inject
                    String authMethodInput,

            // 5: authentication input
            @Idx(index = "5", type = CREDENTIAL)
            @Pkg(label = "[[SessionStart.authInput.label]]")
            @NotEmpty
                    SecureString authValueInput
    )
    {
        // business logic
        try
        {
            boolean sessionNameExists = false;
            boolean sessionIsIdentical = false;

            // check if a session with this name already exists
            if (ControlRoomSessionManager.checkIfSessionExists(sessionNameInput))
            {
                sessionNameExists = true;

                // get plaintext for inputs
                String commandUrl = controlRoomUrlInput.getInsecureString();
                String commandUsername = usernameInput.getInsecureString();
                String commandAuthValue = authValueInput.getInsecureString();

                // get existing session values from client
                ControlRoomRestClient client = ControlRoomSessionManager.getSession(sessionNameInput);

                String sessionUrl = client.controlRoomUrl;
                String sessionUsername = client.authRequest.username;
                String sessionAuthValue;

                if (authMethodInput == PASSWORD)
                {
                    sessionAuthValue = client.authRequest.password;
                } else if (authMethodInput == API_KEY)
                {
                    sessionAuthValue = client.authRequest.apiKey;
                } else
                {
                    throw new Exception("Auth method is not password or API key");
                }

                // check values
                if (sessionUrl.equals(commandUrl)
                            && sessionUsername.equals(commandUsername)
                            && sessionAuthValue.equals(commandAuthValue))
                {
                    sessionIsIdentical = true;
                } else
                {
                    logger.debug("Session URL: {}", sessionUrl);
                    logger.debug("Command URL: {}", commandUrl);

                    logger.debug("Session username: {}", sessionUsername);
                    logger.debug("Command username: {}", commandUsername);

                    logger.debug("Session auth value: {}", sessionAuthValue);
                    logger.debug("Command auth value: {}", commandAuthValue);
                }
            }

            // is the session name already exists, return without re-authenticating or throw exception
            if (sessionNameExists && sessionIsIdentical)
            {
                // a session with this name and identical parameters was found
                // return existing values without starting a new session
                logger.debug("Identical session already started with name {}", sessionNameInput);
                ControlRoomRestClient client = ControlRoomSessionManager.getSession(sessionNameInput);
                return client.getAuthResponse().getDictionaryValue();
            } else if (sessionNameExists && !sessionIsIdentical)
            {
                // a session with this name was found but the parameters do not match
                // throw an exception
                logger.debug("Non-identical session found with name {}", sessionName);
                throw new BotException("Session name was found but parameters do not match - please end the original session before starting a new one with the same session name");
            }

            // session name does not already exist - continue with authentication flow

            // set session variables without fetching the REST client
            this.setSessionTypeAndName(SessionType.CUSTOM_SESSION.name(), sessionNameInput);

            // set authentication variables
            String controlRoomUrl = controlRoomUrlInput.getInsecureString();
            String username = usernameInput.getInsecureString();
            String authMethod = authMethodInput;
            String authValue = authValueInput.getInsecureString();

            // instantiate REST client
            logger.debug("Creating client");
            ControlRoomRestClient client = new ControlRoomRestClient(sessionType, super.globalSessionContext, controlRoomUrl, username, authMethod, authValue);

            // set token in client
            client.updateToken();
            logger.debug("Client authenticated");

            // add client to sessions map
            ControlRoomSessionManager.setSession(sessionName, client);
            logger.debug("Session created with name {}", sessionName);

            DictionaryValue output = client.getAuthResponse().getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotException(ex.getMessage());
        }
    }
}