package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.lockers;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "AddCredentialToLocker",
        label = "[[AddCredentialToLocker.label]]",
        group_label = "[[CredentialVault.Lockers.group_label]]",
        node_label = "[[AddCredentialToLocker.node_label]]",
        description = "[[AddCredentialToLocker.description]]",
        icon = "pkg.svg"
)

public class M_AddCredentialToLocker
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(M_AddCredentialToLocker.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void addCredentialToLocker(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: locker ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CredentialVault.lockerId.label]]")
            @NotEmpty
                    String lockerId,

            // 3: credential ID
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[CredentialVault.credentialId.label]]")
            @NotEmpty
                    String credentialId
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for adding credential to locker:");
            logger.debug("Locker ID: {}", lockerId);
            logger.debug("Credential ID: {}", credentialId);

            // execute API
            logger.debug("Executing API to search for credentials");
            v2_CredentialVault.addCredentialToLocker(client, lockerId, credentialId);

            // convert output for bot
            logger.debug("API executed successfully");

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}