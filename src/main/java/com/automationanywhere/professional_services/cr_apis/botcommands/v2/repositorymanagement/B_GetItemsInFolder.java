package com.automationanywhere.professional_services.cr_apis.botcommands.v2.repositorymanagement;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_RepositoryManagement;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetItemsInFolder",
        label = "[[GetItemsInFolder.label]]",
        group_label = "[[RepositoryManagement.group_label]]",
        node_label = "[[GetItemsInFolder.node_label]]",
        description = "[[GetItemsInFolder.description]]",
        icon = "pkg.svg",

        return_label = "[[RepositoryManagement.repositoryItems.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class B_GetItemsInFolder
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(B_GetItemsInFolder.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue getItemsInFolder(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: folder ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[GetItemsInFolder.folderId.label]]")
            @NotEmpty
                    Double folderIdInput,

            // 3: filter request JSON
            @Idx(index = "3", type = TEXTAREA)
            @Pkg(label = "[[Generic.filterRequestJson.label]]")
                    String filterRequestJson
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // transform number input value
            Long folderId = folderIdInput.longValue();

            // log credential search input parameters
            logger.debug("Input parameters for get repository items (folder):");
            logger.debug("Workspace type: {}", folderId.toString());
            logger.debug("Filter request JSON: {}", filterRequestJson);

            // execute API
            logger.debug("Executing API to get repository items (folder)");
            ApiResponse response = v2_RepositoryManagement.getItemsInFolder(client, folderId, filterRequestJson);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}
