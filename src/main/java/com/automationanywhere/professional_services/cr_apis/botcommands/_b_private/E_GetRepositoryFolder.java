package com.automationanywhere.professional_services.cr_apis.botcommands._b_private;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v2_RepositoryManagement;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@BotCommand

@CommandPkg(
        name = "GetRepositoryFolder",
        group_label = "[[Private.group_label]]",
        label = "[[GetRepositoryFolder.label]]",
        node_label = "[[GetRepositoryFolder.node_label]]",
        description = "[[GetRepositoryFolder.description]]",
        icon = "pkg.svg",

        return_label = "[[GetRepositoryFolder.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class E_GetRepositoryFolder
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(E_GetRepositoryFolder.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue createRepositoryFolder(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: folder ID
            @Idx(index = "2", type = AttributeType.NUMBER)
            @Pkg(label = "[[Generic.folderId.label]]")
            @NotEmpty
                    Double folderIdInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number variables
            Long folderId = folderIdInput.longValue();

            // log credential search input parameters
            logger.debug("Input parameters for get repository folder:");
            logger.debug("Folder ID: {}", folderId.toString());

            // execute API
            logger.debug("Executing API to get folder");
            ApiResponse response = _private_v2_RepositoryManagement.getRepositoryFolder(this.client, folderId);
            return response.getDictionaryValue();

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}