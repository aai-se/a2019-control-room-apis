package com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.teams;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_AARI;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetTeamMembers",
        label = "[[GetTeamMembers.label]]",
        group_label = "[[AARI.Teams.group_label]]",
        node_label = "[[GetTeamMembers.node_label]]",
        description = "[[GetTeamMembers.description]]",
        icon = "pkg.svg",

        return_label = "[[GetTeamMembers.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class C_GetTeamMembers
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(C_GetTeamMembers.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue getTeamMembers(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: team ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[AARI.teamId.label]]")
            @NotEmpty
                    Double teamIdInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs
            Long teamId = teamIdInput.longValue();

            // log credential search input parameters
            logger.debug("Input parameters for get team members:");
            logger.debug("Team ID: {}", teamId);

            // execute API
            logger.debug("Executing API to get team members");
            ApiResponse response = v2_AARI.getTeamMembers(client, teamId);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}