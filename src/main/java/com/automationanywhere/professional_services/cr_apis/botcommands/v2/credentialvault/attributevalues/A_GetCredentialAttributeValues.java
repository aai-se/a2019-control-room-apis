package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.attributevalues;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetCredentialAttributeValues",
        label = "[[GetCredentialAttributeValues.label]]",
        group_label = "[[CredentialVault.AttributeValues.group_label]]",
        node_label = "[[GetCredentialAttributeValues.node_label]]",
        description = "[[GetCredentialAttributeValues.description]]",
        icon = "pkg.svg",

        return_label = "[[GetCredentialAttributeValues.return_label]]",
        return_type = DataType.LIST,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class A_GetCredentialAttributeValues
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_GetCredentialAttributeValues.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public Value<List> getCredentialAttributeValues(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: credential ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CredentialVault.credentialId.label]]")
            @NotEmpty
                    String credentialId,

            // 3: credential attribute ID
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[GetCredentialAttributeValues.credentialAttributeId.label]]")
            @NotEmpty
                    String credentialAttributeId,

            // 4: user ID
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "[[GetCredentialAttributeValues.userId.label]]", description = "[[GetCredentialAttributeValues.userId.description]]")
                    String userId,

            // 5: encryption key
            @Idx(index = "5", type = TEXT)
            @Pkg(label = "[[CredentialVault.encryptionKey.label]]", description = "[[CredentialVault.encryptionKey.description]]")
                    String encryptionKey
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for retrieving credential attribute values:");
            logger.debug("Credential attribute ID: {}", credentialAttributeId);

            if (!userId.isEmpty())
            {
                logger.debug("User ID: {}", userId);
            }
            if (!encryptionKey.isEmpty())
            {
                logger.debug("RSA encryption key: {}", encryptionKey);
            }

            // execute API
            logger.debug("Executing API to get  credential attribute values");
            ApiResponse response = v2_CredentialVault.getAttributeValues(client, credentialId, credentialAttributeId, userId, encryptionKey);

            // convert output for bot
            logger.debug("Converting response into bot output");
            ListValue output = response.getListValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}