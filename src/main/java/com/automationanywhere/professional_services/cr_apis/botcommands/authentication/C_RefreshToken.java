/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
package com.automationanywhere.professional_services.cr_apis.botcommands.authentication;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_Authentication;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "RefreshToken",
        label = "[[RefreshToken.label]]",
        group_label = "[[Authentication.group_label]]",
        node_label = "[[RefreshToken.node_label]]",
        description = "[[RefreshToken.description]]",
        icon = "pkg.svg",

        return_label = "[[RefreshToken.return_label]]",
        return_type = STRING,
        return_required = true
)

public class C_RefreshToken
        extends BaseBotCommand
{

    // standard static variables
    private static Logger logger = LogManager.getLogger(C_RefreshToken.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public StringValue refreshToken(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: authentication token
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[RefreshToken.authToken.label]]")
            @NotEmpty
                    String authToken
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // refresh token with Control Room and get new token
            ApiResponse apiResponse = v1_Authentication.refreshToken(client, authToken);
            JSONObject jsonObj = apiResponse.getJsonObject();
            String refreshedAuthToken = jsonObj.getString("token");

            // log token refresh details
            logger.debug("Original token value: {}", authToken);
            logger.debug("Refreshed token value: {}", refreshedAuthToken);

            // return refreshed authentication token
            return new StringValue(refreshedAuthToken);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}
