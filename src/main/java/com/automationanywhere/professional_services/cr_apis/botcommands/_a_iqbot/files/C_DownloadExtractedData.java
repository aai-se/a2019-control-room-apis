package com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.iqbot.files.FileDownloadResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.professional_services.cr_apis.apis.functions.IQBot_Files;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.RADIO;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "DownloadExtractedData",
        label = "[[DownloadExtractedData.label]]",
        group_label = "[[IQBot_Files.group.label]]",
        node_label = "[[DownloadExtractedData.node_label]]",
        description = "[[DownloadExtractedData.description]]",
        icon = "pkg.svg",

        return_label = "[[DownloadExtractedData.return_label]]",
        return_type = DataType.ANY,
        return_required = true
)

public class C_DownloadExtractedData
        extends BaseBotCommand
{
    public static final String RETURN_TYPE_LIST = "List";
    public static final String RETURN_TYPE_MAPPINGS = "Mappings";

    // standard static variables
    private static final Logger logger = LogManager.getLogger(C_DownloadExtractedData.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public Value downloadExtractedData(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: file ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[IQBot.fileId.label]]")
            @NotEmpty
                    String fileId,

            // 3: return type
            @Idx(index = "3", type = RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(
                            node_label = "[[DownloadExtractedData.returnType.list.label]]",
                            label = "[[DownloadExtractedData.returnType.list.label]]",
                            description = "[[DownloadExtractedData.returnType.list.description]]",
                            value = RETURN_TYPE_LIST)
                    ),
                    @Idx.Option(index = "3.2", pkg = @Pkg(
                            node_label = "[[DownloadExtractedData.returnType.mappings.label]",
                            label = "[[DownloadExtractedData.returnType.mappings.label]]",
                            description = "[[DownloadExtractedData.returnType.mappings.description]]",
                            value = RETURN_TYPE_MAPPINGS)
                    )
            })
            @Pkg(label = "[[DownloadExtractedData.returnType.label]]", default_value = RETURN_TYPE_LIST, default_value_type = STRING)
            @Inject
                    String returnType,

            // 3: config JSON
            @Idx(index = "3.2.1", type = TEXT)
            @Pkg(label = "[[DownloadExtractedData.dataMappings.label]]", description = "[[DownloadExtractedData.dataMappings.description]]")
            @NotEmpty
                    String configJson
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for IQ Bot output download:");
            logger.debug("File ID: {}", fileId);
            logger.debug("Return type: {}", returnType);
            logger.debug("Config: {}", configJson);

            // execute API
            logger.debug("Executing API to get file output");
            FileDownloadResponse response = IQBot_Files.downloadExtractedData(client, fileId, configJson);

            // convert output for bot
            logger.debug("Converting response into bot output");

            if (returnType.equals(RETURN_TYPE_LIST))
            {
                return response.convertToListValue();
            } else
            {
                return response.convertToDictionaryValue();
            }
        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}