package com.automationanywhere.professional_services.cr_apis.botcommands.v2.blm;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_BLM;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.LocalFile;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "DownloadBlmPackage",
        label = "[[DownloadBlmPackage.label]]",
        group_label = "[[BLM.group_label]]",
        node_label = "[[DownloadBlmPackage.node_label]]",
        description = "[[DownloadBlmPackage.description]]",
        icon = "pkg.svg"
)

public class D_DownloadBlmPackage
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(D_DownloadBlmPackage.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void downloadBlmPackage(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: request ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[BLM.requestId.label]]")
            @NotEmpty
                    String requestId,

            // 3: path to output file
            @Idx(index = "3", type = AttributeType.FILE)
            @Pkg(label = "[[DownloadBlmPackage.pathToOutputFile.label]]")
            @NotEmpty
            @LocalFile
                    String pathToOutputFile
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for download export package:");
            logger.debug("Request ID: {}", requestId);
            logger.debug("Path to output file: {}", pathToOutputFile);

            // execute API
            logger.debug("Executing API to download output file");
            v2_BLM.downloadBlmPackage(client, requestId, pathToOutputFile);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}