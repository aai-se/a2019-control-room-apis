package com.automationanywhere.professional_services.cr_apis.botcommands._a_iqbot.files;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.IQBot_Files;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "UploadFile",
        label = "[[UploadFile.label]]",
        group_label = "[[IQBot_Files.group.label]]",
        node_label = "[[UploadFile.node_label]]",
        description = "[[UploadFile.description]]",
        icon = "pkg.svg",

        return_label = "[[UploadFile.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class A_UploadFile
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_UploadFile.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue uploadFile(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: learning instance ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[IQBot.learningInstanceId.label]]")
            @NotEmpty
                    String learningInstanceId,

            // 3: path to file
            @Idx(index = "3", type = FILE)
            @Pkg(label = "[[UploadFile.pathToFile.label]]")
            @NotEmpty
                    String pathToFile
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for IQ Bot file upload:");
            logger.debug("Learning instance ID: {}", learningInstanceId);
            logger.debug("Path to file: {}", pathToFile);

            // execute API
            logger.debug("Executing API to upload file");
            ApiResponse response = IQBot_Files.uploadFile(client, learningInstanceId, pathToFile);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}