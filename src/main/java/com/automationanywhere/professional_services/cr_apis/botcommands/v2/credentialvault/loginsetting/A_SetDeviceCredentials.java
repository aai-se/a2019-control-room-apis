package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.loginsetting;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "SetDeviceCredentials",
        label = "[[SetDeviceCredentials.label]]",
        group_label = "[[CredentialVault.LoginSetting.group_label]]",
        node_label = "[[SetDeviceCredentials.node_label]]",
        description = "[[SetDeviceCredentials.description]]",
        icon = "pkg.svg"
)

public class A_SetDeviceCredentials
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_SetDeviceCredentials.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void setDeviceCredentials(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: device username
            @Idx(index = "2", type = AttributeType.CREDENTIAL)
            @Pkg(label = "[[SetDeviceCredentials.deviceUsername.label]]")
            @NotEmpty
                    SecureString deviceUsernameInput,

            // 3: device password
            @Idx(index = "3", type = AttributeType.CREDENTIAL)
            @Pkg(label = "[[SetDeviceCredentials.devicePassword.label]]")
            @NotEmpty
                    SecureString devicePasswordInput,

            // 4: Control Room username
            @Idx(index = "4", type = AttributeType.CREDENTIAL)
            @Pkg(label = "[[SetDeviceCredentials.controlRoomUsername.label]]", description = "[[SetDeviceCredentials.controlRoomUsername.description]]")
                    SecureString controlRoomUsernameInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert input parameters
            String deviceUsername = deviceUsernameInput.getInsecureString();
            String devicePassword = devicePasswordInput.getInsecureString();
            String controlRoomUsername = controlRoomUsernameInput.getInsecureString();

            // execute API
            logger.debug("Executing API to update device credentials");
            v2_CredentialVault.setDeviceCredentials(this.client, deviceUsername, devicePassword, controlRoomUsername);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}