package com.automationanywhere.professional_services.cr_apis.botcommands._d_custom_functions;

import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v2_RepositoryManagement;
import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "AmendBotContent",
        label = "[[AmendBotContent.label]]",
        group_label = "[[Custom_functions.group_label]]",
        node_label = "[[AmendBotContent.node_label]]",
        description = "[[AmendBotContent.description]]",
        icon = "pkg.svg"
)

public class A_AmendBotContent
        extends BaseBotCommand
{
    // custom static variables
    public static final String DO_NOT_UPDATE_PATHS = "N";
    public static final String UPDATE_PATHS = "Y";

    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_AmendBotContent.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void amendBotContent(

            // 1: session type
            @Idx(index = "1", type = RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: bot file ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[Generic.botFileId.label]]")
            @NotEmpty
                    Double fileIdInput,

            // 3: deactivate input variables
            @Idx(index = "3", type = BOOLEAN)
            @Pkg(label = "[[AmendBotContent.deactivateInputs.label]]", default_value = "false", default_value_type = DataType.BOOLEAN)
            @NotEmpty
                    Boolean deactivateInputs,

            // 4: deactivate output variables
            @Idx(index = "4", type = BOOLEAN)
            @Pkg(label = "[[AmendBotContent.deactivateOutputs.label]]", default_value = "false", default_value_type = DataType.BOOLEAN)
            @NotEmpty
                    Boolean deactivateOutputs,

            // 5: activate comments
            @Idx(index = "5", type = BOOLEAN)
            @Pkg(label = "[[AmendBotContent.activateComments.label]]", default_value = "false", default_value_type = DataType.BOOLEAN)
            @NotEmpty
                    Boolean activateComments,

            // 6: deactivate database close
            @Idx(index = "6", type = BOOLEAN)
            @Pkg(label = "[[AmendBotContent.deactivateDatabaseClose.label]]", default_value = "false", default_value_type = DataType.BOOLEAN)
            @NotEmpty
                    Boolean deactivateDatabaseClose,

            // 7: path updates
            @Idx(index = "7", type = RADIO, options = {
                    @Idx.Option(index = "7.1", pkg = @Pkg(
                            node_label = "[[AmendBotContent.pathUpdates.false.label]]",
                            label = "[[AmendBotContent.pathUpdates.false.label]]",
                            value = DO_NOT_UPDATE_PATHS)
                    ),
                    @Idx.Option(index = "7.2", pkg = @Pkg(
                            node_label = "[[AmendBotContent.pathUpdates.true.label]",
                            label = "[[AmendBotContent.pathUpdates.true.label]]",
                            value = UPDATE_PATHS)
                    )
            })
            @Pkg(label = "[[AmendBotContent.pathUpdates.label]]", default_value = DO_NOT_UPDATE_PATHS, default_value_type = STRING)
            @Inject
                    String pathUpdates,

            // 7.2.1: source URI
            @Idx(index = "7.2.1", type = TEXT)
            @Pkg(label = "[[AmendBotContent.pathUpdates.sourceUri.label]]")
            @NotEmpty
                    String sourceUri,

            // 7.2.2: target URI
            @Idx(index = "7.2.2", type = TEXT)
            @Pkg(label = "[[AmendBotContent.pathUpdates.targetUri.label]]")
            @NotEmpty
                    String targetUri
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number variables
            Long fileId = fileIdInput.longValue();

            // log credential search input parameters
            logger.debug("Input parameters for amend bot content:");
            logger.debug("Bot file ID: {}", fileId.toString());
            logger.debug("Deactivate input variables: {}", deactivateInputs.toString());
            logger.debug("Deactivate output variables: {}", deactivateOutputs.toString());
            logger.debug("Activate comments: {}", activateComments.toString());

            // execute API to get bot content
            logger.debug("Executing API to get bot content");
            ApiResponse botContentResponse = _private_v2_RepositoryManagement.getBotContent(client, fileId);
            String botContent = botContentResponse.json;

            // amend bot content
            logger.debug("Executing logic to amend bot content");

            if (deactivateInputs)
            {
                botContent = botContent.replaceAll("\"input\":true", "\"input\":false");
            }

            if (deactivateOutputs)
            {
                botContent = botContent.replaceAll("\"output\":true", "\"output\":false");
            }

            if (activateComments)
            {
                botContent = botContent.replaceAll("\"packageName\":\"Comment\",\\s*\"disabled\":true,",
                        "\"packageName\":\"Comment\",\r\n\"disabled\":false,");
            }

            if (deactivateDatabaseClose)
            {
                botContent = botContent.replaceAll("\"commandName\":\"disconnect\",\\s*\"packageName\":\"Database\",\\s*\"disabled\":false,",
                        "\"commandName\":\"disconnect\",\r\n\"packageName\":\"Database\",\r\n\"disabled\":true,");
            }

            if (pathUpdates.equals(UPDATE_PATHS))
            {
                botContent = botContent.replaceAll(sourceUri, targetUri);
            }

            // execute API to set bot content
            logger.debug("Executing API to set bot content");
            _private_v2_RepositoryManagement.setBotContent(client, fileId, botContent, false);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}