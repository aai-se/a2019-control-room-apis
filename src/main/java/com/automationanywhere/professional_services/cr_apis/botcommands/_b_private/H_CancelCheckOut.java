package com.automationanywhere.professional_services.cr_apis.botcommands._b_private;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.professional_services.cr_apis.apis.functions._private_v2_RepositoryManagement;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@BotCommand

@CommandPkg(
        name = "CancelCheckOut",
        group_label = "[[Private.group_label]]",
        label = "[[CancelCheckOut.label]]",
        node_label = "[[CancelCheckOut.node_label]]",
        description = "[[CancelCheckOut.description]]",
        icon = "pkg.svg"
)

public class H_CancelCheckOut
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(H_CancelCheckOut.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void cancelCheckout(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = DataType.STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: file ID
            @Idx(index = "2", type = AttributeType.NUMBER)
            @Pkg(label = "[[Generic.fileId.label]]")
            @NotEmpty
                    Double fileIdInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number variables
            Long fileId = fileIdInput.longValue();

            // log input parameters
            logger.debug("Input parameters for check out:");
            logger.debug("File ID: {}", fileId.toString());

            // execute API
            logger.debug("Executing API to cancel check out");
            List<Long> fileIds = new ArrayList<>();
            fileIds.add(fileId);
            _private_v2_RepositoryManagement.cancelCheckout(client, fileIds);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}