package com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement.users;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_UserManagement;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement.UserManagementUtilities;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.AttributeType.LIST;
import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.*;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.*;

@BotCommand

@CommandPkg(
        name = "UpdateUser",
        label = "[[UpdateUser.label]]",
        group_label = "[[UserManagement.Users.group_label]]",
        node_label = "[[UpdateUser.node_label]]",
        description = "[[UpdateUser.description]]",
        icon = "pkg.svg",

        return_label = "[[Users.Generic.return_label]]",
        return_type = DICTIONARY,
        return_sub_type = ANY
)

public class D_UpdateUser
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(D_UpdateUser.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue updateUser(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[Users.Generic.userId.label]]")
            @NotEmpty
                    Double id,

            // 3: password
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[Users.Generic.password.label]]")
                    String password,

            // 4: email
            @Idx(index = "4", type = TEXT)
            @Pkg(label = "[[Users.Generic.email.label]]")
            @NotEmpty
                    String email,

            // 5: role IDs
            @Idx(index = "5", type = LIST)
            @Pkg(label = "[[Users.Generic.roleIds.label]]", description = "[[Users.Generic.roleIds.description]]")
            @NotEmpty
                    List<Value> roleIdInput,

            // 6: first name
            @Idx(index = "6", type = TEXT)
            @Pkg(label = "[[Users.Generic.firstName.label]]")
                    String firstName,

            // 7: last name
            @Idx(index = "7", type = TEXT)
            @Pkg(label = "[[Users.Generic.lastName.label]]")
                    String lastName,

            // 8: description
            @Idx(index = "8", type = TEXT)
            @Pkg(label = "[[Users.Generic.description.label]]")
                    String description,

            // 9: license features
            @Idx(index = "9", type = LIST)
            @Pkg(label = "[[Users.Generic.licenseFeatures.label]]")
                    List<Value> licenseFeatureInput,

            // 10: enable auto login
            @Idx(index = "10", type = BOOLEAN)
            @Pkg(label = "[[Users.Generic.enableAutoLogin.label]]")
                    Boolean enableAutoLogin,

            // 11: disabled
            @Idx(index = "11", type = BOOLEAN)
            @Pkg(label = "[[Users.Generic.disabled.label]]")
                    Boolean disabled
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs from doubles to longs
            Long userId = id.longValue();

            // convert list inputs from A2019 data types to Java data types
            ArrayList<Long> roleIds = UserManagementUtilities.convertRoleIds(roleIdInput);
            ArrayList<String> licenseFeatures = UserManagementUtilities.convertLicenseFeatures(licenseFeatureInput);

            // log user creation with input parameters
            logger.debug("Input parameters for user update:");

            logger.debug("User ID: {}", id);
            logger.debug("Password: {}", password);
            logger.debug("Email: {}", email);
            logger.debug("Role ids: {}", roleIds.toString());
            logger.debug("First name: {}", firstName);
            logger.debug("Last name: {}", lastName);
            logger.debug("Description: {}", description);
            logger.debug("License features: {}", licenseFeatures.toString());
            logger.debug("Enable auto login: {}", enableAutoLogin.toString());
            logger.debug("Disabled: {}", disabled.toString());

            logger.debug("Executing API to update user");

            // execute API
            ApiResponse response = v1_UserManagement.updateUser(client, userId, password, email, roleIds, firstName, lastName, description, licenseFeatures,
                    enableAutoLogin, disabled);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}