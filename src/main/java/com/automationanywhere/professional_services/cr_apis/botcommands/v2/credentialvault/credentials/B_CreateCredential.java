package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.credentials;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.botvariables.tables.credentialvault.TableCredentialAttributeDefinition;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.professional_services.cr_apis.apis.v2.credentialvault.attributevalues.CredentialAttributePost;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.automationanywhere.commandsdk.model.AttributeType.TABLE;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "CreateCredential",
        label = "[[CreateCredential.label]]",
        group_label = "[[CredentialVault.Credentials.group_label]]",
        node_label = "[[CreateCredential.node_label]]",
        description = "[[CreateCredential.description]]",
        icon = "pkg.svg",

        return_label = "[[CreateCredential.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = DataType.ANY
)

public class B_CreateCredential
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(B_CreateCredential.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue createCredential(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: credential name
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CreateCredential.credentialName.label]]")
            @NotEmpty
                    String credentialName,

            // 3: credential description
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[CreateCredential.credentialDescription.label]]")
                    String credentialDescription,

            // 4: attribute definitions
            @Idx(index = "4", type = TABLE)
            @Pkg(label = "[[CreateCredential.attributeDefinitions.label]]")
                    Table attributeDefinitionsTable
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log user creation with input parameters
            logger.debug("Input parameters for credential creation:");
            logger.debug("Credential name: {}", credentialName);
            logger.debug("Credential description: {}", credentialDescription);

            List<CredentialAttributePost> attributeDefinitions;

            // if table is populated, validate and convert to list of objects
            if (attributeDefinitionsTable != null && !attributeDefinitionsTable.getRows().isEmpty())
            {
                logger.debug("Converting attribute definitions table");
                attributeDefinitions = new TableCredentialAttributeDefinition(attributeDefinitionsTable).convertToListOfObjects();
                logger.debug("Attribute definitions converted successfully");
            }
            // set table to null if not populated
            else
            {
                logger.debug("Attribute definitions not provided");
                attributeDefinitions = null;
            }

            // execute API
            logger.debug("Executing API to create credential");
            ApiResponse response = v2_CredentialVault.createCredential(client, credentialName, credentialDescription, attributeDefinitions);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}