package com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.teams;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_AARI;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "DeleteTeam",
        label = "[[DeleteTeam.label]]",
        group_label = "[[AARI.Teams.group_label]]",
        node_label = "[[DeleteTeam.node_label]]",
        description = "[[DeleteTeam.description]]",
        icon = "pkg.svg"
)

public class G_DeleteTeam
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(G_DeleteTeam.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void deleteTeam(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: team ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[AARI.teamId.label]]")
            @NotEmpty
                    Double teamIdInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert numeric inputs
            Long teamId = teamIdInput.longValue();

            // log credential search input parameters
            logger.debug("Input parameters for delete team:");
            logger.debug("Team ID: {}", teamId);

            // execute API
            logger.debug("Executing API to delete team");
            v2_AARI.deleteTeam(client, teamId);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}