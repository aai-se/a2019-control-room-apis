package com.automationanywhere.professional_services.cr_apis.botcommands.authentication;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.GlobalSessionContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetBotAgentToken",
        label = "[[GetBotAgentToken.label]]",
        group_label = "[[Authentication.group_label]]",
        node_label = "[[GetBotAgentToken.node_label]]",
        description = "[[GetBotAgentToken.description]]",
        icon = "pkg.svg",

        return_label = "[[GetBotAgentToken.return_label]]",
        return_type = STRING,
        return_required = true
)

public class E_GetBotAgentToken
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(E_GetBotAgentToken.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public StringValue startSessionAndAuthenticateUser()
    {
        // business logic
        try
        {
            return new StringValue(super.globalSessionContext.getUserToken());
        } catch (Exception ex)
        {
            throw new BotCommandException(ex.getMessage());
        }
    }
}