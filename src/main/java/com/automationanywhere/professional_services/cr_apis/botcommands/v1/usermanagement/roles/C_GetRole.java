package com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement.roles;

import com.automationanywhere.professional_services.cr_apis.apis.generic.ApiResponse;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_UserManagement;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXTAREA;
import static com.automationanywhere.commandsdk.model.DataType.ANY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetRole",
        label = "[[GetRole.label]]",
        group_label = "[[UserManagement.Roles.group_label]]",
        node_label = "[[GetRole.node_label]]",
        description = "[[GetRole.description]]",
        icon = "pkg.svg",

        return_label = "[[GetRole.return_label]]",
        return_type = DataType.DICTIONARY,
        return_sub_type = ANY,
        return_required = true
)

public class C_GetRole
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(C_GetRole.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public DictionaryValue getRole(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: role ID
            @Idx(index = "2", type = TEXTAREA)
            @Pkg(label = "[[Roles.Generic.roleId.label]]")
                    String roleId
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for get role:");
            logger.debug("Role ID: {}", roleId);

            // execute API
            logger.debug("Executing API to get role");
            ApiResponse response = v1_UserManagement.getRole(client, roleId);

            // convert output for bot
            logger.debug("Converting response into bot output");
            DictionaryValue output = response.getDictionaryValue();
            return output;

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}