package com.automationanywhere.professional_services.cr_apis.botcommands.v2.aari.files;

import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_AARI;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "GetFileUrl",
        label = "[[GetFileUrl.label]]",
        group_label = "[[AARI.Files.group_label]]",
        node_label = "[[GetFileUrl.node_label]]",
        description = "[[GetFileUrl.description]]",
        icon = "pkg.svg",

        return_label = "[[GetFileUrl.return_label]]",
        return_type = DataType.STRING,
        return_sub_type = DataType.ANY,
        return_required = true
)

public class A_GetFileUrl
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(A_GetFileUrl.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public StringValue getFileUrl(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = AttributeType.TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: file ID
            @Idx(index = "2", type = AttributeType.NUMBER)
            @Pkg(label = "[[GetFileUrl.fileId.label]]")
            @NotEmpty
                    Double fileIdInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs
            Long fileId = fileIdInput.longValue();

            // log input parameters
            logger.debug("Input parameters:");
            logger.debug("File ID: {}", fileId);

            // execute API
            logger.debug("Executing API to get file URL");
            String url = v2_AARI.getFileUrl(client, fileId);

            // convert output for bot
            return new StringValue(url);

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}