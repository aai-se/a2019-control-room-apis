package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.lockers;

import com.automationanywhere.professional_services.cr_apis.apis.enums.LockerMemberPermission;
import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "AddOrUpdateLockerMember",
        label = "[[AddOrUpdateLockerMember.label]]",
        group_label = "[[CredentialVault.Lockers.group_label]]",
        node_label = "[[AddOrUpdateLockerMember.node_label]]",
        description = "[[AddOrUpdateLockerMember.description]]",
        icon = "pkg.svg"
)

public class J_AddOrUpdateLockerMember
        extends BaseBotCommand
{
    // static variables
    public static final String PARTICIPATE = "PARTICIPATE";
    public static final String MANAGE = "MANAGE";
    public static final String OWN = "OWN";

    // standard static variables
    private static final Logger logger = LogManager.getLogger(J_AddOrUpdateLockerMember.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void addOrUpdateLockerMember(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: locker ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CredentialVault.lockerId.label]]")
            @NotEmpty
                    String lockerId,

            // 3: user ID
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[Users.Generic.userId.label]]")
            @NotEmpty
                    String userId,

            // 4: locker permission
            @Idx(index = "4", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "4.1", pkg = @Pkg(
                            node_label = "[[AddOrUpdateLockerMember.membershipType.participant.label]]",
                            label = "[[AddOrUpdateLockerMember.membershipType.participant.label]]",
                            value = PARTICIPATE)
                    ),
                    @Idx.Option(index = "4.2", pkg = @Pkg(
                            node_label = "[[AddOrUpdateLockerMember.membershipType.manager.label]",
                            label = "[[AddOrUpdateLockerMember.membershipType.manager.label]]",
                            value = MANAGE)
                    )
                    ,
                    @Idx.Option(index = "4.3", pkg = @Pkg(
                            node_label = "[[AddOrUpdateLockerMember.membershipType.owner.label]",
                            label = "[[AddOrUpdateLockerMember.membershipType.owner.label]]",
                            value = OWN)
                    )
            })
            @Pkg(label = "[[AddOrUpdateLockerMember.membershipType.label]]", default_value = PARTICIPATE, default_value_type = STRING)
            @Inject
                    String memberPermissionsInput
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log add/update locker member input parameters
            logger.debug("Input parameters for adding/updating locker member:");
            logger.debug("Locker ID: {}", lockerId);
            logger.debug("User ID: {}", userId);
            logger.debug("Membership type: {}", memberPermissionsInput);

            // convert membership type to enum
            LockerMemberPermission memberPermission = LockerMemberPermission.valueOf(memberPermissionsInput);

            // execute API
            logger.debug("Executing API to add/update locker member");
            v2_CredentialVault.addOrUpdateLockerMember(client, lockerId, userId, memberPermission);

            // convert output for bot
            logger.debug("API executed successfully");

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}