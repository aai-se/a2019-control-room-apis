package com.automationanywhere.professional_services.cr_apis.botcommands.v2.credentialvault.credentials;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v2_CredentialVault;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "UpdateCredentialOwner",
        label = "[[UpdateCredentialOwner.label]]",
        group_label = "[[CredentialVault.Credentials.group_label]]",
        node_label = "[[UpdateCredentialOwner.node_label]]",
        description = "[[UpdateCredentialOwner.description]]",
        icon = "pkg.svg"
)

public class F_UpdateCredentialOwner
        extends BaseBotCommand
{
    // standard static variables
    private static final Logger logger = LogManager.getLogger(F_UpdateCredentialOwner.class);

    // global session context
    @GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void updateCredentialOwner(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: credential ID
            @Idx(index = "2", type = TEXT)
            @Pkg(label = "[[CredentialVault.credentialId.label]]")
            @NotEmpty
                    String credentialId,

            // 3: credential owner ID
            @Idx(index = "3", type = TEXT)
            @Pkg(label = "[[UpdateCredentialOwner.credentialOwnerId.label]]")
            @NotEmpty
                    String credentialOwnerId
    )
    {

        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // log credential search input parameters
            logger.debug("Input parameters for updating credential owner:");
            logger.debug("Credential ID: {}", credentialId);
            logger.debug("Credential ID: {}", credentialOwnerId);

            // execute API
            logger.debug("Executing API to update credential ownership");
            v2_CredentialVault.updateCredentialOwnership(client, credentialId, credentialOwnerId);

            // convert output for bot
            logger.debug("API executed successfully");

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}