package com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement.users;

import com.automationanywhere.professional_services.cr_apis.apis.functions.v1_UserManagement;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.professional_services.cr_apis.botcommands.BaseBotCommand;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand

@CommandPkg(
        name = "DeleteUser",
        label = "[[DeleteUser.label]]",
        group_label = "[[UserManagement.Users.group_label]]",
        node_label = "[[DeleteUser.node_label]]",
        description = "[[DeleteUser.description]]",
        icon = "pkg.svg"
)

public class E_DeleteUser
        extends BaseBotCommand
{
    // standard static variables
    private static Logger logger = LogManager.getLogger(E_DeleteUser.class);

    // global session context
    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext)
    {
        super.globalSessionContext = globalSessionContext;
    }

    // action entry point
    @Execute
    public void deleteUser(

            // 1: session type
            @Idx(index = "1", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "1.1", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            label = "[[Generic.sessionType.useBotRunnerSession.label]]",
                            value = COMMAND_BOT_RUNNER_SESSION)
                    ),
                    @Idx.Option(index = "1.2", pkg = @Pkg(
                            node_label = "[[Generic.sessionType.useCustomSession.label]",
                            label = "[[Generic.sessionType.useCustomSession.label]]",
                            value = COMMAND_CUSTOM_SESSION)
                    )
            })
            @Pkg(label = "[[Generic.sessionType.label]]", default_value = COMMAND_BOT_RUNNER_SESSION, default_value_type = STRING)
            @Inject
                    String sessionTypeInput,

            // 1.2.1: session name
            @Idx(index = "1.2.1", type = TEXT)
            @Pkg(label = "[[Generic.sessionName.label]]", default_value = "Default", default_value_type = DataType.STRING)
            @NotEmpty
                    String sessionNameInput,

            // 2: ID
            @Idx(index = "2", type = NUMBER)
            @Pkg(label = "[[Users.Generic.userId.label]]")
            @NotEmpty
                    Double id
    )
    {
        // business logic
        try
        {
            // set session
            this.setSession(sessionTypeInput, sessionNameInput);
            logger.debug("Session retrieved");

            // convert number inputs from doubles to longs
            Long userId = id.longValue();

            // log user creation with input parameters
            logger.debug("Input parameters for user deletion:");

            logger.debug("User ID: {}", id);

            logger.debug("Executing API to delete user");

            // execute API
            v1_UserManagement.deleteUser(client, userId);

            // convert output for bot
            logger.debug("API executed successfully");

        } catch (Exception ex)
        {
            throw new BotCommandException(ex);
        }
    }
}