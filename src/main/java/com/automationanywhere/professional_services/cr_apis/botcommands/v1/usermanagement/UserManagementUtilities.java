package com.automationanywhere.professional_services.cr_apis.botcommands.v1.usermanagement;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;

import java.util.ArrayList;
import java.util.List;

public class UserManagementUtilities
{
    public static ArrayList<Long> convertRoleIds(List<Value> input)
            throws Exception
    {
        ArrayList<Long> output = new ArrayList<Long>();

        for (Value value : input)
        {
            if (value instanceof NumberValue)
            {
                NumberValue val = (NumberValue) value;
                output.add(val.get().longValue());
            } else
            {
                throw new Exception("Unsupported data type in list of role IDs - must be Number");
            }
        }

        return output;
    }

    public static ArrayList<String> convertLicenseFeatures(List<Value> input)
            throws Exception
    {
        ArrayList<String> output = new ArrayList<String>();

        if (input != null && !input.isEmpty())
        {
            for (Value value : input)
            {
                if (value instanceof StringValue)
                {
                    StringValue val = (StringValue) value;
                    output.add(val.get());
                } else
                {
                    throw new Exception("Unsupported data type in list of license features - must be String");
                }
            }
        }

        return output;
    }
}
