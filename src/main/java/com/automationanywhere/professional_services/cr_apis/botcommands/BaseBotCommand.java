package com.automationanywhere.professional_services.cr_apis.botcommands;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomRestClient;
import com.automationanywhere.professional_services.cr_apis.apis.enums.SessionType;
import com.automationanywhere.professional_services.cr_apis.apis.ControlRoomSessionManager;

public abstract class BaseBotCommand
{
    // static variables
    protected static final String COMMAND_BOT_RUNNER_SESSION = "BOT_RUNNER_SESSION";
    protected static final String COMMAND_CUSTOM_SESSION = "CUSTOM_SESSION";

    // instance variables
    protected SessionType sessionType;
    protected String sessionName;
    protected ControlRoomRestClient client;

    protected com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public abstract void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext);

    protected void setSession(String sessionTypeInput, String sessionNameInput)
            throws Exception
    {
        // set session type and name
        this.setSessionTypeAndName(sessionTypeInput, sessionNameInput);

        // if using the bot runner session and if a bot runner session with a valid client isn't already set, set it now
        if (sessionType == SessionType.BOT_RUNNER_SESSION && !ControlRoomSessionManager.checkIfSessionExists(SessionType.BOT_RUNNER_SESSION.name()))
        {
            startBotRunnerSession(this.globalSessionContext);
        }

        // set reference to the client
        this.client = ControlRoomSessionManager.getSession(sessionName);
    }

    protected void setSessionTypeAndName(String sessionTypeInput, String sessionNameInput)
    {
        // translate string to enum
        this.sessionType = SessionType.valueOf(sessionTypeInput);

        // overwrite session name if required for bot runner session type
        if (this.sessionType == SessionType.BOT_RUNNER_SESSION)
        {
            this.sessionName = SessionType.BOT_RUNNER_SESSION.name();
        } else
        {
            this.sessionName = sessionNameInput;
        }
    }

    protected void startBotRunnerSession(GlobalSessionContext globalSessionContext)
            throws Exception
    {
        ControlRoomRestClient client = new ControlRoomRestClient(sessionType, globalSessionContext, null, null, null, null);
        client.updateToken();
        ControlRoomSessionManager.setSession(sessionName, client);
    }
}